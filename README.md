# Drone Control

Drone Control is a 3D-space-shooter with strategy elements. Combat is inspired by games like Freelancer and Star Citizen.

This is a hobby project of mine so there is no intention of making any money from this :) Since it is made with [Unity](https://unity.com/) it should be easy to fork or mod the game.

## Licensing

Most of the project is licensed under the MIT license, but not all of it. Refere to the credits.txt in the root folder of the repo for more details.