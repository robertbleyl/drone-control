using Core;
using Core.Events;
using DroneControl.Core;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Users;
using UnityEngine.UI;

namespace DroneControl.Player {
    public class CursorController : MonoBehaviour {
        private readonly Vector2 centeredAnchor = new(0.5f, 0.5f);
        private readonly Vector2 bottomRightAnchor = new(1f, 0f);

        [SerializeField]
        private Sprite gatlingGunCursorSprite;

        [SerializeField]
        private Sprite pulseCannonCursorSprite;

        [SerializeField]
        private Sprite railGunCursorSprite;

        [SerializeField]
        private Sprite missileCursorFailing;

        [SerializeField]
        private Sprite missileCursorSuccess;

        [SerializeField]
        public bool disableAmmoDisplay;

        [SerializeField]
        public Color gatlingGunColor;

        [SerializeField]
        public Color pulseCannonColor;

        [SerializeField]
        public Color railGunColor;

        [SerializeField]
        private float cursorSpeed = 1000f;

        [SerializeField]
        private PlayerInput playerInput;

        [SerializeField]
        private Image cursorImage;

        [SerializeField]
        private RectTransform cursorTransform;

        [SerializeField]
        private RectTransform canvasTransform;

        [SerializeField]
        private Canvas canvas;

        [SerializeField]
        private GameObject ammoDisplayBackground;

        [SerializeField]
        private Image ammoDisplay;

        private ShipInputController inputController;
        private GameObject currentShip;
        private GameObject currentTarget;

        private string currentControlScheme = InputConstants.INPUT_SCHEME_KEYBOARD_AND_MOUSE;

        private Mouse virtualMouse;
        private bool gamepadSchemeActive;

        private Camera cam;

        private void Start() {
            cam = Camera.main;
            GameObject playerObject = GameObject.FindGameObjectWithTag(Tags.Player);
            inputController = playerObject.GetComponent<ShipInputController>();

            GameEvents.INSTANCE.onWeaponTypeChanged += onWeaponTypeChanged;
            GameEvents.INSTANCE.onTargetChange += onTargetChange;
            GameEvents.INSTANCE.onPlayerShipChange += changeShip;
            GameEvents.INSTANCE.onAmmoChanged += onAmmoChanged;
        }


        private void OnDestroy() {
            GameEvents.INSTANCE.onWeaponTypeChanged -= onWeaponTypeChanged;
            GameEvents.INSTANCE.onTargetChange -= onTargetChange;
            GameEvents.INSTANCE.onPlayerShipChange -= changeShip;
            GameEvents.INSTANCE.onAmmoChanged -= onAmmoChanged;
        }

        private void onAmmoChanged(AmmoChangedEvent ammoChangedEvent) {
            if (ammoChangedEvent.ownedShip == currentShip) {
                ammoDisplay.fillAmount = ammoChangedEvent.newAmmoPercentage;
            }
        }

        private void onTargetChange(GameObject target) {
            currentTarget = target;
        }

        private void onWeaponTypeChanged(WeaponSwitchedEvent evt) {
            if (evt.weaponType == WeaponType.MISSILES) {
                return;
            }

            Sprite sprite = null;
            Color color = Color.white;

            switch (evt.weaponType) {
                case WeaponType.GATLING_GUN:
                    sprite = gatlingGunCursorSprite;
                    color = gatlingGunColor;
                    break;
                case WeaponType.PULSE_CANNON:
                    sprite = pulseCannonCursorSprite;
                    color = pulseCannonColor;
                    break;
                case WeaponType.RAIL_GUN:
                    sprite = railGunCursorSprite;
                    color = railGunColor;
                    break;
            }

            ammoDisplay.color = color;
            updateCursorImage(sprite, true, !disableAmmoDisplay, Vector2.one);
            ammoDisplay.fillAmount = evt.newAmmoPercentage;
        }

        private void changeShip(GameObject ship) {
            currentShip = ship;

            if (currentShip != null) {
                onWeaponTypeChanged(new WeaponSwitchedEvent(WeaponType.GATLING_GUN, currentShip, 1f));
            }
        }

        public void OnEnable() {
            if (virtualMouse == null) {
                virtualMouse = (Mouse)InputSystem.AddDevice("VirtualMouse");
            }
            else if (!virtualMouse.added) {
                InputSystem.AddDevice(virtualMouse);
            }

            InputUser.PerformPairingWithDevice(virtualMouse, playerInput.user);

            if (cursorTransform != null) {
                Vector2 cursorPosition = cursorTransform.anchoredPosition;
                InputState.Change(virtualMouse.position, cursorPosition);
            }

            InputSystem.onAfterUpdate += updateCursor;
            playerInput.onControlsChanged += onControlsChanged;
        }

        public void OnDisable() {
            if (virtualMouse != null && virtualMouse.added) {
                InputSystem.RemoveDevice(virtualMouse);
            }

            InputSystem.onAfterUpdate -= updateCursor;
            playerInput.onControlsChanged -= onControlsChanged;
        }

        private void onControlsChanged(PlayerInput input) {
            if (input.currentControlScheme != currentControlScheme) {
                currentControlScheme = input.currentControlScheme;

                gamepadSchemeActive = currentControlScheme.Equals(InputConstants.INPUT_SCHEME_GAMEPAD);

                if (gamepadSchemeActive) {
                    Vector2 mousePos = Mouse.current.position.ReadValue();
                    InputState.Change(virtualMouse.position, mousePos);
                    anchorCursor(mousePos);
                }
                else {
                    Mouse.current.WarpCursorPosition(virtualMouse.position.ReadValue());
                }
            }
        }

        private void updateCursor() {
            if (!gamepadSchemeActive) {
                anchorCursor(Mouse.current.position.ReadValue());
                return;
            }

            if (virtualMouse == null || Gamepad.current == null) {
                return;
            }

            Vector2 deltaValue = Gamepad.current.leftStick.ReadValue();

            deltaValue *= cursorSpeed * Time.unscaledDeltaTime;

            Vector2 currentPosition = virtualMouse.position.ReadValue();
            Vector2 newPosition = currentPosition + deltaValue;

            newPosition.x = Mathf.Clamp(newPosition.x, 0, Screen.width);
            newPosition.y = Mathf.Clamp(newPosition.y, 0, Screen.height);

            InputState.Change(virtualMouse.position, newPosition);
            InputState.Change(virtualMouse.delta, deltaValue);

            anchorCursor(newPosition);
        }

        private void anchorCursor(Vector2 position) {
            Camera c = canvas.renderMode == RenderMode.ScreenSpaceOverlay ? null : cam;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasTransform, position, c,
                out Vector2 anchoredPosition);
            cursorTransform.anchoredPosition = anchoredPosition;
        }

        private void Update() {
            Cursor.visible = false;

            if (inputController.lockingOn) {
                Sprite cursorSprite = missileCursorFailing;

                if (MissileLockOnArea.missileLockOnPossible(currentTarget, getCurrentCursorPosition())) {
                    cursorSprite = missileCursorSuccess;
                }

                updateCursorImage(cursorSprite, true, false, Vector2.one);
            }
        }

        public Vector3 getCurrentCursorPosition() {
            if (currentControlScheme.Equals(InputConstants.INPUT_SCHEME_GAMEPAD) && virtualMouse != null &&
                virtualMouse.added) {
                return virtualMouse.position.ReadValue();
            }

            return Mouse.current.position.ReadValue();
        }

        public void updateCursorImage(Sprite cursorSprite, bool centeredCursor, bool showAmmoDisplay, Vector2 scale) {
            cursorImage.sprite = cursorSprite;

            if (centeredCursor) {
                cursorImage.rectTransform.anchorMin = centeredAnchor;
                cursorImage.rectTransform.anchorMax = centeredAnchor;
                cursorImage.rectTransform.pivot = centeredAnchor;
            }
            else {
                cursorImage.rectTransform.anchorMin = bottomRightAnchor;
                cursorImage.rectTransform.anchorMax = bottomRightAnchor;
                cursorImage.rectTransform.pivot = bottomRightAnchor;
            }

            ammoDisplayBackground.SetActive(showAmmoDisplay);
        }
    }
}