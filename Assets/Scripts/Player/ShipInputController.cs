﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Events;
using DroneControl.Core;
using DroneControl.Ship;
using DroneControl.Ship.Weapon;
using FMODUnity;
using Ship.Properties;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DroneControl.Player {
    public class ShipInputController : MonoBehaviour {
        [SerializeField]
        private float lockOnTime = 3f;

        [SerializeField]
        public bool disableDecoupledMode;

        [SerializeField]
        private EventReference switchToGatlingGunSound;

        [SerializeField]
        private EventReference switchToPulseCannonSound;

        [SerializeField]
        private EventReference switchToRailGunSound;

        [SerializeField]
        private EventReference startLockOnSound;

        [SerializeField]
        private EventReference cancelLockOnSound;

        [SerializeField]
        private EventReference noAmmoSound;

        [SerializeField]
        private float noAmmoSoundCooldown = 1f;

        [SerializeField]
        private PlayerInput playerInput;

        private ShipMovementController movementController;
        private DashEnergyController dashEnergyController;
        private PlayerShipWeaponController weaponController;
        private HealthController healthController;
        private CursorController cursorController;

        public bool placingBuilding;
        public bool disableMissiles;

        public bool allWeaponsDisabled;
        public bool disablePulseCannon;
        public bool disableRailgun;

        private float noAmmoSoundTimer;

        private RapidFireMissileFactory rapidFireMissileFactory;
        public bool lockingOn;
        private bool lockedOn;
        private float lockOnTimer;

        private float forwardThrusterChange;
        private bool firingMainWeapons;

        private Camera cam;

        private GameObject currentShip;
        private GameObject currentTarget;

        private void Start() {
            cam = Camera.main;

            cursorController = GameObject.FindGameObjectWithTag(Tags.CursorController).GetComponent<CursorController>();

            GameEvents.INSTANCE.onGameObjectDestroyed += checkDestroyed;
            GameEvents.INSTANCE.onShowScreen += screenChanged;
            GameEvents.INSTANCE.onBotSpawned += onBotSpawned;
            GameEvents.INSTANCE.onPlayerShipChange += onPlayerShipChange;
            GameEvents.INSTANCE.onTargetChange += onTargetChange;

            initInput();

            GameEvents.INSTANCE.changePlayerShip(null);
            
            enabled = false;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onGameObjectDestroyed -= checkDestroyed;
            GameEvents.INSTANCE.onShowScreen -= screenChanged;
            GameEvents.INSTANCE.onBotSpawned -= onBotSpawned;
            GameEvents.INSTANCE.onPlayerShipChange -= onPlayerShipChange;
            GameEvents.INSTANCE.onTargetChange -= onTargetChange;

            playerInput.actions["ShipThrust"].performed -= increaseShipThrust;
            playerInput.actions["ShipThrust"].canceled -= decreaseThrust;

            playerInput.actions["ShipStrafe"].performed -= updateStrafe;
            playerInput.actions["ShipStrafe"].canceled -= updateStrafe;

            playerInput.actions["ShipRoll"].performed -= updateRoll;
            playerInput.actions["ShipRoll"].canceled -= updateRoll;

            playerInput.actions["ToggleCouplingMode"].performed -= updateDecoupledMode;
            playerInput.actions["ShipDash"].performed -= updateDash;

            playerInput.actions["FireMainWeapons"].performed -= fireWeapon;
            playerInput.actions["FireMainWeapons"].canceled -= stopFiringWeapon;

            playerInput.actions["SwitchToGatlingGun"].performed -= switchToGatlingGun;
            playerInput.actions["SwitchToPulseCannon"].performed -= switchToPulseCannon;
            playerInput.actions["SwitchToRailgun"].performed -= switchToRailGun;
            playerInput.actions["SwitchToMissileLauncher"].performed -= switchToMissileLauncher;

            playerInput.actions["SelectNearestEnemy"].performed -= markNearestTarget;
            playerInput.actions["SelectEnemyInCrosshair"].performed -= updateCrosshairTarget;

            playerInput.actions["ShowBuildMenu"].performed -= toggleBuildMenu;
            playerInput.actions["TogglePauseMenu"].performed -= togglePauseMenu;
        }

        private void initInput() {
            playerInput.actions["ShipThrust"].performed += increaseShipThrust;
            playerInput.actions["ShipThrust"].canceled += decreaseThrust;

            playerInput.actions["ShipStrafe"].performed += updateStrafe;
            playerInput.actions["ShipStrafe"].canceled += updateStrafe;

            playerInput.actions["ShipRoll"].performed += updateRoll;
            playerInput.actions["ShipRoll"].canceled += updateRoll;

            playerInput.actions["ToggleCouplingMode"].performed += updateDecoupledMode;
            playerInput.actions["ShipDash"].performed += updateDash;

            playerInput.actions["FireMainWeapons"].performed += fireWeapon;
            playerInput.actions["FireMainWeapons"].canceled += stopFiringWeapon;

            playerInput.actions["SwitchToGatlingGun"].performed += switchToGatlingGun;
            playerInput.actions["SwitchToPulseCannon"].performed += switchToPulseCannon;
            playerInput.actions["SwitchToRailgun"].performed += switchToRailGun;
            playerInput.actions["SwitchToMissileLauncher"].performed += switchToMissileLauncher;

            playerInput.actions["SelectNearestEnemy"].performed += markNearestTarget;
            playerInput.actions["SelectEnemyInCrosshair"].performed += updateCrosshairTarget;

            playerInput.actions["ShowBuildMenu"].performed += toggleBuildMenu;
            playerInput.actions["TogglePauseMenu"].performed += togglePauseMenu;
        }

        private void increaseShipThrust(InputAction.CallbackContext ctx) {
            forwardThrusterChange = ctx.ReadValue<float>();
        }

        private void decreaseThrust(InputAction.CallbackContext _) {
            forwardThrusterChange = 0f;
        }

        private void fireWeapon(InputAction.CallbackContext ctx) {
            firingMainWeapons = ctx.ReadValue<float>() > 0f;
        }

        private void stopFiringWeapon(InputAction.CallbackContext ctx) {
            firingMainWeapons = false;
        }

        private void updateStrafe(InputAction.CallbackContext ctx) {
            float value = ctx.ReadValue<float>();

            movementController.strafeLeft = value > 0f;
            movementController.strafeRight = value < 0f;
        }

        private void updateRoll(InputAction.CallbackContext ctx) {
            float value = ctx.ReadValue<float>();

            movementController.rollLeft = value > 0f;
            movementController.rollRight = value < 0f;
        }

        private void checkDestroyed(DestructionEvent evt) {
            if (evt.gameObject == currentShip) {
                if (!PlayerData.INSTANCE.hasLost && enabled) {
                    cam.transform.parent = transform.parent;
                    StartCoroutine(returnToBuildMenuAfterDeath());
                }
            }
            else if (evt.gameObject == currentTarget) {
                StartCoroutine(resetToNearestTarget());
            }
        }

        private void onTargetChange(GameObject target) {
            currentTarget = target;
        }

        private IEnumerator returnToBuildMenuAfterDeath() {
            yield return new WaitForSeconds(2.5f);

            GameEvents.INSTANCE.changePlayerShip(null);
            lockingOn = false;
            GameEvents.INSTANCE.changeTarget(null);
            GameEvents.INSTANCE.playerShipExplosionFinished();

            if (!PlayerData.INSTANCE.hasLost) {
                GameEvents.INSTANCE.showScreen(GameScreenType.buildMenu);
            }
        }

        private void onBotSpawned(GameObject bot) {
            if (currentTarget == null) {
                StartCoroutine(resetToNearestTarget());
            }
        }

        private void screenChanged(GameScreenType type) {
            enabled = type is GameScreenType.hud or GameScreenType.playerDestroyedScreen;

            if (enabled) {
                playerInput.SwitchCurrentActionMap(InputConstants.INPUT_MAP_SHIP_INPUTS);
            }

            if (type != GameScreenType.hud && movementController != null) {
                movementController.yawPercentage = 0f;
                movementController.pitchPercentage = 0f;
            }
        }

        private void Update() {
            if (!enabled || currentShip == null) {
                return;
            }

            if (healthController.getCurrentHitPointsPercentage() <= 0f) {
                GameEvents.INSTANCE.showScreen(GameScreenType.playerDestroyedScreen);
                return;
            }

            if (noAmmoSoundTimer > 0f) {
                noAmmoSoundTimer -= Time.deltaTime;
            }

            updateForwardMovement();
            updateRotation();
            fireWeapons();
        }

        private void updateForwardMovement() {
            if (forwardThrusterChange == 0f) {
                return;
            }

            float amount = 0.6f * Time.deltaTime * forwardThrusterChange;

            if (movementController.forwardThrustPercentage <= 1f) {
                movementController.forwardThrustPercentage += amount;
            }

            if (movementController.forwardThrustPercentage > 1f) {
                movementController.forwardThrustPercentage = 1f;
            }
            else if (movementController.forwardThrustPercentage < 0f) {
                movementController.forwardThrustPercentage = 0f;
            }
        }

        private IEnumerator resetToNearestTarget() {
            yield return new WaitForSecondsRealtime(0.01f);
            markNearestTarget(new InputAction.CallbackContext());
        }

        private void markNearestTarget(InputAction.CallbackContext callbackContext) {
            lockOnTimer = 0f;
            lockedOn = false;

            if (currentShip != null) {
                List<GameObject> targets = getTargetableBots();

                double minDist = double.MaxValue;
                GameObject newTarget = null;

                foreach (GameObject target in targets) {
                    double dist = Vector3.Distance(currentShip.transform.position,
                        target.transform.position);

                    if (dist > 1f && dist < minDist) {
                        minDist = dist;
                        newTarget = target;
                    }
                }

                GameEvents.INSTANCE.changeTarget(newTarget);
            }
        }

        private void updateCrosshairTarget(InputAction.CallbackContext callbackContext) {
            List<GameObject> targets = getTargetableBots();

            Vector3 mousePos = cursorController.getCurrentCursorPosition();
            double minDist = double.MaxValue;
            GameObject newTarget = null;

            foreach (GameObject target in targets) {
                Vector3 screenPos = cam.WorldToScreenPoint(target.transform.position);

                if (screenPos.z <= 0f) {
                    continue;
                }

                screenPos.z = 0f;
                double dist = Vector3.Distance(mousePos, screenPos);

                if (dist <= 64f && dist < minDist) {
                    minDist = dist;
                    newTarget = target;
                }
            }

            if (newTarget != null) {
                GameEvents.INSTANCE.changeTarget(newTarget);
            }
        }

        private List<GameObject> getTargetableBots() {
            return Bots.INSTANCE.getBotControllers()
                .Select(c => c.getCurrentShip())
                .Where(d => d.GetComponent<HealthController>().enabled)
                .Select(d => d.GetComponent<Rigidbody>().gameObject).ToList();
        }

        private void updateDecoupledMode(InputAction.CallbackContext callbackContext) {
            if (movementController != null && !disableDecoupledMode) {
                movementController.decoupled = !movementController.decoupled;
            }
        }

        private void updateRotation() {
            Vector3 cursorPosition = cursorController.getCurrentCursorPosition();

            float halfWidth = Screen.width / 2f;
            float halfHeight = Screen.height / 2f;

            Vector3 center = new Vector3(halfWidth, halfHeight, 0f);

            float xDiff = cursorPosition.x - center.x;
            float yDiff = center.y - cursorPosition.y;

            movementController.yawPercentage = xDiff / halfWidth;
            movementController.pitchPercentage = yDiff / halfHeight;
        }

        private void updateDash(InputAction.CallbackContext callbackContext) {
            if (!dashEnergyController.hasEnoughEnergy(dashEnergyController.getDashEnergyCosts())) {
                return;
            }

            if (forwardThrusterChange > 0f) {
                movementController.dashForward = true;
            }
            else if (forwardThrusterChange < 0f) {
                movementController.dashBackwards = true;
            }
            else if (movementController.strafeLeft) {
                movementController.dashLeft = true;
            }
            else if (movementController.strafeRight) {
                movementController.dashRight = true;
            }
        }

        private void switchToMissileLauncher(InputAction.CallbackContext callbackContext) {
            if (!disableMissiles && currentTarget != null) {
                if (weaponController.getWeaponController(WeaponType.MISSILES).getCooldownPercentage() <= 0f) {
                    weaponController.switchWeapons(WeaponType.MISSILES);
                    lockingOn = true;
                    RuntimeManager.PlayOneShot(startLockOnSound);
                }
                else if (noAmmoSoundTimer <= 0f) {
                    noAmmoSoundTimer = noAmmoSoundCooldown;
                    RuntimeManager.PlayOneShot(noAmmoSound);
                }
            }
        }

        private void switchToRailGun(InputAction.CallbackContext callbackContext) {
            weaponController.switchWeapons(WeaponType.RAIL_GUN);
            weaponSwitched();
            RuntimeManager.PlayOneShot(switchToRailGunSound);
        }

        private void switchToPulseCannon(InputAction.CallbackContext callbackContext) {
            weaponController.switchWeapons(WeaponType.PULSE_CANNON);
            weaponSwitched();
            RuntimeManager.PlayOneShot(switchToPulseCannonSound);
        }

        private void switchToGatlingGun(InputAction.CallbackContext callbackContext) {
            weaponController.switchWeapons(WeaponType.GATLING_GUN);
            weaponSwitched();
            RuntimeManager.PlayOneShot(switchToGatlingGunSound);
        }

        private void weaponSwitched() {
            cancelLockOn();
        }

        private void cancelLockOn() {
            lockedOn = false;
            lockingOn = false;
            lockOnTimer = 0f;
        }

        private void fireWeapons() {
            if (allWeaponsDisabled) {
                return;
            }

            if (lockingOn) {
                if (lockedOn) {
                    if (firingMainWeapons) {
                        weaponController.fireActiveWeapons(currentTarget, cursorController.getCurrentCursorPosition());
                        cancelLockOn();
                        weaponController.switchToLastActiveWeapons();
                    }
                }
                else if (MissileLockOnArea.missileLockOnPossible(currentTarget,
                             cursorController.getCurrentCursorPosition())) {
                    lockOnTimer += Time.deltaTime;

                    if (lockOnTimer >= lockOnTime) {
                        lockedOn = true;
                        lockOnTimer = lockOnTime;
                    }
                }
                else {
                    lockedOn = false;

                    lockOnTimer -= Time.deltaTime;

                    if (lockOnTimer < 0f) {
                        lockOnTimer = 0f;
                    }
                }
            }
            else {
                cancelLockOn();

                if (rapidFireMissileFactory != null && !rapidFireMissileFactory.firing && firingMainWeapons) {
                    weaponController.fireActiveWeapons(null, cursorController.getCurrentCursorPosition());
                }
            }
        }

        private void onPlayerShipChange(GameObject ship) {
            currentShip = ship;

            if (currentShip == null) {
                lockingOn = false;
                lockedOn = false;
                return;
            }

            movementController = currentShip.GetComponent<ShipMovementController>();
            weaponController = currentShip.GetComponent<PlayerShipWeaponController>();
            healthController = currentShip.GetComponent<HealthController>();
            dashEnergyController = currentShip.GetComponent<DashEnergyController>();

            StartCoroutine(fetchWeaponControllers());
            StartCoroutine(resetToNearestTarget());
        }

        private IEnumerator fetchWeaponControllers() {
            yield return new WaitForEndOfFrame();
            rapidFireMissileFactory = weaponController.getWeaponController(WeaponType.MISSILES)
                .GetComponent<RapidFireMissileFactory>();
        }

        private void togglePauseMenu(InputAction.CallbackContext callbackContext) {
            GameEvents.INSTANCE.togglePauseMenu();
        }

        private void toggleBuildMenu(InputAction.CallbackContext callbackContext) {
            if (currentShip == null || placingBuilding) {
                return;
            }

            GameEvents.INSTANCE.showScreen(GameScreenType.buildMenu);
        }

        public float getLockOnPercentage() {
            return lockOnTimer / lockOnTime;
        }
    }
}