﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using DroneControl.Core;
using DroneControl.Ship;
using DroneControl.Ship.Weapon;
using UnityEngine;
using Random = System.Random;

namespace DroneControl.Player {
    public class PlayerShipWeaponController : AbstractShipWeaponController {
        [SerializeField]
        private WeaponController gatlingGun;

        [SerializeField]
        private WeaponController pulseCannon;

        [SerializeField]
        private WeaponController railGun;

        [SerializeField]
        private WeaponController missileLauncher;

        [SerializeField]
        private float minAmmoGain = 0.5f;

        [SerializeField]
        private float maxAmmoGain = 0.7f;

        private readonly Dictionary<WeaponType, WeaponController> weaponTypesMap = new();

        private readonly Random rand = new();

        private WeaponType activeWeaponType;
        private WeaponType lastWeaponType;

        private Camera cam;

        private void Awake() {
            cam = Camera.main;
            activeWeaponType = WeaponType.GATLING_GUN;
            lastWeaponType = WeaponType.GATLING_GUN;

            weaponTypesMap[WeaponType.GATLING_GUN] = gatlingGun;
            weaponTypesMap[WeaponType.RAIL_GUN] = railGun;
            weaponTypesMap[WeaponType.PULSE_CANNON] = pulseCannon;
            weaponTypesMap[WeaponType.MISSILES] = missileLauncher;
        }

        private void Start() {
            GameEvents.INSTANCE.onAmmoReplenished += onAmmoReplenished;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onAmmoReplenished -= onAmmoReplenished;
        }

        private void onAmmoReplenished(AmmoReplenishEvent evt) {
            gainAmmo(evt.gainFactor);
        }

        public WeaponType getActiveWeaponType() {
            return activeWeaponType;
        }

        public WeaponController getActiveWeaponController() {
            return getWeaponController(activeWeaponType);
        }

        public WeaponController getWeaponController(WeaponType type) {
            return weaponTypesMap[type];
        }

        public void switchWeapons(WeaponType weaponType) {
            lastWeaponType = activeWeaponType;
            activeWeaponType = weaponType;
            fireWeaponSwitchedEvent();
        }

        private void fireWeaponSwitchedEvent() {
            GameEvents.INSTANCE.weaponTypeChanged(new WeaponSwitchedEvent(
                activeWeaponType,
                gameObject,
                weaponTypesMap[activeWeaponType].getCurrentAmmoPercentage())
            );
        }

        public void switchToLastActiveWeapons() {
            activeWeaponType = lastWeaponType;
            fireWeaponSwitchedEvent();
        }


        public void fireActiveWeapons(GameObject target, Vector2 cursorPosition) {
            Ray ray = cam.ScreenPointToRay(cursorPosition);
            weaponTypesMap[activeWeaponType].fireWeapon(ray.direction, gameObject, target);
        }

        public Vector3 getTargetLeadPositionForActiveWeapons(Rigidbody target) {
            if (activeWeaponType == WeaponType.MISSILES || activeWeaponType == WeaponType.RAIL_GUN) {
                return target.position;
            }

            return weaponTypesMap[activeWeaponType].getTargetLeadPosition(target);
        }

        private void gainAmmo(float gainFactor) {
            List<WeaponController> activeWeapons = weaponTypesMap.Keys
                .Where(key => key != WeaponType.MISSILES)
                .Select(key => weaponTypesMap[key])
                .ToList();

            float emptyPercentagesSum =
                activeWeapons.Count - activeWeapons.Select(w => w.getCurrentAmmoPercentage()).Sum();
            int emptyPercentage = Mathf.FloorToInt(100f * emptyPercentagesSum / activeWeapons.Count);
            float additionalAmmoPercentage = gainFactor * rand.Next(Mathf.CeilToInt(emptyPercentage * minAmmoGain),
                Mathf.CeilToInt(emptyPercentage * maxAmmoGain)) / 100f;

            foreach (WeaponController weaponController in activeWeapons) {
                float currentAmmoPercentage = weaponController.getCurrentAmmoPercentage();

                bool isFull = Math.Abs(currentAmmoPercentage - 1f) < 0.001f;
                if (isFull) {
                    continue;
                }

                float targetPercentage =
                    currentAmmoPercentage + (additionalAmmoPercentage * (1f - currentAmmoPercentage));
                int targetAmmo = Mathf.FloorToInt(weaponController.getAmmoCapacity() * targetPercentage);
                weaponController.addAmmo(targetAmmo - weaponController.getCurrentAmmo());
            }
        }
    }
}