﻿using System.Collections.Generic;
using DroneControl.Ship;
using UnityEngine;
using Random = System.Random;

namespace DroneControl.Player {
    public class SpaceDustController : MonoBehaviour {
        private Random random = new();

        [SerializeField]
        private GameObject particlePrefab;

        [SerializeField]
        private int count = 12;

        private readonly List<ParticleSystem> particleSystems = new();

        private ShipMovementController movementController;
        private Rigidbody shipRigidBody;
        private float moveTimer;

        private void Start() {
            for (int i = 0; i < count; i++) {
                GameObject ps = Instantiate(particlePrefab);
                particleSystems.Add(ps.GetComponent<ParticleSystem>());
            }
        }


        private void Update() {
            GameObject currentPlayerShip = PlayerData.INSTANCE.currentPlayerShip;
        
            if (currentPlayerShip == null) {
                movementController = null;

                foreach (ParticleSystem ps in particleSystems) {
                    ParticleSystem.EmissionModule emission = ps.emission;
                    emission.enabled = false;
                }

                return;
            }

            if (movementController == null) {
                movementController = currentPlayerShip.GetComponent<ShipMovementController>();
                shipRigidBody = currentPlayerShip.GetComponent<Rigidbody>();

                foreach (ParticleSystem ps in particleSystems) {
                    ps.transform.parent = currentPlayerShip.transform.parent;
                }
            }

            moveTimer += Time.deltaTime;

            if (moveTimer >= 0.1f) {
                moveTimer = 0f;
                Transform shipTransform = currentPlayerShip.transform;

                foreach (ParticleSystem ps in particleSystems) {
                    Transform particleSystemTransform;
                    (particleSystemTransform = ps.transform).position =
                        shipTransform.TransformPoint(new Vector3(randomCoord(), randomCoord(), 0f));
                    particleSystemTransform.position += shipRigidBody.velocity.normalized * 200f;
                }
            }

            float speed = movementController.forwardThrustPercentage;
            float minSpeed = 0.001f;

            if (speed < minSpeed) {
                speed = movementController.strafeLeft || movementController.strafeRight ? 0.2f : speed;
            }

            float maxLife = 30f;

            foreach (ParticleSystem ps in particleSystems) {
                ParticleSystem.EmissionModule emission = ps.emission;
                emission.enabled = speed >= 0.001f;

                float life = maxLife - (speed * maxLife) + 3f;
                ParticleSystem.MainModule main = ps.main;
                main.startLifetime = life;
            }
        }

        private int randomCoord() {
            int coord = random.Next(20);
            int sign = random.Next(2) == 1 ? -1 : 1;
            return sign * coord;
        }
    }
}