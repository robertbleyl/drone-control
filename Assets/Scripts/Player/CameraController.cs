﻿using System.Collections;
using Cinemachine;
using Core.Events;
using DroneControl.Core;
using UnityEngine;

namespace DroneControl.Player {
    public class CameraController : MonoBehaviour {
        [SerializeField]
        private Animator animator;

        [SerializeField]
        private CinemachineVirtualCamera flightCamera;

        private GameObject currentShip;

        private void Start() {
            GameEvents.INSTANCE.onPlayerShipChange += onPlayerShipChange;
            GameEvents.INSTANCE.onShowScreen += onShowScreen;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onPlayerShipChange -= onPlayerShipChange;
            GameEvents.INSTANCE.onShowScreen -= onShowScreen;
        }

        private void onPlayerShipChange(GameObject ship) {
            currentShip = ship;

            if (ship == null) {
                Time.timeScale = 1f;
                return;
            }

            flightCamera.Follow = ship.transform;
            flightCamera.LookAt = ship.transform;
        }

        private void onShowScreen(GameScreenType type) {
            switch (type) {
                case GameScreenType.hud:
                case GameScreenType.buildMenu:
                    animator.Play(type.ToString());
                    StartCoroutine(waitForCameraTransition(type));
                    break;
                case GameScreenType.pauseMenu:
                    Time.timeScale = 0f;
                    break;
            }
        }

        private IEnumerator waitForCameraTransition(GameScreenType type) {
            yield return new WaitUntil(() => !animator.IsInTransition(0));

            yield return new WaitForSecondsRealtime(0.5f);

            switch (type) {
                case GameScreenType.hud:
                    Time.timeScale = 1f;
                    break;
                case GameScreenType.buildMenu:
                    Time.timeScale = currentShip == null ? 1f : 0f;
                    break;
            }
        }
    }
}