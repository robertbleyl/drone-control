using Cinemachine;
using Core;
using Core.Events;
using DroneControl.Core;
using DroneControl.Ship;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DroneControl.Player {
    public class BuildMenuInputController : MonoBehaviour {
        private const float SPAWN_INPUT_DELAY = 0.5f;

        [SerializeField]
        private float scrollSpeed = 1500f;

        [SerializeField]
        private GameObject playerShipPrefab;

        [SerializeField]
        private PlayerInput playerInput;

        [SerializeField]
        private CinemachineVirtualCamera virtualCamera;

        public bool disableSpawn;

        private CursorController cursorController;

        private float spawnInputDelayTimer;
        private bool playerHasActiveShip;

        private float horizontalScrollAmount;
        private float verticalScrollAmount;

        private GameObject hoveredDrone;
        private GameObject hoveredBuildingButton;

        private void Start() {
            GameEvents.INSTANCE.onShowScreen += screenChanged;
            GameEvents.INSTANCE.onPlayerShipChange += onPlayerShipChange;
            GameEvents.INSTANCE.onHoverDrone += onHoverDrone;
            GameEvents.INSTANCE.onHoverBuildingButton += onHoverBuildingButton;

            initInput();
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onShowScreen -= screenChanged;
            GameEvents.INSTANCE.onPlayerShipChange -= onPlayerShipChange;
            GameEvents.INSTANCE.onHoverDrone -= onHoverDrone;
            GameEvents.INSTANCE.onHoverBuildingButton -= onHoverBuildingButton;

            playerInput.actions["SpawnAtDrone"].performed -= changePlayerShip;
            playerInput.actions["HideBuildMenu"].performed -= hideBuildMenu;
            playerInput.actions["ToggleBuildAndPauseMenu"].performed += togglePauseMenu;

            playerInput.actions["ScrollHorizontal"].performed -= scrollHorizontal;
            playerInput.actions["ScrollHorizontal"].canceled -= cancelHorizontalScroll;

            playerInput.actions["ScrollVertical"].performed -= scrollVertical;
            playerInput.actions["ScrollVertical"].canceled -= cancelVerticalScroll;
        }

        private void initInput() {
            playerInput.actions["SpawnAtDrone"].performed += changePlayerShip;
            playerInput.actions["HideBuildMenu"].performed += hideBuildMenu;
            playerInput.actions["ToggleBuildAndPauseMenu"].performed += togglePauseMenu;

            playerInput.actions["ScrollHorizontal"].performed += scrollHorizontal;
            playerInput.actions["ScrollHorizontal"].canceled += cancelHorizontalScroll;

            playerInput.actions["ScrollVertical"].performed += scrollVertical;
            playerInput.actions["ScrollVertical"].canceled += cancelVerticalScroll;
        }

        private void screenChanged(GameScreenType type) {
            if (type == GameScreenType.buildMenu) {
                playerInput.SwitchCurrentActionMap(InputConstants.INPUT_MAP_BUILD_MENU_INPUTS);
            }
        }

        private void onPlayerShipChange(GameObject ship) {
            playerHasActiveShip = ship != null;
        }

        private void onHoverDrone(DroneHoverEvent evt) {
            if (hoveredDrone == evt.drone && !evt.hovering) {
                hoveredDrone = null;
            }
            else if (evt.hovering) {
                hoveredDrone = evt.drone;
            }
        }

        private void onHoverBuildingButton(BuildingButtonHoverEvent evt) {
            if (hoveredBuildingButton == evt.button && !evt.hovering) {
                hoveredBuildingButton = null;
            }
            else if (evt.hovering) {
                hoveredBuildingButton = evt.button;
            }
        }

        public void OnDisable() {
            playerInput.actions.FindActionMap(InputConstants.INPUT_MAP_BUILD_MENU_INPUTS).Disable();
        }

        private void changePlayerShip(InputAction.CallbackContext cxt) {
            if (disableSpawn || hoveredBuildingButton != null || hoveredDrone == null) {
                return;
            }

            GameObject newPlayerShip = Instantiate(playerShipPrefab, transform, true);

            ShipMovementController movementController = newPlayerShip.GetComponent<ShipMovementController>();

            movementController.forwardThrustPercentage =
                hoveredDrone.GetComponent<ShipMovementController>().forwardThrustPercentage;

            newPlayerShip.transform.position = hoveredDrone.transform.position;
            newPlayerShip.transform.rotation = hoveredDrone.transform.rotation;

            GameEvents.INSTANCE.replaceDrone(hoveredDrone);
            Destroy(hoveredDrone);

            spawnInputDelayTimer = SPAWN_INPUT_DELAY;

            GameEvents.INSTANCE.changePlayerShip(newPlayerShip);
            GameEvents.INSTANCE.changeTarget(null);
            GameEvents.INSTANCE.showScreen(GameScreenType.hud);
        }

        private void hideBuildMenu(InputAction.CallbackContext cxt) {
            if (playerHasActiveShip) {
                GameEvents.INSTANCE.showScreen(GameScreenType.hud);
            }
        }

        private void togglePauseMenu(InputAction.CallbackContext cxt) {
            GameEvents.INSTANCE.togglePauseMenu();
        }

        private void scrollHorizontal(InputAction.CallbackContext ctx) {
            horizontalScrollAmount = ctx.ReadValue<float>();
        }

        private void scrollVertical(InputAction.CallbackContext ctx) {
            verticalScrollAmount = ctx.ReadValue<float>();
        }

        private void cancelVerticalScroll(InputAction.CallbackContext cxt) {
            verticalScrollAmount = 0f;
        }

        private void cancelHorizontalScroll(InputAction.CallbackContext cxt) {
            horizontalScrollAmount = 0f;
        }

        private void Update() {
            if (spawnInputDelayTimer > 0) {
                spawnInputDelayTimer -= Time.deltaTime;
            }

            updateCameraPos();
        }

        private void updateCameraPos() {
            if (horizontalScrollAmount == 0f && verticalScrollAmount == 0f) {
                return;
            }

            Vector3 camPos = virtualCamera.transform.position;
            camPos.x += horizontalScrollAmount * Time.unscaledDeltaTime * scrollSpeed;
            camPos.z += verticalScrollAmount * Time.unscaledDeltaTime * scrollSpeed;

            if (checkCameraScrollPos(camPos)) {
                virtualCamera.transform.position = camPos;
            }
        }

        private bool checkCameraScrollPos(Vector3 camPos) {
            float x = camPos.x;
            float z = camPos.z;

            float minX = BuildGridBoundaries.minScrollX;
            float maxX = BuildGridBoundaries.maxScrollX;
            float minZ = BuildGridBoundaries.minScrollZ;
            float maxZ = BuildGridBoundaries.maxScrollZ;

            return x > minX && x < maxX && z > minZ && z < maxZ;
        }
    }
}