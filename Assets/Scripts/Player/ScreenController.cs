﻿using System.Collections.Generic;
using Core;
using Core.Events;
using DroneControl.Core;
using DroneControl.Player;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Player {
    public class ScreenController : MonoBehaviour {
        [SerializeField]
        private Sprite crosshair;

        [SerializeField]
        private Sprite defaultCursor;

        private readonly Dictionary<GameScreenType, ScreenData> screens = new();

        private CursorController cursorController;

        private ScreenData activeScreen;

        private GameScreenType previousScreenType;

        private void Start() {
            cursorController = GameObject.FindGameObjectWithTag(Tags.CursorController).GetComponent<CursorController>();
            bool isInitScene = SceneManager.GetActiveScene().name.Equals("Init");

            Vector2 halfScale = new(0.5f, 0.5f);

            if (!isInitScene) {
                addScreen(GameScreenType.hud, crosshair, true, Vector2.one);
                addScreen(GameScreenType.buildMenu, defaultCursor, false, halfScale);
                addScreen(GameScreenType.playerDestroyedScreen, defaultCursor, false, halfScale);
                addScreen(GameScreenType.endGame, defaultCursor, false, halfScale);
            }

            addScreen(GameScreenType.pauseMenu, defaultCursor, false, halfScale);

            GameEvents.INSTANCE.onShowScreen += show;
            GameEvents.INSTANCE.onTogglePauseMenu += togglePauseMenu;

            show(isInitScene ? GameScreenType.pauseMenu : GameScreenType.buildMenu);
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onShowScreen -= show;
            GameEvents.INSTANCE.onTogglePauseMenu -= togglePauseMenu;
        }


        private void addScreen(GameScreenType type,
            Sprite cursor,
            bool centeredCursor,
            Vector2 cursorScale) {
            CanvasGroup canvasGroup = GameObject.FindGameObjectWithTag(type.ToString()).GetComponent<CanvasGroup>();
            ScreenData screen = new(type, canvasGroup, cursor, centeredCursor, cursorScale);
            screens[type] = screen;

            activeScreen = screen;
            activeScreen.deactivate();
        }

        private void show(GameScreenType type) {
            previousScreenType = activeScreen.getScreenType();

            activeScreen.deactivate();
            activeScreen = screens[type];
            activeScreen.activate(cursorController);
        }

        private void togglePauseMenu() {
            GameEvents.INSTANCE.showScreen(activeScreen.getScreenType() != GameScreenType.pauseMenu
                ? GameScreenType.pauseMenu
                : previousScreenType);
        }

        public GameScreenType getActiveScreenType() {
            return activeScreen.getScreenType();
        }
    }
}