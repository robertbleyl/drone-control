﻿using Core.Events;
using FMODUnity;
using UnityEngine;

namespace DroneControl.Player {
    public class LocalPlayer : MonoBehaviour {
        [SerializeField]
        private float startingMinerals;

        [SerializeField]
        public bool godModeEnabled;

        [SerializeField]
        private EventReference weaponHitAudioEvent;

        private void Start() {
            PlayerData.INSTANCE.initMinerals(startingMinerals);
            PlayerData.INSTANCE.godModeEnabled = godModeEnabled;

            GameEvents.INSTANCE.onPlayerShipChange += onPlayerShipChange;
            GameEvents.INSTANCE.onWeaponHitShip += weaponHit;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onPlayerShipChange -= onPlayerShipChange;
            GameEvents.INSTANCE.onWeaponHitShip -= weaponHit;
        }

        private void onPlayerShipChange(GameObject ship) {
            PlayerData.INSTANCE.currentPlayerShip = ship;
        }

        private void weaponHit(WeaponHitEvent evt) {
            if (evt.firingShip ==  PlayerData.INSTANCE.currentPlayerShip) {
                RuntimeManager.PlayOneShot(weaponHitAudioEvent);
            }
        }
    }
}