﻿using UnityEngine;

namespace DroneControl.Player {
    public static class MissileLockOnArea {
        public static bool missileLockOnPossible(GameObject currentTarget, Vector2 mousePos) {
            if (currentTarget == null) {
                return false;
            }

            Vector2 screenPos = Camera.main.WorldToScreenPoint(currentTarget.transform.position);
            float rhombusSize = 64f;

            Vector2 A = mousePos - new Vector2(rhombusSize, 0f);
            Vector2 C = mousePos + new Vector2(rhombusSize, 0f);

            Vector2 B = mousePos - new Vector2(0, rhombusSize);
            Vector2 D = mousePos + new Vector2(0, rhombusSize);

            Vector2 Q = 0.5f * (A + C);
            float a = 0.5f * (Vector2.Distance(A, C));
            float b = 0.5f * (Vector2.Distance(B, D));
            Vector2 U = (C - A) / (2 * a);
            Vector2 V = (D - B) / (2 * b);

            Vector2 W = screenPos - Q;
            float xAbs = Mathf.Abs(Vector2.Dot(W, U));
            float yAbs = Mathf.Abs(Vector2.Dot(W, V));

            return xAbs / a + yAbs / b <= 1f;
        }
    }
}