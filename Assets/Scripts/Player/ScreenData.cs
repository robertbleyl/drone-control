using DroneControl.Core;
using DroneControl.Player;
using UnityEngine;

namespace Player {
    public class ScreenData {
        private readonly GameScreenType type;
        private readonly CanvasGroup canvasGroup;
        private readonly Sprite cursor;
        private readonly bool centeredCursor;
        private readonly Vector2 cursorScale;

        public ScreenData(GameScreenType type,
            CanvasGroup canvasGroup,
            Sprite cursor,
            bool centeredCursor,
            Vector2 cursorScale) {
            this.type = type;
            this.canvasGroup = canvasGroup;
            this.cursor = cursor;
            this.centeredCursor = centeredCursor;
            this.cursorScale = cursorScale;
        }

        public void activate(CursorController cursorController) {
            canvasGroup.alpha = 1f;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;

            cursorController.updateCursorImage(cursor, centeredCursor, type == GameScreenType.hud, cursorScale);
        }

        public void deactivate() {
            canvasGroup.alpha = 0f;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }

        public GameScreenType getScreenType() {
            return type;
        }
    }
}