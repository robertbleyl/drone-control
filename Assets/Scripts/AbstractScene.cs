﻿using Core;
using Core.Events;
using DroneControl.Core;
using DroneControl.Player;
using DroneControl.UI.EndGameScreen;
using UI.BuildMenu;
using UI.HUD;
using UnityEngine;

public class AbstractScene : MonoBehaviour {
    protected const float DEFAULT_NOTIFICATION_TIME = 3f;
    protected const float SUCCESS_END_NOTIFICATION_TIME = 5f;
    protected const float FAILURE_END_NOTIFICATION_TIME = 3.5f;

    protected LocalPlayer localPlayer;
    protected EndGameScreenController endGameScreenController;
    protected MusicController musicController;
    protected HUDController hudController;
    protected BuildMenuController buildMenuController;

    protected string nextMissionSceneName;

    protected float stageTimer;
    protected bool endStarted;
    protected bool finished;
    protected bool lost;

    protected Camera cam;

    protected GameObject currentShip;

    protected virtual void Start() {
        cam = Camera.main;
        localPlayer = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<LocalPlayer>();
        endGameScreenController =
            GameObject.FindGameObjectWithTag(Tags.endGame).GetComponent<EndGameScreenController>();
        musicController = GameObject.FindGameObjectWithTag(Tags.Music).GetComponent<MusicController>();
        hudController = GameObject.FindGameObjectWithTag(Tags.hud).GetComponent<HUDController>();
        buildMenuController = GameObject.FindGameObjectWithTag(Tags.buildMenu).GetComponent<BuildMenuController>();

        GameObject.FindGameObjectWithTag(Tags.Music).GetComponent<MusicController>().setIntensity(0);

        GameEvents.INSTANCE.onPlayerShipChange += onPlayerShipChange;
        GameEvents.INSTANCE.onStartShipDestruction += checkDestroyed;
    }

    private void OnDestroy() {
        GameEvents.INSTANCE.onPlayerShipChange -= onPlayerShipChange;
        GameEvents.INSTANCE.onStartShipDestruction -= checkDestroyed;
    }

    private void onPlayerShipChange(GameObject ship) {
        currentShip = ship;
    }

    private void checkDestroyed(GameObject ship) {
        if (!endStarted && ship == currentShip && PlayerData.INSTANCE.hasLost) {
            startFailureEnd();
        }
    }

    protected virtual void Update() {
        if (endStarted) {
            stageTimer -= Time.deltaTime;

            if (stageTimer <= 0f) {
                finish();
            }
        }
    }

    protected void startFailureEnd() {
        cam.transform.parent = transform.parent;
        musicController.stopMusic(FAILURE_END_NOTIFICATION_TIME);
        stageTimer = FAILURE_END_NOTIFICATION_TIME;
        endStarted = true;
        lost = true;
    }

    protected void startSuccessEnd() {
        musicController.stopMusic(SUCCESS_END_NOTIFICATION_TIME);
        stageTimer = SUCCESS_END_NOTIFICATION_TIME;
        endStarted = true;
        lost = false;
    }

    private void finish() {
        if (finished) {
            return;
        }

        finished = true;
        endGameScreenController.show(lost, nextMissionSceneName);
        GameEvents.INSTANCE.showScreen(GameScreenType.endGame);
    }
}