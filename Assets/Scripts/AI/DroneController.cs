﻿using System.Collections;
using System.Collections.Generic;
using AI;
using Core;
using Core.Events;
using DroneControl.Core;
using DroneControl.Difficulty;
using DroneControl.Player;
using DroneControl.Ship;
using Player;
using UnityEngine;

namespace DroneControl.AI {
    public class DroneController : MonoBehaviour {

        [SerializeField]
        private GameObject circlePrefab;

        [SerializeField]
        private GameObject hoverCirclePrefab;

        [SerializeField]
        public DronePath dronePath;

        [SerializeField]
        private float mineralCapacity = 25f;

        [SerializeField]
        private float miningRate = 4f;

        [SerializeField]
        public bool spawnable = true;

        private CursorController cursorController;
        private AIMovementController aIMovementController;
        private ShipMovementController shipMovementController;
        private AntiGravController antiGravController;

        private GameObject circle;
        private GameObject hoverCircle;

        public bool mining;
        public GameObject targetAsteroid;

        private float minedMinerals;
        private float currentMineralRate;

        private Camera cam;

        private void Start() {
            Drones.INSTANCE.addDrone(gameObject);

            cam = Camera.main;
            aIMovementController = GetComponent<AIMovementController>();
            shipMovementController = GetComponent<ShipMovementController>();
            antiGravController = GetComponent<AntiGravController>();

            cursorController = GameObject.FindGameObjectWithTag(Tags.CursorController).GetComponent<CursorController>();

            aIMovementController.init(shipMovementController);

            circle = Instantiate(circlePrefab);
            hoverCircle = Instantiate(hoverCirclePrefab);

            GameObject buildMenuOverlayCanvas = GameObject.FindGameObjectWithTag(Tags.BuildMenuOverlayCanvas);

            if (buildMenuOverlayCanvas != null) {
                circle.transform.SetParent(buildMenuOverlayCanvas.transform);
                hoverCircle.transform.SetParent(buildMenuOverlayCanvas.transform);
                StartCoroutine(updateActiveScreen());
            }
            else {
                circle.SetActive(false);
                hoverCircle.SetActive(false);
            }

            GameEvents.INSTANCE.onShowScreen += onShowScreen;
            GameEvents.INSTANCE.onGameObjectDestroyed += onGameObjectDestroyed;
            GameEvents.INSTANCE.onDifficultyChanged += onDifficultyChanged;
            GameEvents.INSTANCE.onStartShipDestruction += onStartShipDestruction;

            if (dronePath != null) {
                updateTarget();
            }

            onDifficultyChanged();
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onShowScreen -= onShowScreen;
            GameEvents.INSTANCE.onGameObjectDestroyed -= onGameObjectDestroyed;
            GameEvents.INSTANCE.onDifficultyChanged -= onDifficultyChanged;
            GameEvents.INSTANCE.onStartShipDestruction -= onStartShipDestruction;

            Destroy(circle);
            Destroy(hoverCircle);
        }

        private void onDifficultyChanged() {
            currentMineralRate = miningRate * Difficulty.Difficulty.getValue(DifficultyItem.DRONE_MINING_RATE);
        }

        private void onStartShipDestruction(GameObject obj) {
            if (gameObject == obj) {
                PlayerData.INSTANCE.increaseLostDrones();
            }
        }

        private IEnumerator updateActiveScreen() {
            yield return new WaitForSecondsRealtime(0.1f);

            GameScreenType activeScreenType = GameObject.FindGameObjectWithTag(Tags.Player)
                .GetComponent<ScreenController>()
                .getActiveScreenType();

            onShowScreen(activeScreenType);
        }

        private void onShowScreen(GameScreenType screenType) {
            if (screenType == GameScreenType.buildMenu && PlayerData.INSTANCE.currentPlayerShip == null) {
                circle.SetActive(true);
                hoverCircle.SetActive(false);
            }
            else {
                circle.SetActive(false);
                hoverCircle.SetActive(false);
            }
        }

        private void onGameObjectDestroyed(DestructionEvent evt) {
            if (evt.gameObject == PlayerData.INSTANCE.currentPlayerShip) {
                circle.SetActive(true);
                hoverCircle.SetActive(false);
            }
        }

        public void updateTarget() {
            GameObject asteroidField = dronePath.asteroidField;
            List<GameObject> asteroidsWithMinerals = new();

            foreach (Transform childTransform in asteroidField.transform) {
                asteroidsWithMinerals.Add(childTransform.gameObject);
            }

            Vector3 dronePos = transform.position;
            asteroidsWithMinerals.Sort((a, b) =>
                Vector3.Distance(a.transform.position, dronePos)
                    .CompareTo(Vector3.Distance(b.transform.position, dronePos)));

            int max = asteroidsWithMinerals.Count < 5 ? asteroidsWithMinerals.Count : 5;
            targetAsteroid = asteroidsWithMinerals[Random.Range(0, max)];

            GetComponent<AIMovementController>().targetLocation = targetAsteroid.transform.position;
        }

        private void Update() {
            mining = false;
            shipMovementController.forwardThrustPercentage = 1f;

            bool nearSpawn = nearSpawnPoint();
            antiGravController.enabled = !nearSpawn;

            if (nearSpawn) {
                checkDropOff();
            }
            else {
                checkMining();
            }

            if (circle.activeSelf || hoverCircle.activeSelf) {
                if (PlayerData.INSTANCE.currentPlayerShip == null && spawnable) {
                    checkHoverCircle();
                }

                circle.transform.localScale = Vector3.one;
                hoverCircle.transform.localScale = Vector3.one;
                Vector3 screenPos = cam.WorldToScreenPoint(transform.position);

                if (screenPos.z > 0f) {
                    screenPos.z = 0f;
                    circle.transform.position = screenPos;
                    hoverCircle.transform.position = screenPos;
                }
            }
        }

        private void checkDropOff() {
            if (minedMinerals > 0f) {
                shipMovementController.forwardThrustPercentage = 0f;
                float depositedMinerals = Time.deltaTime * currentMineralRate;

                if (depositedMinerals > 0) {
                    minedMinerals -= depositedMinerals;
                    PlayerData.INSTANCE.addMinerals(depositedMinerals);

                    if (minedMinerals < 0f) {
                        minedMinerals = 0f;
                    }

                    updateTargetDisplayMinerals();
                }
            }
            else {
                aIMovementController.targetLocation = targetAsteroid.transform.position;
                rotateTillFacingTarget();
            }
        }

        private void checkMining() {
            bool nearTarget = nearTargetAsteroid();
            antiGravController.enabled = !nearTarget;

            if (minedMinerals < mineralCapacity) {
                if (nearTarget) {
                    mining = true;
                    shipMovementController.forwardThrustPercentage = 0f;
                    minedMinerals += Time.deltaTime * currentMineralRate;

                    updateTargetDisplayMinerals();
                }
            }
            else {
                aIMovementController.targetLocation = dronePath.spawnPoint.transform.position;

                if (nearTarget) {
                    rotateTillFacingTarget();
                }
            }
        }

        private void updateTargetDisplayMinerals() {
            GameEvents.INSTANCE.targetDisplayProgressChanged(
                new TargetDisplayProgressChangedEvent(gameObject, minedMinerals / mineralCapacity));
        }

        private void checkHoverCircle() {
            Vector3 dronePos = cam.WorldToScreenPoint(transform.position);
            dronePos.z = 0f;
            float distance = Vector3.Distance(cursorController.getCurrentCursorPosition(), dronePos);

            if (distance < Screen.width / 30f) {
                circle.SetActive(false);
                hoverCircle.SetActive(true);
                GameEvents.INSTANCE.hoverDrone(new DroneHoverEvent(gameObject, true));
            }
            else {
                circle.SetActive(true);
                hoverCircle.SetActive(false);
                GameEvents.INSTANCE.hoverDrone(new DroneHoverEvent(gameObject, false));
            }
        }

        private void rotateTillFacingTarget() {
            Transform droneTransform = transform;
            float angle = Vector3.Angle(aIMovementController.targetLocation - droneTransform.position,
                droneTransform.forward);

            if (angle > 20f) {
                shipMovementController.forwardThrustPercentage = 0f;
            }
        }

        private bool nearSpawnPoint() {
            float dist = Vector3.Distance(transform.position, dronePath.spawnPoint.transform.position);
            return dist < 80f;
        }

        private bool nearTargetAsteroid() {
            Collider col = targetAsteroid.GetComponentInChildren<Collider>();
            Vector3 position = transform.position;

            Vector3 closestPoint = col.ClosestPointOnBounds(position);
            float dist = Vector3.Distance(position, closestPoint);
            return dist < 30f;
        }
    }
}