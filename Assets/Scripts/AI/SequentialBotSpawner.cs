﻿using System.Collections;
using Core.Events;
using UnityEngine;

namespace DroneControl.AI {
    public class SequentialBotSpawner : AbstractBotSpawner {

        [SerializeField]
        private int botCount = 3;

        protected override void spawnBot () {
            base.spawnBot ();
            botCount--;
            countTextUI.text = botCount + "";
        }

        protected override bool shipDestroyed (DestructionEvent e) {
            bool destroyed = base.shipDestroyed (e);

            if (destroyed && botCount > 0) {
                spawnBot ();
            }

            if (destroyed && botCount == 0) {
                StartCoroutine (destroySpawner ());
            }

            return destroyed;
        }

        private IEnumerator destroySpawner () {
            yield return new WaitForSeconds (0.1f);
            Destroy (gameObject);
        }
    }
}