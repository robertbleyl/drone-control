﻿using System.Collections.Generic;
using System.Linq;
using Core;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace AI {
    public class AsteroidGenerator : MonoBehaviour {
        [SerializeField]
        private int count = 200;

        [SerializeField]
        private int minSize = 15;

        [SerializeField]
        private int maxSize = 50;

        [SerializeField]
        private GameObject generationBoundPrefab;

        [SerializeField]
        private GameObject[] asteroids;

        [SerializeField]
        private BoxCollider container;

        private List<GameObject> existing;
        private readonly List<GameObject> dronePathBoundObject = new();

        private int diffSize;

        private void Start() {
            diffSize = maxSize - minSize;

            initExitingBounds();

            for (int i = 0; i < count; i++) {
                GameObject asteroid = generateAsteroid();

                if (asteroid != null) {
                    asteroid.transform.parent = transform;
                }
            }

            foreach (GameObject target in dronePathBoundObject) {
                Destroy(target);
            }

            Destroy(container);
            Destroy(this);
        }

        private void initExitingBounds() {
            existing = GameObject.FindGameObjectsWithTag(Tags.Asteroid).ToList();

            GameObject[] motherShips = GameObject.FindGameObjectsWithTag(Tags.MotherShip);
            existing.AddRange(motherShips);

            GameObject[] paths = GameObject.FindGameObjectsWithTag(Tags.DronePath);

            foreach (GameObject path in paths) {
                addBoundsForDronePath(path.GetComponent<DronePath>());
            }

            GameObject generationBound = Instantiate(generationBoundPrefab);
            BoxCollider boundCollider = generationBound.GetComponent<BoxCollider>();
            boundCollider.center = Vector3.zero;
            boundCollider.size = new Vector3(10000f, 80f, 10000f);
            existing.Add(generationBound);
        }

        private void addBoundsForDronePath(DronePath path) {
            addBound(path.spawnPoint.transform.position, path.asteroidField.transform.position);
        }

        private void addBound(Vector3 start, Vector3 end) {
            GameObject generationBound = Instantiate(generationBoundPrefab);
            generationBound.transform.position = (start + end) / 2f;

            Vector3 size = generationBound.GetComponent<BoxCollider>().size;
            size.z = (end - start).magnitude;
            generationBound.GetComponent<BoxCollider>().size = size;

            generationBound.transform.LookAt(end);

            dronePathBoundObject.Add(generationBound);
            existing.Add(generationBound);
        }

        private GameObject generateAsteroid() {
            int iterations = 0;

            GameObject prefab = asteroids[Random.Range(0, asteroids.Length)];
            Selection.activeObject = PrefabUtility.InstantiatePrefab(prefab);
            GameObject asteroid = Selection.activeGameObject;

            while (iterations < 200) {
                iterations++;
                Transform asteroidTransform = asteroid.transform;
                asteroidTransform.position = createLocation();
                asteroidTransform.localScale = createScale();
                asteroidTransform.rotation = createRotation();
                Physics.SyncTransforms();

                Bounds vol = asteroid.GetComponentInChildren<Collider>().bounds;

                bool cannotBePlace = existing.Exists(v => v.GetComponentInChildren<Collider>().bounds.Intersects(vol));

                if (cannotBePlace) {
                    continue;
                }

                existing.Add(asteroid);
                return asteroid;
            }

            Destroy(asteroid);

            return null;
        }

        private float next(float bound) {
            if (bound < 0) {
                bound *= -1;
            }

            int next = Random.Range(0, (int)bound);

            if (Random.Range(0, 2) == 1) {
                next = -next;
            }

            return next;
        }

        private Vector3 createLocation() {
            Vector3 position = transform.position;
            float x = position.x + next(container.size.x);
            float y = position.y + next(container.size.y);
            float z = position.z + next(container.size.z);

            return new Vector3(x, y, z);
        }

        private Quaternion createRotation() {
            float x = Random.Range(0f, 1f);
            float y = Random.Range(0f, 1f);
            float z = Random.Range(0f, 1f);
            float w = Random.Range(0f, 1f);

            return new Quaternion(x, y, z, w);
        }

        private Vector3 createScale() {
            float s = minSize + Random.Range(0, diffSize);
            float deviation = 0.1f;

            float x = s + ((Random.Range(0, 2) == 1 ? -deviation : deviation) * s);
            float y = s + ((Random.Range(0, 2) == 1 ? -deviation : deviation) * s);
            float z = s + ((Random.Range(0, 2) == 1 ? -deviation : deviation) * s);

            return new Vector3(x, y, z);
        }
    }
}

#endif