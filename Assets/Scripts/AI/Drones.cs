﻿using System.Collections.Generic;
using Core.Events;
using DroneControl.AI;
using UnityEngine;

namespace AI {
    public class Drones {
        public static readonly Drones INSTANCE = new();

        private readonly List<GameObject> drones = new();

        private Drones() {
            GameEvents.INSTANCE.onReplaceDrone += removeDrone;
            GameEvents.INSTANCE.onGameObjectDestroyed += destroyDrone;
        }

        private void removeDrone(GameObject drone) {
            DroneController droneController = drone.GetComponent<DroneController>();

            if (droneController != null) {
                drones.Remove(drone);
            
                PlayerData.INSTANCE.hasLost = drones.Count == 0 && PlayerData.INSTANCE.getMinerals() < 50f;
            }
        }

        private void destroyDrone(DestructionEvent evt) {
            removeDrone(evt.gameObject);
        }

        public void addDrone(GameObject drone) {
            drones.Add(drone);
        }

        public List<GameObject> getDrones() {
            return drones;
        }

        public void clear() {
            drones.Clear();
        }
    }
}