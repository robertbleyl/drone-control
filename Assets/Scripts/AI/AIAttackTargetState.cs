using DroneControl.Core;
using UnityEngine;

namespace DroneControl.AI {
    public class AIAttackTargetState : AIPlayerState {
        private readonly float startEvadeDistance;
        private readonly float startEvadeTurretDistance;
        private readonly float startEvadeTime;

        private float attackingTime;

        public AIAttackTargetState(AIPlayerData data) : base(data) {
            startEvadeDistance = 150f;
            startEvadeTurretDistance = 120f;
            startEvadeTime = 4f;
        }

        public override void reset() {
            attackingTime = 0f;
        }

        public override AIPlayerStateType nextType() {
            attackingTime += Time.deltaTime;

            if (attackingTime >= startEvadeTime) {
                return AIPlayerStateType.EVADE_TARGET;
            }

            float dist = Vector3.Distance(data.attackTarget.transform.position, data.botShip.transform.position);
            float evadeDist = data.targetIsTurret ? startEvadeTurretDistance : startEvadeDistance;

            if (dist < evadeDist) {
                return AIPlayerStateType.EVADE_TARGET;
            }

            return AIPlayerStateType.ATTACK_TARGET;
        }

        public override void executeState() {
            Vector3 position = data.attackTarget.transform.position;
            data.aIMovementController.targetLocation = position;
            Vector3 toTarget = position - data.botShip.transform.position;

            double angle = Vector3.Angle(data.botShip.transform.forward.normalized, toTarget.normalized);

            if (angle <= AISteering.MAX_VIEW_ANGLE) {
                fireWeapons();
            }
        }

        private void fireWeapons() {
            Vector3 targetLeadLocation = data.weaponController.getTargetLeadPosition(data.attackTargetRigidBody);
            targetLeadLocation = addAimHandicap(targetLeadLocation);
            data.weaponController.fireWeapon(targetLeadLocation - data.botShip.transform.position, data.attackTarget);
        }

        private Vector3 addAimHandicap(Vector3 aimLocation) {
            System.Random rand = AIPlayerData.rand;
            float x = rand.Next(15) * randomSign();
            float y = rand.Next(15) * randomSign();
            float z = rand.Next(15) * randomSign();
            return aimLocation + new Vector3(x, y, z);
        }

        private static int randomSign() {
            return AIPlayerData.rand.Next(2) == 1 ? 1 : -1;
        }
    }
}