﻿using Core.Events;
using DroneControl.AI;
using UnityEngine;
using Random = System.Random;

namespace AI {
    public class DroneSpawnController : MonoBehaviour {
        private readonly Random rand = new();

        [SerializeField]
        private DronePath dronePath;

        [SerializeField]
        private GameObject dronePrefab;

        [SerializeField]
        private bool autoRespawnDestroyedDrones;

        private Vector3 spawnArea;

        private void Start() {
            Collider spawnAreaCollider = GetComponent<BoxCollider>();
            spawnArea = spawnAreaCollider.bounds.extents;
            Destroy(spawnAreaCollider);

            if (autoRespawnDestroyedDrones) {
                GameEvents.INSTANCE.onGameObjectDestroyed += checkDroneDestroyed;
                GameEvents.INSTANCE.onReplaceDrone += replaceDrone;
            }
        }

        private void checkDroneDestroyed(DestructionEvent e) {
            if (e.gameObject.GetComponent<DroneController>() != null) {
                spawnDrone();
            }
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onGameObjectDestroyed -= checkDroneDestroyed;
            GameEvents.INSTANCE.onReplaceDrone -= replaceDrone;
        }

        private void replaceDrone(GameObject drone) {
            spawnDrone();
        }

        public GameObject spawnDrone() {
            GameObject drone = Instantiate(dronePrefab, transform.parent.parent, true);

            DroneController droneController = drone.GetComponent<DroneController>();
            droneController.dronePath = dronePath;
            droneController.updateTarget();

            SphereCollider droneCollider = drone.GetComponentInChildren<SphereCollider>();

            for (int i = 0; i < 1000; i++) {
                int x = getRandomCoordinate((int)spawnArea.x);
                int y = getRandomCoordinate((int)spawnArea.y);
                int z = getRandomCoordinate((int)spawnArea.z);

                drone.transform.position = transform.position + new Vector3(x, y, z);
                float radius = droneCollider.bounds.size.magnitude;
                Collider[] colliders = Physics.OverlapSphere(drone.transform.position, radius);

                if (colliders.Length == 0) {
                    break;
                }
            }

            drone.transform.LookAt(dronePath.asteroidField.transform.position);
            GameEvents.INSTANCE.buildDrone(drone);

            return drone;
        }

        private int getRandomCoordinate(int max) {
            int value = rand.Next(max);

            if (rand.Next(2) == 1) {
                value *= -1;
            }

            return value;
        }
    }
}