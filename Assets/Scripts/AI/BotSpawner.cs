﻿using Core.Events;
using UnityEngine;

namespace DroneControl.AI {
    public class BotSpawner : MonoBehaviour {
        [SerializeField]
        public GameObject botPrefab;

        [SerializeField]
        public GameObject shipPrefab;

        [SerializeField]
        public bool spawnDirectly;

        public void Start() {
            if (spawnDirectly) {
                spawnBot();
            }
        }

        public GameObject spawnBot() {
            GameObject bot = Instantiate(botPrefab, transform.parent, true);

            AIPlayerController aIPlayerController = bot.GetComponent<AIPlayerController>();
            aIPlayerController.init(shipPrefab, transform.position);
            aIPlayerController.getCurrentShip().AddComponent<AntiGravController>();

            if (spawnDirectly) {
                aIPlayerController.getCurrentShip().AddComponent<AIPlayerDebugController>();
            }

            GameEvents.INSTANCE.botSpawned(bot);

            return aIPlayerController.getCurrentShip();
        }
    }
}