﻿using UnityEngine;

namespace DroneControl.AI {
    public class AntiGravController : MonoBehaviour {
        private Rigidbody rigidBody;

        private void Start() {
            rigidBody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate() {
            if (Physics.Raycast(new Ray(transform.position, transform.forward), out RaycastHit hit, 100) &&
                hit.rigidbody != null) {
                rigidBody.AddForce((transform.position - hit.rigidbody.position) * 900f / hit.distance);
            }
        }
    }
}