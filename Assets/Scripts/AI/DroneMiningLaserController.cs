using UnityEngine;

namespace DroneControl.AI {
    public class DroneMiningLaserController : MonoBehaviour {
        [SerializeField]
        private DroneController droneController;

        [SerializeField]
        private LineRenderer lineRenderer;

        private void Update() {
            lineRenderer.enabled = droneController.mining;

            if (droneController.mining) {
                lineRenderer.SetPosition(0, transform.position);
                lineRenderer.SetPosition(1, droneController.targetAsteroid.transform.position);
            }
        }
    }
}