using UnityEngine;

namespace DroneControl.AI {
    public class AIEvadeTargetState : AIPlayerState {
        private readonly float stopEvadeDistance;
        private readonly float stopEvadeTurretDistance;
        private readonly float stopEvadeTime;

        private float evadingTime;

        public AIEvadeTargetState(AIPlayerData data) : base(data) {
            stopEvadeDistance = 400f;
            stopEvadeTurretDistance = 500f;
            stopEvadeTime = 6f;
        }

        public override void reset() {
            evadingTime = 0f;
        }

        public override AIPlayerStateType nextType() {
            evadingTime += Time.deltaTime;

            if (evadingTime >= stopEvadeTime) {
                return AIPlayerStateType.ATTACK_TARGET;
            }

            float dist = Vector3.Distance(data.attackTarget.transform.position, data.botShip.transform.position);
            float evadeDist = data.targetIsTurret ? stopEvadeTurretDistance : stopEvadeDistance;

            if (dist > evadeDist) {
                return AIPlayerStateType.ATTACK_TARGET;
            }

            return AIPlayerStateType.EVADE_TARGET;
        }

        public override void executeState() {
            Vector3 position = data.botShip.transform.position;
            Vector3 toTarget = data.attackTarget.transform.position - position;
            data.aIMovementController.targetLocation = position - toTarget;
        }
    }
}