﻿using UnityEngine;

namespace DroneControl.AI {
    public class TimeBotSpawner : AbstractBotSpawner {
        [SerializeField]
        private int seconds = 30;

        [SerializeField]
        private int maxCount = 8;

        [SerializeField]
        private float timeBetweenSuccessiveSpawns = 2f;

        private float timer;
        private int successiveSpawnCount = 1;
        private float successiveSpawnTimer;
        private int successiveSpawnsLeft;

        protected override void Update() {
            base.Update();
            timer += Time.deltaTime;

            if (timer >= seconds) {
                timer -= seconds;

                successiveSpawnsLeft = successiveSpawnCount;
                successiveSpawnCount *= 2;

                if (successiveSpawnCount > maxCount) {
                    successiveSpawnCount = maxCount;
                }
            }

            if (successiveSpawnsLeft > 0) {
                successiveSpawnTimer -= Time.deltaTime;

                if (successiveSpawnTimer <= 0f) {
                    successiveSpawnTimer += timeBetweenSuccessiveSpawns;

                    spawnBot();
                    successiveSpawnsLeft--;
                }
            }

            int displayTimer = (int)(seconds - timer);
            countTextUI.text = "00:" + (displayTimer < 10 ? "0" : "") + displayTimer;
        }
    }
}