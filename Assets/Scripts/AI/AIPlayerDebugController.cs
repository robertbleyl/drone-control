﻿using DroneControl.Ship;
using UnityEngine;

namespace DroneControl.AI {
    public class AIPlayerDebugController : MonoBehaviour {
        private ShipMovementController movementController;

        private Texture2D crosshair;

        private Camera cam;
        private bool init;

        private void Awake() {
            cam = Camera.main;
            crosshair = Resources.Load<Texture2D>("Crosshair");
        }

        private void OnGUI() {
            if (!init) {
                movementController = GetComponentInChildren<ShipMovementController>();

                if (movementController != null) {
                    GameObject movementControllerGameObject = movementController.gameObject;
                    Transform camTransform = cam.transform;
                    camTransform.parent = movementControllerGameObject.transform;
                    camTransform.position = movementControllerGameObject.transform.position + new Vector3(0f, 10f, -30f);
                    camTransform.rotation = movementControllerGameObject.transform.rotation;

                    init = true;
                }
            }

            if (init) {
                float x = Screen.width * ((movementController.yawPercentage + 1f) / 2f);
                float y = Screen.height * ((movementController.pitchPercentage + 1f) / 2f);

                x = Mathf.Clamp(x, 0, Screen.width - 64);
                y = Mathf.Clamp(y, 0, Screen.height - 64);

                GUI.DrawTexture(new Rect(x, y, 64, 64), crosshair);
            }
        }
    }
}