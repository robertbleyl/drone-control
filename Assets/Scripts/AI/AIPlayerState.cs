﻿using System.Collections.Generic;
using UnityEngine;

namespace DroneControl.AI {
    public abstract class AIPlayerState {
        protected readonly AIPlayerData data;

        protected AIPlayerState(AIPlayerData data) {
            this.data = data;
            reset();
        }

        public abstract void reset();

        public abstract AIPlayerStateType nextType();

        public abstract void executeState();

        protected GameObject getNearestObject(List<GameObject> targets) {
            double minDist = double.MaxValue;
            GameObject nearestTarget = null;

            foreach (GameObject target in targets) {
                if (data.botShip == null || target == null) {
                    continue;
                }

                double dist = Vector3.Distance(data.botShip.transform.position, target.transform.position);

                if (dist < minDist) {
                    minDist = dist;
                    nearestTarget = target;
                }
            }

            return nearestTarget;
        }
    }
}