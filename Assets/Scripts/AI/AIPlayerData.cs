using DroneControl.Ship;
using UnityEngine;
using Random = System.Random;

namespace DroneControl.AI {
    public class AIPlayerData {
        public static readonly Random rand = new();

        public GameObject objective;
        public GameObject attackTarget;
        public Rigidbody attackTargetRigidBody;
        public bool targetIsTurret;
        public GameObject botShip;
        public GameObject[] asteroidMineralFields;

        public AIMovementController aIMovementController;
        public ShipMovementController movementController;
        public AIShipWeaponController weaponController;
    }
}