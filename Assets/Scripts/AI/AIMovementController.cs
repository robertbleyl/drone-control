using System.Collections.Generic;
using DroneControl.Core;
using DroneControl.Ship;
using UnityEngine;

namespace DroneControl.AI {
    public class AIMovementController : MonoBehaviour {
        [SerializeField]
        private List<GameObject> collisionCheckPoints = new();

        public Vector3 targetLocation;

        private ShipMovementController movementController;

        private readonly AISteering steering = new();

        public void init(ShipMovementController movementController) {
            this.movementController = movementController;
        }

        private void Update() {
            if (movementController != null) {
                Vector3 evadeLocation = getEvadeTargetLocation();

                steering.pitchPercentage = movementController.pitchPercentage;
                steering.yawPercentage = movementController.yawPercentage;

                if (evadeLocation != Vector3.zero) {
                    steering.steerToTargetLocation(evadeLocation, transform);
                }
                else {
                    steering.steerToTargetLocation(targetLocation, transform);
                }

                movementController.pitchPercentage = steering.pitchPercentage;
                movementController.yawPercentage = steering.yawPercentage;
            }
        }

        private Vector3 getEvadeTargetLocation() {
            foreach (GameObject point in collisionCheckPoints) {
                Vector3 pos = getEvadeTargetLocation(point.transform.position);

                if (pos != Vector3.zero) {
                    return pos;
                }
            }

            return Vector3.zero;
        }

        private Vector3 getEvadeTargetLocation(Vector3 pos) {
            if (Physics.Raycast(new Ray(pos, transform.forward), out RaycastHit hit, 200f)) {
                Vector3 result = hit.transform.position;
                Transform trans = transform;
                Vector3 position = trans.position;
                Vector3 direction = result - position;
                Vector3 forward = trans.forward;

                float angle = Vector3.Angle(direction, forward);

                float l = direction.magnitude / Mathf.Cos(Mathf.Deg2Rad * angle);

                Vector3 forwardPoint = position + forward;
                Vector3 orthogonal = forwardPoint - result;

                return forwardPoint + orthogonal;
            }

            return Vector3.zero;
        }
    }
}