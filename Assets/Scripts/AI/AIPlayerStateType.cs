namespace DroneControl.AI {
    public enum AIPlayerStateType {
        OBJECTIVE,
        CIRCLE_TARGET,
        ATTACK_TARGET,
        EVADE_TARGET
    }
}