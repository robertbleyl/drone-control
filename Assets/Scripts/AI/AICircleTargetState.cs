﻿using AI;
using DroneControl.Buildings;
using DroneControl.Core;
using UnityEngine;

namespace DroneControl.AI {
    public class AICircleTargetState : AIPlayerState {
        private const float CIRCLE_RADIUS = 600f;

        public AICircleTargetState(AIPlayerData data) : base(data) {
        }

        public override void reset() {
            checkTarget();
        }

        private void checkTarget() {
            if (data.attackTarget == null) {
                GameObject nearestTarget = getAttackTarget();

                if (nearestTarget != null) {
                    data.attackTarget = nearestTarget;
                    data.attackTargetRigidBody = nearestTarget.GetComponent<Rigidbody>();
                    data.targetIsTurret = nearestTarget.GetComponent<TurretController>() != null;
                }
            }
        }

        private GameObject getAttackTarget() {
            GameObject nearTurret = getNearestObject(BuildingsProvider.INSTANCE.getBuildings(BuildingType.TURRET));

            if (nearTurret != null &&
                Vector3.Distance(data.botShip.transform.position, nearTurret.transform.position) < 1000f) {
                return nearTurret;
            }

            return getNearestObject(Drones.INSTANCE.getDrones());
        }

        public override AIPlayerStateType nextType() {
            return AIPlayerStateType.CIRCLE_TARGET;
        }

        public override void executeState() {
            checkTarget();

            if (data.attackTarget == null) {
                return;
            }

            Vector3 circleCenter = data.attackTarget.transform.position;
            Vector3 currentPos = data.botShip.transform.position;
            Vector3 toCenter = circleCenter - currentPos;

            float extension = 0f;

            if (toCenter.magnitude < CIRCLE_RADIUS) {
                extension = CIRCLE_RADIUS;
            }
            else if (toCenter.magnitude < CIRCLE_RADIUS + 5f) {
                extension = CIRCLE_RADIUS / 2f;
            }
            else if (toCenter.magnitude < CIRCLE_RADIUS + 10f) {
                extension = CIRCLE_RADIUS / 3f;
            }

            if (toCenter.magnitude < CIRCLE_RADIUS + 40f) {
                data.weaponController.fireWeapon(toCenter, data.attackTarget);
            }

            currentPos -= toCenter.normalized * extension;

            float angle = Mathf.Sin(CIRCLE_RADIUS / toCenter.magnitude);

            Vector3 toCircle = Quaternion.AngleAxis(Mathf.Rad2Deg * angle, Vector3.up) * toCenter;
            float length = toCenter.magnitude / Mathf.Cos(angle);
            Vector3 pointOnCircle = currentPos + (toCircle.normalized * length);

            // Debug.DrawLine (currentPos, circleCenter, Color.red);
            // Debug.DrawLine (currentPos, pointOnCircle, Color.green);
            // Debug.DrawLine (circleCenter, circleCenter - (toCenter.normalized * CIRCLE_RADIUS), Color.yellow);

            data.aIMovementController.targetLocation = pointOnCircle;
        }
    }
}