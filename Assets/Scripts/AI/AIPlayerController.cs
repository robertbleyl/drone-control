﻿using System.Collections.Generic;
using Core;
using Core.Events;
using DroneControl.Ship;
using UnityEngine;

namespace DroneControl.AI {
    public class AIPlayerController : MonoBehaviour, BotController {
        private AIPlayerData data;
        private AIPlayerState currentState;

        private readonly Dictionary<AIPlayerStateType, AIPlayerState> states = new();

        private AIPlayerStateType fallbackStateType;

        public void init(GameObject shipPrefab, Vector3 spawnLocation) {
            data = new AIPlayerData {
                asteroidMineralFields = GameObject.FindGameObjectsWithTag(Tags.AsteroidMineralField),
                botShip = Instantiate(shipPrefab)
            };

            data.movementController = data.botShip.GetComponent<ShipMovementController>();
            data.movementController.forwardThrustPercentage = 1f;

            data.aIMovementController = data.botShip.GetComponent<AIMovementController>();

            data.weaponController = data.botShip.GetComponent<AIShipWeaponController>();

            data.botShip.transform.parent = transform;
            data.botShip.transform.position = spawnLocation;
            data.aIMovementController.init(data.movementController);

            string title = data.botShip.GetComponent<ShipProperties>().title;

            if (title.Equals("Cyclops")) {
                states[AIPlayerStateType.CIRCLE_TARGET] = new AICircleTargetState(data);
                fallbackStateType = AIPlayerStateType.CIRCLE_TARGET;
            }
            else {
                states[AIPlayerStateType.OBJECTIVE] = new AIObjectiveState(data);
                states[AIPlayerStateType.ATTACK_TARGET] = new AIAttackTargetState(data);
                states[AIPlayerStateType.EVADE_TARGET] = new AIEvadeTargetState(data);
                fallbackStateType = AIPlayerStateType.OBJECTIVE;
            }

            currentState = states[fallbackStateType];

            GameEvents.INSTANCE.onGameObjectDestroyed += checkAttackTargetDestroyed;
            GameEvents.INSTANCE.onReplaceDrone += checkDroneReplaced;
            GameEvents.INSTANCE.onStartShipDestruction += onStartShipDestruction;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onGameObjectDestroyed -= checkAttackTargetDestroyed;
            GameEvents.INSTANCE.onReplaceDrone -= checkDroneReplaced;
            GameEvents.INSTANCE.onStartShipDestruction -= onStartShipDestruction;
        }

        private void onStartShipDestruction(GameObject obj) {
            if (gameObject == obj) {
                enabled = false;
            }
        }

        private void checkAttackTargetDestroyed(DestructionEvent evt) {
            if (evt.gameObject == data.attackTarget) {
                clearAttackTarget();
            }
        }

        private void checkDroneReplaced(GameObject drone) {
            if (drone.Equals(data.attackTarget)) {
                clearAttackTarget();
            }
        }


        private void clearAttackTarget() {
            data.attackTarget = null;
            data.attackTargetRigidBody = null;
            data.targetIsTurret = false;

            currentState = states[fallbackStateType];
            currentState.reset();
        }

        private void Update() {
            if (data == null || data.botShip == null) {
                return;
            }

            currentState.executeState();

            AIPlayerState newState = states[currentState.nextType()];

            if (newState != currentState) {
                currentState = newState;
                currentState.reset();
            }
        }

        public GameObject getCurrentShip() {
            return data.botShip;
        }
    }
}