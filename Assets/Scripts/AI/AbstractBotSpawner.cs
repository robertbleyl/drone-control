﻿using System.Collections;
using Core;
using Core.Events;
using DroneControl.Core;
using DroneControl.Ship;
using Player;
using UnityEngine.UI;
using UnityEngine;

namespace DroneControl.AI {
    public abstract class AbstractBotSpawner : MonoBehaviour, IBotSpawner {
        [SerializeField]
        protected GameObject circlePrefab;

        [SerializeField]
        protected GameObject inRangeCirclePrefab;

        [SerializeField]
        protected GameObject countTextPrefab;

        [SerializeField]
        protected Color colorHarbinger = new(1f, 0.1f, 0f, 1f);

        [SerializeField]
        protected Color colorRaven = new(0.56f, 0f, 1f, 1f);

        [SerializeField]
        protected Color colorCyclops = new(1f, 1f, 0f, 1f);

        [SerializeField]
        protected ParticleSystem particle;

        protected BotSpawner botSpawner;

        protected GameObject circle;
        protected GameObject inRangeCircle;

        protected bool showCircles;

        protected GameObject countText;
        protected Text countTextUI;
        protected GameObject currentBotShip;

        protected Camera cam;

        private void Start() {
            cam = Camera.main;
            botSpawner = GetComponent<BotSpawner>();

            Color color = colorHarbinger;
            string title = botSpawner.shipPrefab.GetComponent<ShipProperties>().title;

            if (title.Equals("Raven")) {
                color = colorRaven;
            }
            else if (title.Equals("Cyclops")) {
                color = colorCyclops;
            }

            ParticleSystem.MainModule main = particle.main;
            main.startColor = color;

            Transform buildMenuTransform = GameObject.FindGameObjectWithTag(Tags.BuildMenuOverlayCanvas).transform;

            countText = Instantiate(countTextPrefab, buildMenuTransform, true);
            countTextUI = countText.GetComponent<Text>();

            circle = Instantiate(circlePrefab, buildMenuTransform, true);

            inRangeCircle = Instantiate(inRangeCirclePrefab, buildMenuTransform, true);

            StartCoroutine(updateActiveScreen());

            GameEvents.INSTANCE.onGameObjectDestroyed += onShipDestroyed;
            GameEvents.INSTANCE.onShowScreen += onShowScreen;
            
            BotSpawners.INSTANCE.addBotSpawner(this);
        }

        private IEnumerator updateActiveScreen() {
            yield return new WaitForSecondsRealtime(0.1f);
            spawnBot();
            ScreenController screenController =
                GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<ScreenController>();
            onShowScreen(screenController.getActiveScreenType());
        }

        protected virtual void spawnBot() {
            currentBotShip = botSpawner.spawnBot();
        }

        private void onShipDestroyed(DestructionEvent e) {
            shipDestroyed(e);
        }

        protected virtual bool shipDestroyed(DestructionEvent e) {
            return e.gameObject == currentBotShip;
        }

        protected virtual void OnDestroy() {
            GameEvents.INSTANCE.onGameObjectDestroyed -= onShipDestroyed;
            GameEvents.INSTANCE.onShowScreen -= onShowScreen;

            GameEvents.INSTANCE.botSpawnerRemoved(this);

            Destroy(countText);
            Destroy(circle);
            Destroy(inRangeCircle);
        }

        private void onShowScreen(GameScreenType type) {
            setVisible(type == GameScreenType.buildMenu);
        }

        protected virtual void Update() {
            if (showCircles) {
                Vector3 screenPos = cam.WorldToScreenPoint(transform.position);

                if (screenPos.z > 0f) {
                    screenPos.z = 0f;

                    circle.transform.position = screenPos;
                    inRangeCircle.transform.position = screenPos;
                    countText.transform.position = screenPos;
                }
            }
        }

        public void setVisible(bool visible) {
            showCircles = visible;
            circle.SetActive(visible);
            inRangeCircle.SetActive(false);
            countText.SetActive(visible);
        }

        public void setShowInRangeCircle(bool showInRangeCircle) {
            if (showCircles) {
                circle.SetActive(!showInRangeCircle);
                inRangeCircle.SetActive(showInRangeCircle);
            }
        }

        public Vector3 getPosition() {
            return transform.position;
        }

        public void activate() {
            gameObject.SetActive(true);
        }

        public void destroy() {
            GameEvents.INSTANCE.destroyGameObject(new DestructionEvent(gameObject));
            Destroy(gameObject);
        }
    }
}