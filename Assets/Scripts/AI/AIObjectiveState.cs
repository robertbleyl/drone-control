using AI;
using DroneControl.Buildings;
using DroneControl.Core;
using DroneControl.Ship;
using UnityEngine;

namespace DroneControl.AI {
    public class AIObjectiveState : AIPlayerState {
        private readonly float attackCheckCooldown;
        private readonly bool playerPriority;

        private float currentAttackCheckCooldown;

        public AIObjectiveState(AIPlayerData data) : base(data) {
            attackCheckCooldown = 2f + AIPlayerData.rand.Next(2);
            playerPriority = data.botShip.GetComponent<ShipProperties>().title.Equals("Raven");
        }

        public override void reset() {
            currentAttackCheckCooldown = attackCheckCooldown;
        }

        public override AIPlayerStateType nextType() {
            currentAttackCheckCooldown -= Time.deltaTime;

            if (currentAttackCheckCooldown <= 0f) {
                currentAttackCheckCooldown += attackCheckCooldown;

                GameObject nearestTarget = getAttackTarget();

                if (nearestTarget != null) {
                    data.attackTarget = nearestTarget;
                    data.attackTargetRigidBody = nearestTarget.GetComponent<Rigidbody>();
                    data.targetIsTurret = nearestTarget.GetComponent<TurretController>() != null;
                    return AIPlayerStateType.ATTACK_TARGET;
                }
            }

            return AIPlayerStateType.OBJECTIVE;
        }

        private GameObject getAttackTarget() {
            GameObject currentPlayerShip = PlayerData.INSTANCE.currentPlayerShip;

            if (playerPriority && currentPlayerShip != null) {
                return currentPlayerShip;
            }

            GameObject nearTurret = getNearestObject(BuildingsProvider.INSTANCE.getBuildings(BuildingType.TURRET));

            if (nearTurret != null &&
                Vector3.Distance(data.botShip.transform.position, nearTurret.transform.position) < 700f) {
                return nearTurret;
            }

            if (currentPlayerShip != null &&
                Vector3.Distance(data.botShip.transform.position, currentPlayerShip.transform.position) < 600f) {
                return currentPlayerShip;
            }

            if (Vector3.Distance(data.botShip.transform.position, data.objective.transform.position) > 300f) {
                return null;
            }

            return getNearestObject(Drones.INSTANCE.getDrones());
        }

        public override void executeState() {
            if (data.objective == null) {
                setNearestObjectiveLocation();
            }

            data.aIMovementController.targetLocation = data.objective.transform.position;
        }

        private void setNearestObjectiveLocation() {
            double minDist = double.MaxValue;

            foreach (GameObject mineralField in data.asteroidMineralFields) {
                double dist = Vector3.Distance(mineralField.transform.position, data.botShip.transform.position);

                if (dist < minDist) {
                    minDist = dist;
                    data.objective = mineralField;
                }
            }
        }
    }
}