﻿using System.Collections;
using AI;
using Core;
using DroneControl.Buildings;
using DroneControl.Ship;
using UI.PauseMenu;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DroneControl.UI {
    public class LoadingScreenController : MonoBehaviour {
        public Image progressBar;

        private void Start() {
            Drones.INSTANCE.clear();
            Bots.INSTANCE.clear();
            BuildingsProvider.INSTANCE.clear();
            BotSpawners.INSTANCE.clear();

            StartCoroutine(loadAsyncOperation());
        }

        private IEnumerator loadAsyncOperation() {
            AsyncOperation levelLoader = SceneManager.LoadSceneAsync(SceneToBeLoaded.sceneName);

            while (levelLoader.progress < 1f) {
                progressBar.fillAmount = levelLoader.progress;
                yield return new WaitForEndOfFrame();
            }
        }
    }
}