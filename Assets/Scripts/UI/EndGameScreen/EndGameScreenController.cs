﻿using System;
using Core.Events;
using FMODUnity;
using UnityEngine.UI;
using UI.PauseMenu;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DroneControl.UI.EndGameScreen {
    public class EndGameScreenController : MonoBehaviour {
        [SerializeField]
        private float[] revealTimesSuccess = new float[5];

        [SerializeField]
        private float[] revealTimesFailure = new float[5];

        [SerializeField]
        private int lostDronesWeight = 50;

        [SerializeField]
        private int deathsWeight = 50;

        [SerializeField]
        private int killedTargetsWeight = 100;

        [SerializeField]
        private EventReference successMusic;

        [SerializeField]
        private EventReference failureMusic;

        [SerializeField]
        private Text messageText;

        [SerializeField]
        private GameObject nextMissionButton;

        [SerializeField]
        private GameObject finalScoreLabel;

        [SerializeField]
        private GameObject difficultyLabel;

        [SerializeField]
        private Text finalScoreValue;

        [SerializeField]
        private Text difficultyValue;

        [SerializeField]
        private Text lostDronesValue;

        [SerializeField]
        private Text deathsValue;

        [SerializeField]
        private Text killedTargetsValue;

        [SerializeField]
        private Text lostDronesSubtract;

        [SerializeField]
        private Text deathsSubtract;

        [SerializeField]
        private Text killedTargetsAdd;

        [SerializeField]
        private Text spentMineralsValue;

        [SerializeField]
        private Text deployablesValue;

        [SerializeField]
        private Text timeValue;

        [SerializeField]
        private Color successColor = Color.green;

        [SerializeField]
        private Color failureColor = Color.red;

        private string nextMissionSceneName;

        private float revealTimer;
        private bool endActive;

        private bool difficultyChanged;
        private int difficultyMultiplier;

        private int lostDronesSubtractedValue;
        private int deathsSubtractedValue;
        private int killedTargetsAddedValue;
        private int displayedScore;

        private bool finalScoreRevealed;
        private int finalScore;

        private float revealTimeKilledTargets;
        private float revealTimeDeaths;
        private float revealTimeLostDrones;
        private float revealTimeDifficulty;
        private float revealTimeFinalScore;

        private void Start() {
            difficultyMultiplier = Difficulty.Difficulty.getScoreMultiplier();
            GameEvents.INSTANCE.onDifficultyChanged += onDifficultyChanged;
        }

        private void onDifficultyChanged() {
            difficultyChanged = true;
        }

        private void Update() {
            if (!endActive) {
                return;
            }

            revealTimer += Time.deltaTime;

            if (!killedTargetsValue.enabled && revealTimer >= revealTimeKilledTargets) {
                killedTargetsValue.enabled = true;
                killedTargetsAdd.enabled = !difficultyChanged;
                displayedScore += killedTargetsAddedValue;
                finalScoreValue.text = displayedScore + "";
            }
            else if (!deathsValue.enabled && revealTimer >= revealTimeDeaths) {
                deathsValue.enabled = true;
                deathsSubtract.enabled = !difficultyChanged;
                displayedScore -= deathsSubtractedValue;
                finalScoreValue.text = displayedScore + "";
            }
            else if (!lostDronesValue.enabled && revealTimer >= revealTimeLostDrones) {
                lostDronesValue.enabled = true;
                lostDronesSubtract.enabled = !difficultyChanged;
                displayedScore -= lostDronesSubtractedValue;
                finalScoreValue.text = displayedScore + "";
            }
            else if (!difficultyValue.enabled && revealTimer >= revealTimeDifficulty && !difficultyChanged) {
                difficultyValue.enabled = true;
            }
            else if (!finalScoreRevealed && revealTimer >= revealTimeFinalScore && !difficultyChanged) {
                finalScoreValue.text = finalScore + "";
            }
        }

        public void show(bool lost, string nextMissionSceneName) {
            this.nextMissionSceneName = nextMissionSceneName;

            nextMissionButton.SetActive(nextMissionSceneName != null && nextMissionSceneName.Length > 0);

            difficultyValue.text = difficultyMultiplier + "";

            lostDronesSubtractedValue = PlayerData.INSTANCE.getLostDrones() * lostDronesWeight;
            deathsSubtractedValue = PlayerData.INSTANCE.getPlayerDeaths() * deathsWeight;
            killedTargetsAddedValue = PlayerData.INSTANCE.getKilledTargets() * killedTargetsWeight;

            finalScore = (killedTargetsAddedValue - deathsSubtractedValue - lostDronesSubtractedValue) *
                         difficultyMultiplier;

            lostDronesValue.text = PlayerData.INSTANCE.getLostDrones() + "";
            deathsValue.text = PlayerData.INSTANCE.getPlayerDeaths() + "";
            spentMineralsValue.text = PlayerData.INSTANCE.getSpentMinerals() + "";
            deployablesValue.text = PlayerData.INSTANCE.getDeployedObjects() + "";
            killedTargetsValue.text = PlayerData.INSTANCE.getKilledTargets() + "";

            int minutes = Mathf.FloorToInt(Time.timeSinceLevelLoad / 60f);
            int seconds = (int)Time.timeSinceLevelLoad % 60;

            string minutesString = minutes < 10 ? "0" + minutes : minutes + "";
            string secondsString = seconds < 10 ? "0" + seconds : seconds + "";
            timeValue.text = minutesString + ":" + secondsString;

            killedTargetsAdd.text = "+" + killedTargetsAddedValue;

            if (lostDronesSubtractedValue > 0) {
                lostDronesSubtract.text = "-" + lostDronesSubtractedValue;
                lostDronesSubtract.color = failureColor;
            }
            else {
                lostDronesSubtract.text = "0";
                lostDronesSubtract.color = successColor;
            }

            if (deathsSubtractedValue > 0) {
                deathsSubtract.text = "-" + deathsSubtractedValue;
                deathsSubtract.color = failureColor;
            }
            else {
                deathsSubtract.text = "0";
                deathsSubtract.color = successColor;
            }

            finalScoreValue.text = "0";

            difficultyValue.enabled = false;
            deathsValue.enabled = false;
            lostDronesValue.enabled = false;
            killedTargetsValue.enabled = false;

            lostDronesSubtract.enabled = false;
            deathsSubtract.enabled = false;
            killedTargetsAdd.enabled = false;

            float[] timeArray;

            if (lost) {
                messageText.text = "Mission failed!\n\nAll drones were destroyed!";
                messageText.color = failureColor;
                timeArray = revealTimesFailure;

                RuntimeManager.PlayOneShot(failureMusic, transform.position);
            }
            else {
                messageText.text = "Mission successful!";
                messageText.color = successColor;
                timeArray = revealTimesSuccess;

                RuntimeManager.PlayOneShot(successMusic, transform.position);
            }

            revealTimeKilledTargets = timeArray[0];
            revealTimeDeaths = timeArray[1];
            revealTimeLostDrones = timeArray[2];
            revealTimeDifficulty = timeArray[3];
            revealTimeFinalScore = timeArray[4];

            endActive = true;

            finalScoreLabel.SetActive(!difficultyChanged);
            finalScoreValue.gameObject.SetActive(!difficultyChanged);
            difficultyLabel.SetActive(!difficultyChanged);
        }

        public void returnToMenu() {
            // Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("Init");
        }

        public void restartMission() {
            SceneToBeLoaded.sceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene("LoadingScreen");
        }

        public void loadNextMission() {
            SceneToBeLoaded.sceneName = nextMissionSceneName;
            SceneManager.LoadScene("LoadingScreen");
        }
    }
}