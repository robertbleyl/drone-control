﻿using System;
using Core.Events;
using DroneControl.UI.PauseMenu.Options;
using DroneControl.UI.PauseMenu.Options.VideoOptions;
using UI.PauseMenu;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DroneControl.UI.PauseMenu {
    public class PauseMenuController : MonoBehaviour {
        [SerializeField]
        private GameObject continueButton;

        [SerializeField]
        private GameObject restartMissionButton;

        [SerializeField]
        private GameObject mainNavigationPanel;

        [SerializeField]
        private GameObject mainContentPanel;

        [SerializeField]
        private GameObject galaxyMapPanel;

        [SerializeField]
        private GameObject optionsPage;

        [SerializeField]
        private GameObject creditsPage;

        private GameObject activePage;

        private void Start() {
            bool initScene = SceneManager.GetActiveScene().name.Equals("Init");

            continueButton.SetActive(!initScene);
            restartMissionButton.SetActive(!initScene);
            mainNavigationPanel.SetActive(true);

            readPlayerPrefs();
        }

        private void readPlayerPrefs() {
            TextureOptionProvider.applyTextureQuality(PlayerPrefs.GetInt(TextureOptionProvider.TEXTURE_OPTION_KEY, 0));
            ModelQualityOptionProvider.applyModelQuality(PlayerPrefs.GetInt(ModelQualityOptionProvider.MODEL_OPTION_KEY,
                0));
            LODOptionProvider.applyLODOption(PlayerPrefs.GetInt(LODOptionProvider.LOD_OPTION_KEY, 0));
            ShadowQualityOptionProvider.applyShadowQuality(PlayerPrefs.GetInt(
                ShadowQualityOptionProvider.SHAODW_OPTION_KEY,
                0));

            int width = PlayerPrefs.GetInt(ResolutionOptionDataProvider.WIDTH_OPTION_KEY);
            int height = PlayerPrefs.GetInt(ResolutionOptionDataProvider.HEIGHT_OPTION_KEY);
            string fullscreenModeString = PlayerPrefs.GetString(ResolutionOptionDataProvider.FULLSCREEN_OPTION_KEY);
            int refreshRate = PlayerPrefs.GetInt(ResolutionOptionDataProvider.REFRESH_RATE_OPTION_KEY);

            if (width > 100 && height > 100 && refreshRate > 20 &&
                Enum.TryParse(fullscreenModeString, out FullScreenMode fullScreenMode)) {
                if (width != Screen.width && height != Screen.height && Screen.fullScreenMode != fullScreenMode) {
                    Screen.SetResolution(width, height, fullScreenMode, refreshRate);
                }
            }

            AudioOptionsPageController.applyVolumesToAllBusses();
        }

        public void unpause() {
            GameEvents.INSTANCE.togglePauseMenu();
        }

        public void restartMission() {
            SceneToBeLoaded.sceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene("LoadingScreen");
        }

        public void goBack() {
            mainNavigationPanel.SetActive(true);
            activePage.SetActive(false);
            mainContentPanel.SetActive(false);
        }

        private void showPage(GameObject nextPage) {
            mainNavigationPanel.SetActive(false);
            activePage = nextPage;
            activePage.SetActive(true);
            mainContentPanel.SetActive(true);
        }

        public void showGalaxyMapPanel() {
            showPage(galaxyMapPanel);
        }

        public void showOptionsPage() {
            showPage(optionsPage);
        }

        public void showCreditsPage() {
            showPage(creditsPage);
        }

        public void quitGame() {
            Application.Quit();
        }
    }
}