﻿using UnityEngine;

namespace DroneControl.UI.PauseMenu {
    public abstract class SelectionPanel<T> : MonoBehaviour {
        protected SelectionButton<T> activeButton;
        protected T selectedData;

        protected abstract void activateData(T data);

        protected abstract void deactivateData(T data);

        public void selectTab(SelectionButton<T> button, T data, bool toggleData = true) {
            unselect(toggleData);

            activeButton = button;
            selectedData = data;

            activeButton.showActiveColor();

            if (toggleData) {
                activateData(selectedData);
            }
        }

        public void unselect(bool toggleData) {
            if (activeButton != null) {
                activeButton.showInactiveColor();

                if (toggleData) {
                    deactivateData(selectedData);
                }
            }
        }
    }
}