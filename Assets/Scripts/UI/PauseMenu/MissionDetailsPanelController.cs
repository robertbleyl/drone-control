﻿using UI.PauseMenu;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DroneControl.UI.PauseMenu {
    public class MissionDetailsPanelController : MonoBehaviour {

        [SerializeField]
        private string misssionName;

        public void startMission () {
            SceneToBeLoaded.sceneName = misssionName;
            SceneManager.LoadScene ("LoadingScreen");
        }
    }
}
