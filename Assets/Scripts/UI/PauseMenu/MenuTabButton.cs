﻿using UnityEngine;

namespace DroneControl.UI.PauseMenu {
    public class MenuTabButton : SelectionButton<GameObject> {

        [SerializeField]
        private GameObject tabContent = null;

        public override GameObject getData () {
            return tabContent;
        }
    }
}
