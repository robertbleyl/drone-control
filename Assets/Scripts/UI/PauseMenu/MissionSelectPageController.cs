﻿using System.Collections;
using UnityEngine;

namespace DroneControl.UI.PauseMenu {
    public class MissionSelectPageController : MonoBehaviour {
        [SerializeField]
        private MenuTabButton tutorialsTabButton;

        [SerializeField]
        private MenuTabButton tutorial1TabButton;

        private void Start() {
            StartCoroutine(selectTutorialsTab());
        }

        private IEnumerator selectTutorialsTab() {
            yield return new WaitForSecondsRealtime(0.01f);
            tutorialsTabButton.selectTab();
            StartCoroutine(selectTutorial1Tab());
        }

        private IEnumerator selectTutorial1Tab() {
            yield return new WaitForSecondsRealtime(0.01f);
            tutorial1TabButton.selectTab();
        }
    }
}