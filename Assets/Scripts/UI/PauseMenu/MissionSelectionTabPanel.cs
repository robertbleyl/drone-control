﻿using System.Collections;
using UnityEngine;

namespace DroneControl.UI.PauseMenu {
    public class MissionSelectionTabPanel : MonoBehaviour {
        [SerializeField]
        private MenuTabButton mission01TabButton;

        private void Start() {
            StartCoroutine(selectMission01Tab());
        }

        private IEnumerator selectMission01Tab() {
            yield return new WaitForSecondsRealtime(0.01f);
            mission01TabButton.selectTab();
        }
    }
}