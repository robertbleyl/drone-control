﻿using System.Collections.Generic;
using DroneControl.Difficulty;
using UnityEngine.UI;
using UnityEngine;

namespace DroneControl.UI.PauseMenu.Options {
    public class DifficultyItemSelectionPanel : SelectionPanel<DifficultyLevel> {

        [SerializeField]
        private DifficultyItem item;

        [SerializeField]
        private float valueVeryEasy;
        [SerializeField]
        private float valueEasy;
        [SerializeField]
        private float valueNormal;
        [SerializeField]
        private float valueHard;
        [SerializeField]
        private float valueVeryHard;

        private Dictionary<DifficultyLevel, float> data = new Dictionary<DifficultyLevel, float> ();
        private Dictionary<DifficultyLevel, DifficultyItemSelectionButton> buttons = new Dictionary<DifficultyLevel, DifficultyItemSelectionButton> ();

        private void Start () {
            data[DifficultyLevel.VERY_EASY] = valueVeryEasy;
            data[DifficultyLevel.EASY] = valueEasy;
            data[DifficultyLevel.NORMAL] = valueNormal;
            data[DifficultyLevel.HARD] = valueHard;
            data[DifficultyLevel.VERY_HARD] = valueVeryHard;

            DifficultyItemSelectionButton[] buttonList = GetComponentsInChildren<DifficultyItemSelectionButton> ();

            foreach (DifficultyItemSelectionButton button in buttonList) {
                int value = Mathf.RoundToInt (data[button.level] * 100f);
                button.GetComponentInChildren<Text> ().text = value + "%";
                buttons[button.level] = button;
            }

            DifficultyLevel level = Difficulty.Difficulty.getLevel (item);
            selectTab (buttons[level], level, false);
        }

        public void setDifficultyLevel (DifficultyLevel level) {
            selectTab (buttons[level], level);
        }

        protected override void activateData (DifficultyLevel level) {
            Difficulty.Difficulty.changeValue (item, level, data[level]);
        }

        protected override void deactivateData (DifficultyLevel data) {
        }
    }
}
