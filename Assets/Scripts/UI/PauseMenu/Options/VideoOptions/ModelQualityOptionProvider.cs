﻿using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.Dropdown;

namespace DroneControl.UI.PauseMenu.Options.VideoOptions {
    public class ModelQualityOptionProvider : OptionDataProvider {

        public static string MODEL_OPTION_KEY = "ModelQuality";

        public override void applyOptionValue (int index) {
            applyModelQuality (index);
            PlayerPrefs.SetInt (MODEL_OPTION_KEY, index);
        }

        public static void applyModelQuality (int index) {
            QualitySettings.maximumLODLevel = index;
        }

        public override int getInitValue () {
            return QualitySettings.maximumLODLevel;
        }

        public override List<OptionData> getOptions () {
            List<OptionData> data = new List<OptionData> ();
            data.Add (new OptionData ("Very High"));
            data.Add (new OptionData ("High"));
            data.Add (new OptionData ("Medium"));
            data.Add (new OptionData ("Low"));
            return data;
        }
    }
}
