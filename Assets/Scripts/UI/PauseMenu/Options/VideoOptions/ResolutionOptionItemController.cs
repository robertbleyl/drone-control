﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

namespace DroneControl.UI.PauseMenu.Options.VideoOptions {
    public class ResolutionOptionItemController : OptionItemController {
        [SerializeField]
        private Dropdown resolutionDropDown;

        [SerializeField]
        private Dropdown fullscreenDropDown;

        [SerializeField]
        private GameObject resolutionConfirmationModal;

        [SerializeField]
        private Text countDownLabel;

        [SerializeField]
        private int countDownTime = 15;

        private ResolutionOptionDataProvider resolutionOptionDataProvider;
        private float countDownTimer;

        protected override void Start() {
            resolutionOptionDataProvider = GetComponent<ResolutionOptionDataProvider>();

            StartCoroutine(delayStart());
        }

        private IEnumerator delayStart() {
            yield return new WaitForSecondsRealtime(0.1f);

            resolutionDropDown.AddOptions(resolutionOptionDataProvider.getResolutionOptions());
            resolutionDropDown.value = resolutionOptionDataProvider.getResolutionInitValue();

            fullscreenDropDown.AddOptions(resolutionOptionDataProvider.getFullscreenModeOptions());
            fullscreenDropDown.value = resolutionOptionDataProvider.getFullscreenModeInitValue();
        }

        public override void apply() {
            if (resolutionOptionDataProvider.applyOptionValues(resolutionDropDown.value, fullscreenDropDown.value)) {
                resolutionConfirmationModal.SetActive(true);
                countDownTimer = countDownTime;
            }
        }

        private void Update() {
            if (countDownTimer > 0f) {
                countDownTimer -= Time.deltaTime;

                countDownLabel.text = Mathf.RoundToInt(countDownTimer) + "";

                if (countDownTimer <= 0f) {
                    restorePreviousResolution();
                }
            }
        }

        public void keepResolution() {
            resolutionOptionDataProvider.keepResolution(resolutionDropDown.value, fullscreenDropDown.value);
            resolutionConfirmationModal.SetActive(false);
        }

        public void restorePreviousResolution() {
            resolutionOptionDataProvider.restorePreviousResolution();
            resolutionConfirmationModal.SetActive(false);

            StartCoroutine(restoreDropDowns());
        }

        private IEnumerator restoreDropDowns() {
            yield return new WaitForSecondsRealtime(0.1f);
            resolutionDropDown.value = resolutionOptionDataProvider.getResolutionInitValue();
            fullscreenDropDown.value = resolutionOptionDataProvider.getFullscreenModeInitValue();
        }
    }
}