﻿using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.Dropdown;

namespace DroneControl.UI.PauseMenu.Options.VideoOptions {
    public class LODOptionProvider : OptionDataProvider {

        public static string LOD_OPTION_KEY = "LevelOfDetail";

        private static float[] lodData = new float[]
        {
            2f,
            1.5f,
            1f,
            0.6f,
            0.3f
        };

        public override void applyOptionValue (int index) {
            applyLODOption (index);
            PlayerPrefs.SetInt (LOD_OPTION_KEY, index);
        }

        public static void applyLODOption (int index) {
            QualitySettings.lodBias = lodData[index];
        }

        public override int getInitValue () {
            for (int i = 0; i < lodData.Length; i++) {
                if (lodData[i] == QualitySettings.lodBias) {
                    return i;
                }
            }

            return 0;
        }

        public override List<OptionData> getOptions () {
            List<OptionData> data = new List<OptionData> ();
            data.Add (new OptionData ("Very High"));
            data.Add (new OptionData ("High"));
            data.Add (new OptionData ("Medium"));
            data.Add (new OptionData ("Low"));
            data.Add (new OptionData ("Very Low"));
            return data;
        }
    }
}
