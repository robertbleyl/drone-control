﻿using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.Dropdown;
using ShadowResolution = UnityEngine.Rendering.Universal.ShadowResolution;

namespace DroneControl.UI.PauseMenu.Options.VideoOptions {
    public class ShadowQualityOptionProvider : OptionDataProvider {

        public struct ShadowQualitySetting {

            public ShadowQualitySetting (ShadowResolution resolution, bool softShadow, bool shadowsEnabled) {
                this.resolution = resolution;
                this.softShadow = softShadow;
                this.shadowsEnabled = shadowsEnabled;
            }

            public ShadowResolution resolution { get; set; }
            public bool softShadow { get; set; }
            public bool shadowsEnabled { get; set; }
        }

        public static string SHAODW_OPTION_KEY = "ShadowQuality";

        public static ShadowQualitySetting[] qualities = new ShadowQualitySetting[] {
            new ShadowQualitySetting (ShadowResolution._4096, true, true),
            new ShadowQualitySetting (ShadowResolution._2048, true, true),
            new ShadowQualitySetting (ShadowResolution._1024, false, true),
            new ShadowQualitySetting (ShadowResolution._512, false, true),
            new ShadowQualitySetting (ShadowResolution._256, false, true),
            new ShadowQualitySetting (ShadowResolution._256, false, false)
        };

        public static void applyShadowQuality (int index) {
            ShadowQualitySetting setting = qualities[index];

            ShadowQualitySettings.MainLightCastShadows = setting.shadowsEnabled;
            ShadowQualitySettings.AdditionalLightCastShadows = setting.shadowsEnabled;

            ShadowQualitySettings.MainLightShadowResolution = setting.resolution;
            ShadowQualitySettings.AdditionalLightShadowResolution = setting.resolution;

            ShadowQualitySettings.SoftShadowsEnabled = setting.softShadow;
        }

        public override void applyOptionValue (int index) {
            applyShadowQuality (index);
            PlayerPrefs.SetInt (SHAODW_OPTION_KEY, index);
        }

        public override int getInitValue () {
            if (!ShadowQualitySettings.MainLightCastShadows) {
                return 5;
            }

            for (int i = 0; i < qualities.Length; i++) {
                if (qualities[i].resolution == ShadowQualitySettings.MainLightShadowResolution) {
                    return i;
                }
            }

            return 0;
        }

        public override List<OptionData> getOptions () {
            List<OptionData> data = new List<OptionData> ();
            data.Add (new OptionData ("Very High"));
            data.Add (new OptionData ("High"));
            data.Add (new OptionData ("Medium"));
            data.Add (new OptionData ("Low"));
            data.Add (new OptionData ("Very Low"));
            data.Add (new OptionData ("Off"));
            return data;
        }
    }
}
