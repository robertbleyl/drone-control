﻿using UnityEngine;

namespace DroneControl.UI.PauseMenu.Options.VideoOptions {
    public class VideoOptionsPanelController : MonoBehaviour {

        private OptionItemController[] optionItemControllers;

        private void Start () {
            optionItemControllers = GetComponentsInChildren<OptionItemController> ();
        }

        public void save () {
            foreach (OptionItemController controller in optionItemControllers) {
                controller.apply ();
            }
        }
    }
}
