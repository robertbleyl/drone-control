﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.UI.Dropdown;

namespace DroneControl.UI.PauseMenu.Options.VideoOptions {
    public class ResolutionOptionDataProvider : MonoBehaviour {

        public static string WIDTH_OPTION_KEY = "Width";
        public static string HEIGHT_OPTION_KEY = "Height";
        public static string FULLSCREEN_OPTION_KEY = "FullScreen";
        public static string REFRESH_RATE_OPTION_KEY = "RefreshRate";

        private List<Resolution> resolutions;
        private List<FullScreenMode> fullScreenModes;

        private int previousWidth;
        private int previousHeight;
        private FullScreenMode previousFullScreenMode;
        private RefreshRate previousRefreshRate;

        private void Start () {
            Dictionary<string, Resolution> resolutionsMap = new Dictionary<string, Resolution> ();

            foreach (Resolution res in Screen.resolutions.Reverse ()) {
                string resString = res.width + "x" + res.height;
                resolutionsMap.TryGetValue (resString, out var highestRefreshRateResolution);

                if (res.refreshRateRatio.CompareTo(highestRefreshRateResolution.refreshRateRatio) < 0) {
                    resolutionsMap[resString] = res;
                }
            }
            
            resolutions = resolutionsMap.Values.ToList ();
            resolutions.Sort (compareResolutions);
            resolutions.Reverse ();

            fullScreenModes = Enum.GetValues (typeof (FullScreenMode)).Cast<FullScreenMode> ().ToList ();
        }

        private int compareResolutions (Resolution a, Resolution b) {
            int result = a.width.CompareTo (b.height);
            return result == 0 ? a.height.CompareTo (b.height) : result;
        }

        public bool applyOptionValues (int resolutionIndex, int fullscreenModeIndex) {
            Resolution resolution = resolutions[resolutionIndex];
            FullScreenMode fullscreenMode = fullScreenModes[fullscreenModeIndex];

            if (Screen.width == resolution.width && Screen.height == resolution.height && Screen.fullScreenMode.Equals(fullscreenMode)) {
                return false;
            }
            
            previousWidth = Screen.width;
            previousHeight = Screen.height;
            previousFullScreenMode = Screen.fullScreenMode;
            previousRefreshRate = Screen.currentResolution.refreshRateRatio;

            Screen.SetResolution (resolution.width, resolution.height, fullscreenMode, resolution.refreshRateRatio);
            return true;

        }

        public void keepResolution (int resolutionIndex, int fullscreenModeIndex) {
            Resolution resolution = resolutions[resolutionIndex];
            FullScreenMode fullscreenMode = fullScreenModes[fullscreenModeIndex];

            PlayerPrefs.SetInt (WIDTH_OPTION_KEY, resolution.width);
            PlayerPrefs.SetInt (HEIGHT_OPTION_KEY, resolution.height);
            PlayerPrefs.SetString (FULLSCREEN_OPTION_KEY, fullscreenMode.ToString ());
            PlayerPrefs.SetFloat(REFRESH_RATE_OPTION_KEY, (float) resolution.refreshRateRatio.value);
        }

        public void restorePreviousResolution () {
            Screen.SetResolution (previousWidth, previousHeight, previousFullScreenMode, previousRefreshRate);
        }

        public List<OptionData> getResolutionOptions () {
            return resolutions.Select (r => new OptionData (r.width + " x " + r.height)).ToList ();
        }

        public List<OptionData> getFullscreenModeOptions () {
            return fullScreenModes.Select (r => new OptionData (r.ToString ())).ToList ();
        }

        public int getResolutionInitValue () {
            for (int i = 0; i < resolutions.Count; i++) {
                Resolution res = resolutions[i];
                if (Screen.width == res.width && Screen.height == res.height) {
                    return i;
                }
            }

            return 0;
        }

        public int getFullscreenModeInitValue () {
            for (int i = 0; i < fullScreenModes.Count; i++) {
                if (Screen.fullScreenMode.Equals (fullScreenModes[i])) {
                    return i;
                }
            }

            return 0;
        }
    }
}
