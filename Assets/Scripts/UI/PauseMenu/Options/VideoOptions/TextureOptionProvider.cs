﻿using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.Dropdown;

namespace DroneControl.UI.PauseMenu.Options.VideoOptions {
    public class TextureOptionProvider : OptionDataProvider {

        public static string TEXTURE_OPTION_KEY = "TextureQuality";

        public override void applyOptionValue (int index) {
            applyTextureQuality (index);
            PlayerPrefs.SetInt (TEXTURE_OPTION_KEY, index);
        }

        public static void applyTextureQuality (int index) {
            QualitySettings.globalTextureMipmapLimit = index;
        }

        public override int getInitValue () {
            return QualitySettings.globalTextureMipmapLimit;
        }

        public override List<OptionData> getOptions () {
            List<OptionData> data = new List<OptionData> ();
            data.Add (new OptionData ("Very High"));
            data.Add (new OptionData ("High"));
            data.Add (new OptionData ("Medium"));
            data.Add (new OptionData ("Low"));
            data.Add (new OptionData ("Very Low"));
            return data;
        }
    }
}
