﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

namespace DroneControl.UI.PauseMenu.Options.VideoOptions {
    public class OptionItemController : MonoBehaviour {

        [SerializeField]
        private Dropdown dropDown;

        private OptionDataProvider optionDataProvider;

        protected virtual void Start () {
            optionDataProvider = GetComponent<OptionDataProvider> ();

            StartCoroutine (delayedStart ());
        }

        private IEnumerator delayedStart () {
            yield return new WaitForSecondsRealtime (0.1f);

            if (optionDataProvider != null) {
                dropDown.AddOptions (optionDataProvider.getOptions ());
                dropDown.value = optionDataProvider.getInitValue ();
            }
        }

        public virtual void apply () {
            if (optionDataProvider != null) {
                optionDataProvider.applyOptionValue(dropDown.value);
            }
        }
    }
}
