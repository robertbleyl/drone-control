﻿using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.Dropdown;

namespace DroneControl.UI.PauseMenu.Options.VideoOptions {
    public abstract class OptionDataProvider : MonoBehaviour {

        public abstract List<OptionData> getOptions ();

        public abstract int getInitValue ();

        public abstract void applyOptionValue (int index);
    }
}
