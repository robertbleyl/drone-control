﻿using DroneControl.Difficulty;
using UnityEngine;

namespace DroneControl.UI.PauseMenu.Options {
    public class DifficultyItemSelectionButton : SelectionButton<DifficultyLevel> {

        [SerializeField]
        public DifficultyLevel level;

        public override DifficultyLevel getData () {
            return level;
        }
    }
}
