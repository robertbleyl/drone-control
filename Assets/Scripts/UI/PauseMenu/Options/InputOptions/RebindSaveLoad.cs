using System;
using Core.Events;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DroneControl.UI.PauseMenu.Options.InputOptions {
    public class RebindSaveLoad : MonoBehaviour {
        [SerializeField]
        public InputActionAsset actions;

        private void Start() {
            string rebinds = PlayerPrefs.GetString("rebinds");

            if (!string.IsNullOrEmpty(rebinds)) {
                try {
                    actions.LoadBindingOverridesFromJson(rebinds);
                }
                catch (NotImplementedException) {
                    // ignore
                }
            }

            GameEvents.INSTANCE.onKeyBindingsChanged += saveBindings;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onKeyBindingsChanged -= saveBindings;
        }

        private void saveBindings() {
            string rebinds = actions.SaveBindingOverridesAsJson();
            PlayerPrefs.SetString("rebinds", rebinds);
        }
    }
}