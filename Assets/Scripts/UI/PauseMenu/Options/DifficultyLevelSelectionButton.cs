﻿using DroneControl.Difficulty;
using UnityEngine;

namespace DroneControl.UI.PauseMenu.Options {
    public class DifficultyLevelSelectionButton : SelectionButton<DifficultyLevel> {

        [SerializeField]
        private DifficultyLevel level;

        public override DifficultyLevel getData () {
            return level;
        }
    }
}
