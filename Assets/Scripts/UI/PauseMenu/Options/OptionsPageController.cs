﻿using System.Collections;
using UnityEngine;

namespace DroneControl.UI.PauseMenu.Options {
    public class OptionsPageController : MonoBehaviour {

        [SerializeField]
        private MenuTabButton difficultyTabButton = null;

        private void Start () {
            StartCoroutine (selectDifficultyTabButton ());
        }

        private IEnumerator selectDifficultyTabButton () {
            yield return new WaitForSecondsRealtime (0.01f);
            difficultyTabButton.selectTab ();
        }
    }
}
