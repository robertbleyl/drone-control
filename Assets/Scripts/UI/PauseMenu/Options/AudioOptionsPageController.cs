﻿using FMODUnity;
using UnityEngine;
using UnityEngine.UI;

namespace DroneControl.UI.PauseMenu.Options {
    public class AudioOptionsPageController : MonoBehaviour {
        private static string MASTER_VOLUME_OPTION_KEY = "MasterVolume";
        private static string EFFECTS_VOLUME_OPTION_KEY = "EffectsVolume";
        private static string MUSIC_VOLUME_OPTION_KEY = "MusicVolume";

        private static string masterBusPath = "bus:/";
        private static string effectsBusPath = "bus:/EffectsGroup";
        private static string musicBusPath = "bus:/MusicGroup";

        [SerializeField]
        private Slider masterVolumeSlider;

        [SerializeField]
        private Slider effectsVolumeSlider;

        [SerializeField]
        private Slider musicVolumeSlider;

        private void Start() {
            masterVolumeSlider.value = PlayerPrefs.GetFloat(MASTER_VOLUME_OPTION_KEY, 1f);
            effectsVolumeSlider.value = PlayerPrefs.GetFloat(EFFECTS_VOLUME_OPTION_KEY, 1f);
            musicVolumeSlider.value = PlayerPrefs.GetFloat(MUSIC_VOLUME_OPTION_KEY, 1f);
        }

        public static void applyVolumesToAllBusses() {
            applyVolumeToBus(masterBusPath, MASTER_VOLUME_OPTION_KEY);
            applyVolumeToBus(effectsBusPath, EFFECTS_VOLUME_OPTION_KEY);
            applyVolumeToBus(musicBusPath, MUSIC_VOLUME_OPTION_KEY);
        }

        private static void applyVolumeToBus(string busPath, string key) {
            float volume = PlayerPrefs.GetFloat(key, 1f);
            RuntimeManager.GetBus(busPath).setVolume(volume);
        }

        public void masterVolumeChanged() {
            PlayerPrefs.SetFloat(MASTER_VOLUME_OPTION_KEY, masterVolumeSlider.value);
            applyVolumeToBus(masterBusPath, MASTER_VOLUME_OPTION_KEY);
        }

        public void effectsVolumeChanged() {
            PlayerPrefs.SetFloat(EFFECTS_VOLUME_OPTION_KEY, effectsVolumeSlider.value);
            applyVolumeToBus(effectsBusPath, EFFECTS_VOLUME_OPTION_KEY);
        }

        public void musicVolumeChanged() {
            PlayerPrefs.SetFloat(MUSIC_VOLUME_OPTION_KEY, musicVolumeSlider.value);
            applyVolumeToBus(musicBusPath, MUSIC_VOLUME_OPTION_KEY);
        }
    }
}