using Core.Events;
using DroneControl.UI.PauseMenu.Options.InputOptions;
using UnityEngine;

namespace DroneControl.UI.PauseMenu.Options {
    public class ControlsOptionPageController : MonoBehaviour {
        private RebindActionUI[] rebindActionUis;

        private void Start() {
            rebindActionUis = GetComponentsInChildren<RebindActionUI>();
        }

        public void restoreDefaultBindings() {
            foreach (RebindActionUI rebindActionUi in rebindActionUis) {
                rebindActionUi.ResetToDefault();
            }

            GameEvents.INSTANCE.keyBindingsChanged();
        }
    }
}