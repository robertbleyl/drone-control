﻿using Core.Events;
using DroneControl.Difficulty;

namespace DroneControl.UI.PauseMenu.Options {
    public class DifficultyLevelSelectionPanel : SelectionPanel<DifficultyLevel> {
        private DifficultyItemSelectionPanel[] panels;

        private void Start() {
            panels = GetComponentsInChildren<DifficultyItemSelectionPanel>();

            GameEvents.INSTANCE.onDifficultyChanged += onDifficultyChanged;

            onDifficultyChanged();
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onDifficultyChanged -= onDifficultyChanged;
        }

        private void onDifficultyChanged() {
            DifficultyLevel? level = Difficulty.Difficulty.sameLevelForAllItems();

            if (level != null) {
                foreach (DifficultyLevelSelectionButton button in
                         GetComponentsInChildren<DifficultyLevelSelectionButton>()) {
                    if (button.getData() == level) {
                        selectTab(button, button.getData(), false);
                        break;
                    }
                }
            }
            else {
                unselect(false);
            }
        }

        protected override void activateData(DifficultyLevel level) {
            foreach (DifficultyItemSelectionPanel panel in panels) {
                panel.setDifficultyLevel(level);
            }
        }

        protected override void deactivateData(DifficultyLevel data) {
        }
    }
}