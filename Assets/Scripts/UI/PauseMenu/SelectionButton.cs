﻿using UnityEngine;
using UnityEngine.UI;

namespace DroneControl.UI.PauseMenu {
    public abstract class SelectionButton<T> : MonoBehaviour {
        [SerializeField]
        private Color activeColor;

        [SerializeField]
        private SelectionPanel<T> tabPanel;

        private Image image;
        private Color inactiveColor;

        private void Awake() {
            Button button = GetComponent<Button>();
            button.onClick.AddListener(selectTab);

            image = GetComponent<Image>();
            inactiveColor = image.color;
        }

        public abstract T getData();

        public void selectTab() {
            tabPanel.selectTab(this, getData());
        }

        public void showActiveColor() {
            image.color = activeColor;
        }

        public void showInactiveColor() {
            image.color = inactiveColor;
        }
    }
}