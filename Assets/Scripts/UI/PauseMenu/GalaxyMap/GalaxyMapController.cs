using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.PauseMenu.GalaxyMap {
    public class GalaxyMapController : MonoBehaviour {
        [SerializeField]
        public List<Mission> missions;

        [SerializeField]
        public GameObject buttonPrefab;

        public void Start() {
            foreach (Mission mission in missions) {
                GameObject button = Instantiate(buttonPrefab, gameObject.transform, true);
                button.transform.localPosition = mission.position;
                button.GetComponent<GalaxyMapMissionSelectButton>().init(mission, this);
            }
        }

        public void selectMission(Mission mission) {
            SceneToBeLoaded.sceneName = mission.id;
            SceneManager.LoadScene("LoadingScreen");
        }
    }
}