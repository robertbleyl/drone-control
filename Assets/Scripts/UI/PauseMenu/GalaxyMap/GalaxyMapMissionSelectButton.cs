using UnityEngine;
using UnityEngine.UI;

namespace UI.PauseMenu.GalaxyMap {
    public class GalaxyMapMissionSelectButton : MonoBehaviour {
        [SerializeField]
        public Text buttonText;

        private Mission mission;
        private GalaxyMapController galaxyMapController;

        public void init(Mission mission, GalaxyMapController galaxyMapController) {
            this.mission = mission;
            this.galaxyMapController = galaxyMapController;

            buttonText.text = mission.displayName;
        }

        public void onClick() {
            galaxyMapController.selectMission(mission);
        }
    }
}