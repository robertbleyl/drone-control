﻿using UnityEngine;

namespace DroneControl.UI.PauseMenu {
    public class TabPanel : SelectionPanel<GameObject> {

        protected override void activateData (GameObject data) {
            data.SetActive (true);
        }

        protected override void deactivateData (GameObject data) {
            data.SetActive (false);
        }
    }
}
