﻿using UnityEngine.UI;
using UnityEngine;

namespace DroneControl.UI {
    public class FPSLabelController : MonoBehaviour {

        private Text fpsTextLabel;
        private float fpsLabelUpdateTimer;

        private void Start () {
            fpsTextLabel = GetComponentInChildren<Text> ();
        }

        private void Update () {
            fpsLabelUpdateTimer += Time.unscaledDeltaTime;

            if (fpsLabelUpdateTimer > 0.25f) {
                fpsLabelUpdateTimer = 0f;
                fpsTextLabel.text = (int) (1f / Time.unscaledDeltaTime) + " FPS";
            }
        }
    }
}