﻿using System.Collections.Generic;
using Core;
using Core.Events;
using DroneControl.Core;
using DroneControl.Player;
using DroneControl.Ship;
using Ship.Properties;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.UI;

namespace UI.HUD {
    public class HUDController : MonoBehaviour {
        [SerializeField]
        private GameObject nextWavePanel;

        [SerializeField]
        private Text nextWaveSecondsLabel;

        [SerializeField]
        private Text notificationTextLabel;

        [SerializeField]
        private Text objectiveTextLabel;

        [SerializeField]
        private GameObject objectiveTextPanel;

        [SerializeField]
        private RectTransform speedBarValue;

        [SerializeField]
        private RectTransform dashEnergyBarValue;

        [SerializeField]
        private RectTransform healthBarValue;

        [SerializeField]
        private RectTransform shieldBarValue;

        [SerializeField]
        public GameObject coupledIndicator;

        [SerializeField]
        private GameObject decoupledIndicator;

        [SerializeField]
        public GameObject upgradeButtonPanel;

        [SerializeField]
        private GameObject targetInfoPanel;

        [SerializeField]
        private GameObject harbingerTargetPanel;

        [SerializeField]
        private GameObject ravenTargetPanel;

        [SerializeField]
        private GameObject cyclopsTargetPanel;

        [SerializeField]
        private RectTransform targetShieldBar;

        [SerializeField]
        private RectTransform targetHealthBar;

        [SerializeField]
        public GameObject buildTurretButton;

        [SerializeField]
        public GameObject buildInhibitorButton;

        [SerializeField]
        public GameObject buildSingularityButton;

        [SerializeField]
        public GameObject buildSlowZoneButton;

        [SerializeField]
        public GameObject gatlingGunPanel;

        [SerializeField]
        public GameObject gatlingGunAmmoDisplayContainer;

        [SerializeField]
        public GameObject pulseCannonPanel;

        [SerializeField]
        public GameObject railGunPanel;

        [SerializeField]
        public GameObject missileLauncherPanel;

        [SerializeField]
        private Image missileLauncherCooldown;

        [SerializeField]
        private Image missileLockOn;

        private float notificationTimer;

        private ShipInputController inputController;

        private HealthController healthController;
        private DashEnergyController dashEnergyController;
        private ShipMovementController shipMovementController;
        private PlayerShipWeaponController shipWeaponController;

        private HealthController targetHealthController;

        private float lastWeaponAmmoPercentage;

        private bool screenActive;

        private readonly Dictionary<WeaponType, GameObject> weaponPanels = new();
        private GameObject activeWeaponPanel;

        private float nextWaveTimer;

        private string originalObjectiveText;

        private Camera cam;

        private GameObject currentShip;
        private GameObject currentTarget;

        private void Start() {
            cam = Camera.main;
            GameEvents.INSTANCE.onPlayerShipChange += changeShip;
            GameEvents.INSTANCE.onTargetChange += changeTarget;
            GameEvents.INSTANCE.onShowScreen += onShowScreen;
            GameEvents.INSTANCE.onWeaponTypeChanged += onWeaponTypeChanged;
            GameEvents.INSTANCE.onKeyBindingsChanged += onKeyBindingsChanged;

            notificationTextLabel.text = "";
            objectiveTextLabel.text = "";

            GameObject playerObject = GameObject.FindGameObjectWithTag(Tags.Player);
            inputController = playerObject.GetComponent<ShipInputController>();

            nextWavePanel.SetActive(false);
            objectiveTextPanel.SetActive(false);
            targetInfoPanel.SetActive(false);

            weaponPanels[WeaponType.GATLING_GUN] = gatlingGunPanel;
            weaponPanels[WeaponType.PULSE_CANNON] = pulseCannonPanel;
            weaponPanels[WeaponType.RAIL_GUN] = railGunPanel;
            weaponPanels[WeaponType.MISSILES] = missileLauncherPanel;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onPlayerShipChange -= changeShip;
            GameEvents.INSTANCE.onTargetChange -= changeTarget;
            GameEvents.INSTANCE.onShowScreen -= onShowScreen;
            GameEvents.INSTANCE.onWeaponTypeChanged -= onWeaponTypeChanged;
            GameEvents.INSTANCE.onKeyBindingsChanged -= onKeyBindingsChanged;
        }

        private void changeShip(GameObject ship) {
            currentShip = ship;

            if (currentShip != null) {
                healthController = currentShip.GetComponent<HealthController>();
                dashEnergyController = currentShip.GetComponent<DashEnergyController>();
                shipMovementController = currentShip.GetComponent<ShipMovementController>();
                shipWeaponController = currentShip.GetComponent<PlayerShipWeaponController>();
            }
        }

        private void changeTarget(GameObject target) {
            currentTarget = target;

            targetInfoPanel.SetActive(target != null);

            if (target != null) {
                targetHealthController = target.GetComponent<HealthController>();
                ShipProperties shipProperties = target.GetComponent<ShipProperties>();

                harbingerTargetPanel.SetActive(shipProperties.title.Equals("Harbinger"));
                ravenTargetPanel.SetActive(shipProperties.title.Equals("Raven"));
                cyclopsTargetPanel.SetActive(shipProperties.title.Equals("Cyclops"));
            }
        }

        private void onShowScreen(GameScreenType type) {
            screenActive = type == GameScreenType.hud;
        }

        private void onWeaponTypeChanged(WeaponSwitchedEvent evt) {
            if (activeWeaponPanel != null) {
                activeWeaponPanel.transform.localScale = Vector3.one;
            }

            activeWeaponPanel = weaponPanels[evt.weaponType];
            activeWeaponPanel.transform.localScale = new Vector3(1.3f, 1.3f, 1f);
        }

        private void onKeyBindingsChanged() {
            if (originalObjectiveText != null) {
                // TODO
                // objectiveTextLabel.text = InputUtils.updateKeyBindingsInText (originalObjectiveText);
            }
        }

        private void Update() {
            if (!screenActive) {
                return;
            }

            if (notificationTimer > 0f) {
                notificationTimer -= Time.deltaTime;


                if (notificationTimer <= 0f) {
                    notificationTextLabel.text = "";
                }
            }

            if (nextWaveTimer > 0f) {
                nextWaveTimer -= Time.deltaTime;
            }

            if (nextWaveTimer > 0f) {
                nextWaveSecondsLabel.text = "" + (int)nextWaveTimer;
            }
            else {
                nextWavePanel.SetActive(false);
            }

            if (currentShip != null) {
                updatePlayerShipStatus();
                updateTargetShipStatus();
                updateMissileCooldownBar();
                updateMissileLockOnIndicator();

                if (!inputController.disableDecoupledMode) {
                    coupledIndicator.SetActive(!shipMovementController.decoupled);
                    decoupledIndicator.SetActive(shipMovementController.decoupled);
                }
            }
        }

        private void updatePlayerShipStatus() {
            if (currentShip == null || healthController == null) {
                return;
            }

            updateHorizontalStatusBar(healthBarValue, healthController.getCurrentHitPointsPercentage());
            updateHorizontalStatusBar(shieldBarValue, healthController.getCurrentShieldPercentage());
            updateVerticalStatusBar(speedBarValue, shipMovementController.forwardThrustPercentage);
            updateVerticalStatusBar(dashEnergyBarValue, dashEnergyController.getCurrentEnergyPercentage());
        }

        private void updateTargetShipStatus() {
            if (currentShip == null || currentTarget == null ||
                targetHealthController == null) {
                return;
            }

            updateHorizontalStatusBar(targetHealthBar, targetHealthController.getCurrentHitPointsPercentage());
            updateHorizontalStatusBar(targetShieldBar, targetHealthController.getCurrentShieldPercentage());
        }

        private void updateMissileCooldownBar() {
            if (shipWeaponController != null && shipWeaponController.getWeaponController(WeaponType.MISSILES) != null &&
                missileLauncherCooldown != null) {
                missileLauncherCooldown.fillAmount = 1f - shipWeaponController.getWeaponController(WeaponType.MISSILES)
                    .getCooldownPercentage();
            }
        }

        private void updateMissileLockOnIndicator() {
            float percentage = inputController.getLockOnPercentage();
            missileLockOn.fillAmount = percentage;

            if (currentTarget == null) {
                return;
            }

            Vector3 screenPos = cam.WorldToScreenPoint(currentTarget.transform.position);

            if (screenPos.z <= 0f) {
                return;
            }

            screenPos.z = 0f;
            missileLockOn.transform.position = screenPos;
        }

        private void updateHorizontalStatusBar(RectTransform elem, float percentage) {
            elem.localScale = new Vector3(percentage, elem.localScale.y, 1f);
        }

        private void updateVerticalStatusBar(RectTransform elem, float percentage) {
            elem.localScale = new Vector3(elem.localScale.x, percentage, 1f);
        }

        public void showNotification(string message, float time) {
            notificationTextLabel.text = message;
            notificationTimer = time;
        }

        public void setObjectiveText(string message) {
            originalObjectiveText = message;
            // TODO
            // objectiveTextLabel.text = InputUtils.updateKeyBindingsInText (message);
            objectiveTextPanel.SetActive(message?.Length > 0);
        }

        public void setNextWaveSeconds(int seconds) {
            nextWavePanel.SetActive(true);
            nextWaveSecondsLabel.text = "" + seconds;
            nextWaveTimer = seconds;
        }
    }
}