﻿using Core.Events;
using DroneControl.Core;
using UnityEngine;

namespace UI.HUD {
    public class TargetBorderMarkerController : MonoBehaviour {
        private static int size = 32;

        private GameObject target;
        private Camera cam;

        private void Start() {
            cam = Camera.main;
            transform.localScale = Vector3.up * 20000f;

            GameEvents.INSTANCE.onTargetChange += setTarget;
            GameEvents.INSTANCE.onShowScreen += checkScreen;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onTargetChange -= setTarget;
            GameEvents.INSTANCE.onShowScreen -= checkScreen;
        }

        private void checkScreen(GameScreenType type) {
            enabled = type == GameScreenType.hud;
        }

        private void setTarget(GameObject t) {
            target = t;
        }

        private void OnGUI() {
            if (target == null) {
                transform.localScale = Vector3.up * 20000f;
                return;
            }

            Vector3 rawScreenPosition = cam.WorldToScreenPoint(target.transform.position);
            Vector2 screenPos = new Vector2(rawScreenPosition.x, rawScreenPosition.y);

            int camWidth = Screen.width;
            int camHeight = Screen.height;

            if (screenPos.x > 0f && screenPos.x < camWidth && screenPos.y > 0f && screenPos.y < camHeight &&
                rawScreenPosition.z > 0f) {
                transform.localScale = Vector3.zero;
            }
            else {
                transform.localScale = Vector3.one;
            }

            float halfCameraWidth = camWidth / 2f;

            Vector2 screenCenter = new Vector2(halfCameraWidth, camHeight / 2f);
            Vector2 dir = (screenPos - screenCenter).normalized;

            if (rawScreenPosition.z < 0f) {
                dir = -dir;
            }

            float angle = Vector2.SignedAngle(dir, Vector2.down);
            transform.rotation = new Quaternion(0, 0, 1f, angle * Mathf.Deg2Rad);
            dir *= (halfCameraWidth - size);

            Vector2 pos = screenCenter + dir;

            if (pos.x < size) {
                pos.x = size;
            }

            if (pos.x > camWidth - size) {
                pos.x = camWidth - size;
            }

            if (pos.y < size) {
                pos.y = size;
            }

            if (pos.y > camHeight - size) {
                pos.y = camHeight - size;
            }

            transform.position = pos;
        }
    }
}