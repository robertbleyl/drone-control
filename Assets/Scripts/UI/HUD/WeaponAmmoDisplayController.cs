﻿using Core.Events;
using DroneControl.Core;
using DroneControl.Player;
using DroneControl.Ship.Weapon;
using UnityEngine;
using UnityEngine.UI;

namespace UI.HUD {
    public class WeaponAmmoDisplayController : MonoBehaviour {
        [SerializeField]
        private WeaponType weaponType;

        [SerializeField]
        private Image image;

        private WeaponController weaponController;

        private void Start() {
            GameEvents.INSTANCE.onPlayerShipChange += onPlayerShipChange;

            if (PlayerData.INSTANCE.currentPlayerShip != null) {
                onPlayerShipChange(PlayerData.INSTANCE.currentPlayerShip);
            }
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onPlayerShipChange -= onPlayerShipChange;
        }

        private void onPlayerShipChange(GameObject ship) {
            if (ship != null) {
                weaponController = ship.GetComponent<PlayerShipWeaponController>().getWeaponController(weaponType);
            }
            else {
                weaponController = null;
            }
        }

        private void Update() {
            if (weaponController != null) {
                image.fillAmount = weaponController.getCurrentAmmoPercentage();
            }
        }
    }
}