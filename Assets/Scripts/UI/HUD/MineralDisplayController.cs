﻿using System.Collections;
using Core.Events;
using UnityEngine.UI;
using UnityEngine;

namespace UI.HUD {
    public class MineralDisplayController : MonoBehaviour {
        [SerializeField]
        private Text text;

        private void Start() {
            GameEvents.INSTANCE.onPlayerMineralsChanged += changeMineralText;

            StartCoroutine(initMinerals());
        }

        private IEnumerator initMinerals() {
            yield return new WaitForSecondsRealtime(0.2f);
            changeMineralText(PlayerData.INSTANCE.getMinerals());
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onPlayerMineralsChanged -= changeMineralText;
        }

        private void changeMineralText(float minerals) {
            text.text = Mathf.FloorToInt(minerals) + "";
        }
    }
}