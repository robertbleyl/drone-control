﻿using Core.Events;
using DroneControl.Core;
using DroneControl.Player;
using UnityEngine;

namespace UI.HUD {
    public class TargetMarkerController : MonoBehaviour {
        [SerializeField]
        private GameObject image;

        private PlayerShipWeaponController weaponController;
        private Rigidbody targetRigidBody;

        private Camera cam;

        private void Start() {
            cam = Camera.main;
            image.SetActive(false);

            GameEvents.INSTANCE.onShowScreen += checkScreen;
            GameEvents.INSTANCE.onPlayerShipChange += playerSpawned;
            GameEvents.INSTANCE.onTargetChange += setTarget;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onShowScreen -= checkScreen;
            GameEvents.INSTANCE.onPlayerShipChange -= playerSpawned;
            GameEvents.INSTANCE.onTargetChange -= setTarget;
        }

        private void checkScreen(GameScreenType type) {
            enabled = type == GameScreenType.hud;
        }

        private void setTarget(GameObject target) {
            image.SetActive(false);
            if (target != null) {
                targetRigidBody = target.GetComponentInChildren<Rigidbody>();
            }
            else {
                targetRigidBody = null;
            }
        }

        public void playerSpawned(GameObject ship) {
            if (ship != null) {
                weaponController = ship.GetComponent<PlayerShipWeaponController>();
            }
            else {
                weaponController = null;
            }
        }

        private void Update() {
            if (weaponController != null && targetRigidBody != null) {
                Vector3 pos = weaponController.getTargetLeadPositionForActiveWeapons(targetRigidBody);

                pos = cam.WorldToScreenPoint(pos);

                if (pos.z > 0f) {
                    image.SetActive(true);
                    pos.z = 0f;
                    image.transform.position = pos;
                }
                else {
                    image.SetActive(false);
                }
            }
        }
    }
}