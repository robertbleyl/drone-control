﻿using AI;
using Core;
using Core.Events;
using DroneControl.Buildings;
using DroneControl.Core;
using DroneControl.Ship;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.UI;

namespace UI.HUD {
    public class TopInfoBarController : MonoBehaviour {
        [SerializeField]
        private Image turretCountIcon;

        [SerializeField]
        private Image slowZoneCountIcon;

        [SerializeField]
        private Image singularityCountIcon;

        [SerializeField]
        private Image wormholeCountIcon;

        [SerializeField]
        private Image harbingerCountIcon;

        [SerializeField]
        private Image ravenCountIcon;

        [SerializeField]
        private Image cyclopsCountIcon;

        [SerializeField]
        private Text droneCountLabel;

        [SerializeField]
        private Text turretCountLabel;

        [SerializeField]
        private Text slowZoneCountLabel;

        [SerializeField]
        private Text singularityCountLabel;

        [SerializeField]
        private Text wormholeCountLabel;

        [SerializeField]
        private Text harbingerCountLabel;

        [SerializeField]
        private Text ravenCountLabel;

        [SerializeField]
        private Text cyclopsCountLabel;

        private int botSpawnerCount;

        private void Start() {
            GameEvents.INSTANCE.onBotSpawnerActivated += onBotSpawnerActivated;
            GameEvents.INSTANCE.onBotSpawnerRemoved += onBotSpawnerRemoved;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onBotSpawnerActivated -= onBotSpawnerActivated;
            GameEvents.INSTANCE.onBotSpawnerRemoved -= onBotSpawnerRemoved;
        }

        private void onBotSpawnerActivated(IBotSpawner botSpawner) {
            botSpawnerCount++;
            wormholeCountLabel.text = botSpawnerCount + "";
        }

        private void onBotSpawnerRemoved(IBotSpawner botSpawner) {
            botSpawnerCount--;
            wormholeCountLabel.text = botSpawnerCount + "";
        }

        private void Update() {
            droneCountLabel.text = Drones.INSTANCE.getDrones().Count + "";
            turretCountLabel.text = BuildingsProvider.INSTANCE.getBuildings(BuildingType.TURRET).Count + "";
            slowZoneCountLabel.text = BuildingsProvider.INSTANCE.getBuildings(BuildingType.SLOW_ZONE).Count + "";
            singularityCountLabel.text = BuildingsProvider.INSTANCE.getBuildings(BuildingType.SINGULARITY).Count + "";
            harbingerCountLabel.text = Bots.INSTANCE.harbingerCount + "";
            ravenCountLabel.text = Bots.INSTANCE.ravenCount + "";
            cyclopsCountLabel.text = Bots.INSTANCE.cyclopsCount + "";
        }

        public void showTurretDisplay() {
            turretCountIcon.gameObject.SetActive(true);
            turretCountLabel.gameObject.SetActive(true);
        }

        public void showSlowZoneDisplay() {
            slowZoneCountIcon.gameObject.SetActive(true);
            slowZoneCountLabel.gameObject.SetActive(true);
        }

        public void showSingularityDisplay() {
            singularityCountIcon.gameObject.SetActive(true);
            singularityCountLabel.gameObject.SetActive(true);
        }

        public void showHarbingerDisplay() {
            harbingerCountIcon.gameObject.SetActive(true);
            harbingerCountLabel.gameObject.SetActive(true);
        }

        public void showRavenDisplay() {
            ravenCountIcon.gameObject.SetActive(true);
            ravenCountLabel.gameObject.SetActive(true);
        }

        public void showCyclopsDisplay() {
            cyclopsCountIcon.gameObject.SetActive(true);
            cyclopsCountLabel.gameObject.SetActive(true);
        }

        public void showWormholeDisplay() {
            wormholeCountIcon.gameObject.SetActive(true);
            wormholeCountLabel.gameObject.SetActive(true);
        }
    }
}