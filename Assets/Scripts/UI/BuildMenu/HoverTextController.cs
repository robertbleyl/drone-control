﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.BuildMenu {
    public class HoverTextController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

        [SerializeField]
        private GameObject hoverElement = null;

        public void OnPointerEnter (PointerEventData eventData) {
            if (hoverElement != null) {
                hoverElement.SetActive (true);
            }
        }

        public void OnPointerExit (PointerEventData eventData) {
            if (hoverElement != null) {
                hoverElement.SetActive (false);
            }
        }
    }
}