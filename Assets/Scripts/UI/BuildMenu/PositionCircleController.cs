﻿using Core;
using UnityEngine;

namespace UI.BuildMenu {
    public class PositionCircleController : MonoBehaviour {
        [SerializeField]
        private GameObject circlePrefab;

        private GameObject circle;
        private Camera cam;

        private void Start() {
            circle = Instantiate(circlePrefab, GameObject.FindGameObjectWithTag(Tags.BuildMenuOverlayCanvas).transform,
                true);
            cam = Camera.main;
        }

        private void OnDestroy() {
            Destroy(circle);
        }

        private void Update() {
            Vector3 screenPos = cam.WorldToScreenPoint(transform.position);

            if (screenPos.z > 0f) {
                screenPos.z = 0f;
                circle.transform.position = screenPos;
            }
        }
    }
}