﻿using Core;
using Core.Events;
using DroneControl.Core;
using DroneControl.Player;
using UnityEngine.UI;
using UnityEngine;

namespace UI.BuildMenu {
    public class BuildMenuController : MonoBehaviour {
        [SerializeField]
        private Text notificationTextLabel;

        [SerializeField]
        public Text selectDroneText;

        [SerializeField]
        public GameObject upgradeButtonPanel;

        [SerializeField]
        public GameObject buildTurretButton;

        [SerializeField]
        public GameObject buildInhibitorButton;

        [SerializeField]
        public GameObject buildSingularityButton;

        [SerializeField]
        public GameObject buildSlowZoneButton;

        [SerializeField]
        public GameObject respawnButton;

        [SerializeField]
        private GameObject objectiveTextPanel;

        [SerializeField]
        private Text objectiveText;

        [SerializeField]
        private Sprite defaultCursor;

        [SerializeField]
        private Sprite selectionCursor;

        [SerializeField]
        public bool disableRespawn;

        private float notificationTimer;

        private CursorController cursorController;

        private bool screenActive;

        private string originalObjectiveText;

        private GameObject currentShip;

        private void Start() {
            cursorController = GameObject.FindGameObjectWithTag(Tags.CursorController).GetComponent<CursorController>();

            objectiveText.text = "";
            notificationTextLabel.text = "";

            selectDroneText.enabled = false;
            objectiveTextPanel.SetActive(false);

            GameEvents.INSTANCE.onPlayerShipChange += playerShipChanged;
            GameEvents.INSTANCE.onPlayerShipExplosionFinished += onPlayerShipExplosionFinished;
            GameEvents.INSTANCE.onShowScreen += screenChanged;
            GameEvents.INSTANCE.onKeyBindingsChanged += onKeyBindingsChanged;

            showDroneSelection();
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onPlayerShipChange -= playerShipChanged;
            GameEvents.INSTANCE.onPlayerShipExplosionFinished -= onPlayerShipExplosionFinished;
            GameEvents.INSTANCE.onShowScreen -= screenChanged;
            GameEvents.INSTANCE.onKeyBindingsChanged -= onKeyBindingsChanged;
        }

        private void playerShipChanged(GameObject newPlayerShip) {
            currentShip = newPlayerShip;

            selectDroneText.enabled = false;

            if (!disableRespawn) {
                respawnButton.SetActive(newPlayerShip != null);
            }
        }

        private void onPlayerShipExplosionFinished() {
            if (!PlayerData.INSTANCE.hasLost) {
                showDroneSelection();
            }
        }

        private void screenChanged(GameScreenType type) {
            bool wasActive = screenActive;
            screenActive = type == GameScreenType.buildMenu;

            if (screenActive) {
                if (currentShip != null) {
                    showDefaultCursor();
                }
                else {
                    showSelectionCursor();
                }
            }
        }

        private void onKeyBindingsChanged() {
            if (originalObjectiveText != null) {
                // TODO
                // objectiveText.text = InputUtils.updateKeyBindingsInText (originalObjectiveText);
            }
        }

        public void showDefaultCursor() {
            cursorController.updateCursorImage(defaultCursor, false, false, Vector2.one / 2f);
        }

        private void Update() {
            if (screenActive) {
                if (notificationTimer > 0f) {
                    notificationTimer -= Time.deltaTime;

                    if (notificationTimer <= 0f) {
                        notificationTextLabel.text = "";
                    }
                }
            }
        }

        public void showDroneSelection() {
            respawnButton.SetActive(false);
            selectDroneText.enabled = true;
            showSelectionCursor();
        }

        private void showSelectionCursor() {
            cursorController.updateCursorImage(selectionCursor, false, false, Vector2.one / 2f);
        }

        public void setObjectiveText(string message) {
            originalObjectiveText = message;
            // TODO
            // objectiveText.text = InputUtils.updateKeyBindingsInText (message);
            objectiveTextPanel.SetActive(message?.Length > 0);
        }

        public void respawn() {
            GameEvents.INSTANCE.destroyGameObject(new DestructionEvent(currentShip));
            Destroy(currentShip);
            currentShip = null;
            GameEvents.INSTANCE.changePlayerShip(null);
            GameEvents.INSTANCE.changeTarget(null);

            GameEvents.INSTANCE.changeTarget(null);
            GameEvents.INSTANCE.playerShipExplosionFinished();
        }

        public void showNotification(string message, float time) {
            notificationTextLabel.text = message;
            notificationTimer = time;
        }
    }
}