﻿using Core;
using UnityEngine;

namespace UI.BuildMenu {
    public class BuildGridController : MonoBehaviour {
        private const float PLANE_SIZE = 10f;

        [SerializeField]
        private GameObject gridObject;

        public float gridMinX;
        public float gridMaxX;
        public float gridMinZ;
        public float gridMaxZ;
        public int gridSize;

        private void Start() {
            Vector3 gridPos = gridObject.transform.position;
            Vector3 gridScale = gridObject.transform.localScale;

            float gridScaleX = gridScale.x * PLANE_SIZE;
            float gridScaleZ = gridScale.z * PLANE_SIZE;
            float gridScaleXHalf = gridScaleX / 2f;
            float gridScaleZHalf = gridScaleZ / 2f;

            gridMinX = gridPos.x - gridScaleXHalf;
            gridMaxX = gridPos.x + gridScaleXHalf;
            gridMinZ = gridPos.z - gridScaleZHalf;
            gridMaxZ = gridPos.z + gridScaleZHalf;

            Vector3 gridTiling = gridObject.GetComponent<Renderer>().material.mainTextureScale;
            gridSize = (int)(gridScaleX / gridTiling.x);

            showBuildGrid(false);

            float gridScaleXQuarter = (gridScaleXHalf / 2f);
            float gridScaleZQuarter = (gridScaleZHalf / 2f);
            BuildGridBoundaries.minScrollX = gridPos.x - gridScaleXQuarter;
            BuildGridBoundaries.maxScrollX = gridPos.x + gridScaleXQuarter;
            BuildGridBoundaries.minScrollZ = gridPos.z - gridScaleZQuarter;
            BuildGridBoundaries.maxScrollZ = gridPos.z + gridScaleZQuarter;
        }

        public void showBuildGrid(bool active) {
            gridObject.SetActive(active);
        }
    }
}