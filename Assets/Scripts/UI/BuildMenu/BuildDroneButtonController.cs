﻿using AI;
using UnityEngine;

namespace UI.BuildMenu {
    public class BuildDroneButtonController : MonoBehaviour {
        [SerializeField]
        private DroneSpawnController droneSpawnController;

        [SerializeField]
        private BuildButtonController buildButtonController;

        private Camera cam;

        private void Start() {
            cam = Camera.main;
        }

        private void Update() {
            Vector3 screenPos = cam.WorldToScreenPoint(droneSpawnController.transform.position);

            if (screenPos.z > 0f) {
                screenPos.z = 0f;
                screenPos.y -= 50f;
                transform.position = screenPos;
            }
        }

        public void buildDrone() {
            PlayerData.INSTANCE.spendMinerals(buildButtonController.mineralCosts);
            droneSpawnController.spawnDrone();
        }
    }
}