﻿using System.Collections;
using Core;
using Core.Events;
using DroneControl.Core;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace UI.BuildMenu {
    public class TargetDisplayController : MonoBehaviour {
        [SerializeField]
        private Image shields;

        [SerializeField]
        private Image hitpoints;

        [SerializeField]
        private Image progress;

        [SerializeField]
        private Color32 unfriendlyColor;

        [SerializeField]
        private Vector3 desiredScale = new(1.5f, 1.5f, 1f);

        public GameObject target;

        private Camera cam;
        private ScreenController screenController;

        private void Start() {
            cam = Camera.main;
            screenController = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<ScreenController>();

            GameEvents.INSTANCE.onShowScreen += checkVisible;
            StartCoroutine(setupEnabled());
        }

        public void setUnfriendly() {
            hitpoints.color = unfriendlyColor;
        }

        private IEnumerator setupEnabled() {
            yield return new WaitForSecondsRealtime(0.1f);
            checkVisible(screenController.getActiveScreenType());
        }

        private void checkVisible(GameScreenType type) {
            gameObject.SetActive(type == GameScreenType.buildMenu);
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onShowScreen -= checkVisible;
        }

        public void Update() {
            transform.localScale = desiredScale;
            Vector3 screenPos = cam.WorldToScreenPoint(target.transform.position);

            if (screenPos.z > 0f) {
                screenPos.z = 0f;
                screenPos.y += 30f;
                transform.position = screenPos;
            }
        }

        public void updateShields(float percentage) {
            shields.fillAmount = percentage;
        }

        public void updateHitpoints(float percentage) {
            hitpoints.fillAmount = percentage;
        }

        public void updateProgress(float percentage) {
            progress.fillAmount = percentage;
        }
    }
}