﻿using Core.Events;
using DroneControl.Core;
using UnityEngine;
using UnityEngine.UI;

namespace UI.BuildMenu {
    public class UpgradeButtonController : MonoBehaviour {
        [SerializeField]
        private BuildingType buildingType;

        [SerializeField]
        private GameObject upgradedIconLevel1;

        [SerializeField]
        private GameObject upgradedIconLevel2;

        [SerializeField]
        private Button button;

        private BuildButtonController buildButtonController;

        private int level;

        private void Start() {
            buildButtonController = GetComponent<BuildButtonController>();

            upgradedIconLevel1.SetActive(false);
            upgradedIconLevel2.SetActive(false);
        }

        public void upgrade() {
            if (level < 2) {
                level++;

                if (level == 1) {
                    upgradedIconLevel1.SetActive(true);
                }
                else {
                    upgradedIconLevel2.SetActive(true);
                    button.interactable = false;
                }

                PlayerData.INSTANCE.spendMinerals(buildButtonController.mineralCosts);

                GameEvents.INSTANCE.buildingTypeUpgraded(new BuildingUpgradedEvent(buildingType, level));
            }
        }
    }
}