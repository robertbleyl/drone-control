﻿using System.Linq;
using Core;
using Core.Events;
using DroneControl.Buildings;
using DroneControl.Core;
using DroneControl.Player;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace UI.BuildMenu {
    public class AddBuildingButtonController : MonoBehaviour {
        [SerializeField]
        private GameObject buildingPrefabLevel0;

        [SerializeField]
        private GameObject buildingPrefabLevel1;

        [SerializeField]
        private GameObject buildingPrefabLevel2;

        [SerializeField]
        private BuildingType buildingType;

        private GameObject usedPrefab;

        private LocalPlayer localPlayer;
        private ShipInputController inputController;
        private BuildGridController buildGridController;
        private BuildButtonController buildButtonController;
        public CursorController cursorController;

        private bool addingBuilding;
        private GameObject buildingToBeAdded;
        private BuildingController buildingController;

        [FormerlySerializedAs("invalidBuildingPlacement")]
        public bool positionAlreadyTaken;

        private Camera cam;
        private PlayerInput playerInput;

        private void Start() {
            cam = Camera.main;
            usedPrefab = buildingPrefabLevel0;

            localPlayer = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<LocalPlayer>();
            inputController = localPlayer.GetComponent<ShipInputController>();

            buildGridController = GameObject.FindGameObjectWithTag(Tags.BuildGrid).GetComponent<BuildGridController>();
            buildButtonController = GetComponent<BuildButtonController>();
            cursorController = GameObject.FindGameObjectWithTag(Tags.CursorController).GetComponent<CursorController>();

            GameEvents.INSTANCE.onBuildingTypeUpgraded += onBuildingTypeUpgraded;

            playerInput = localPlayer.GetComponent<PlayerInput>();
            playerInput.actions["PlaceDeployable"].performed += placeDeployable;
            playerInput.actions["CancelDeployment"].performed += cancelDeployment;
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onBuildingTypeUpgraded -= onBuildingTypeUpgraded;

            playerInput.actions["PlaceDeployable"].performed -= placeDeployable;
            playerInput.actions["CancelDeployment"].performed -= cancelDeployment;
        }

        private void onBuildingTypeUpgraded(BuildingUpgradedEvent evt) {
            if (buildingType == evt.getBuildingType()) {
                usedPrefab = evt.getLevel() == 1 ? buildingPrefabLevel1 : buildingPrefabLevel2;
            }
        }

        private void Update() {
            if (!addingBuilding || buildGridController.gridSize == 0) {
                return;
            }

            Ray ray = cam.ScreenPointToRay(cursorController.getCurrentCursorPosition());
            Plane plane = new (Vector3.up, Vector3.zero);

            if (plane.Raycast(ray, out float distance)) {
                Vector3 point = ray.GetPoint(distance);

                int gridSize = buildGridController.gridSize;
                float gridMinX = buildGridController.gridMinX;
                float gridMaxX = buildGridController.gridMaxX;
                float gridMinZ = buildGridController.gridMinZ;
                float gridMaxZ = buildGridController.gridMaxZ;

                point.x = gridSize * ((int)point.x / gridSize);
                point.z = gridSize * ((int)point.z / gridSize);

                if (point.x < gridMinX) {
                    point.x = gridMinX;
                }
                else if (point.x > gridMaxX) {
                    point.x = gridMaxX;
                }

                if (point.z < gridMinZ) {
                    point.z = gridMinZ;
                }
                else if (point.z > gridMaxZ) {
                    point.z = gridMaxZ;
                }

                buildingToBeAdded.transform.position = point;
            }

            positionAlreadyTaken = BuildingsProvider.INSTANCE
                .getAllBuildings()
                .Count(b => b != null && b.GetComponent<BuildingController>().originalPos ==
                    buildingToBeAdded.transform.position) > 0;

            buildingController.showEffectUI(!positionAlreadyTaken);
            buildingController.showInvalidPlacementCircle(buildingController.cannotBePlaced || positionAlreadyTaken);
        }

        private void placeDeployable(InputAction.CallbackContext callbackContext) {
            if (buildingController != null && !buildingController.cannotBePlaced && !positionAlreadyTaken) {
                addingBuilding = false;
                inputController.placingBuilding = false;
                buildGridController.showBuildGrid(false);

                PlayerData.INSTANCE.increaseDeployedObjects();

                Rigidbody body = buildingToBeAdded.GetComponent<Rigidbody>();
                if (body != null) {
                    body.detectCollisions = true;
                }

                if (!localPlayer.godModeEnabled) {
                    PlayerData.INSTANCE.spendMinerals(buildButtonController.mineralCosts);
                }

                GameEvents.INSTANCE.addBuilding(buildingToBeAdded);

                buildingController = null;
            }
        }

        private void cancelDeployment(InputAction.CallbackContext callbackContext) {
            addingBuilding = false;
            inputController.placingBuilding = false;
            buildGridController.showBuildGrid(false);

            Destroy(buildingToBeAdded);
        }

        public void addBuilding() {
            addingBuilding = true;
            inputController.placingBuilding = true;
            buildGridController.showBuildGrid(true);

            buildingToBeAdded = Instantiate(usedPrefab, transform.parent.parent.parent.parent, true);

            Rigidbody body = buildingToBeAdded.GetComponent<Rigidbody>();
            if (body != null) {
                body.detectCollisions = false;
            }

            buildingController = buildingToBeAdded.GetComponent<BuildingController>();
            buildingController.showEffectUI(true);
        }

        public void addBuilding(GameObject toBeAdded) {
            addingBuilding = true;
            buildingToBeAdded = toBeAdded;
        }
    }
}