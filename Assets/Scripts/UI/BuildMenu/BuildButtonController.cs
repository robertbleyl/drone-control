﻿using Core.Events;
using UnityEngine.UI;
using UnityEngine;

namespace UI.BuildMenu {
    public class BuildButtonController : MonoBehaviour {
        [SerializeField]
        public int mineralCosts = 50;

        [SerializeField]
        protected Text mineralCostText;

        [SerializeField]
        protected Button button;

        private void Start() {
            mineralCostText.text = mineralCosts + "";
        }

        private void Update() {
            button.interactable = PlayerData.INSTANCE.getMinerals() >= mineralCosts;
        }

        public void mouseEntered() {
            GameEvents.INSTANCE.hoverBuildingButton(new BuildingButtonHoverEvent(gameObject, true));
        }

        public void mouseExited() {
            GameEvents.INSTANCE.hoverBuildingButton(new BuildingButtonHoverEvent(gameObject, false));
        }
    }
}