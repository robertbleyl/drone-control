﻿using Core;
using Core.Events;
using DroneControl.Ship;
using UnityEngine;

namespace UI.BuildMenu {
    public class TargetDisplayManager : MonoBehaviour {
        [SerializeField]
        private GameObject targetDisplayPrefab;

        private TargetDisplayController targetDisplayController;
        private GameObject targetDisplay;

        private bool init;

        private void Start() {
            GameObject buildMenuOverlayCanvas = GameObject.FindGameObjectWithTag(Tags.BuildMenuOverlayCanvas);

            if (buildMenuOverlayCanvas != null) {
                targetDisplay = Instantiate(targetDisplayPrefab, buildMenuOverlayCanvas.transform, true);

                targetDisplayController = targetDisplay.GetComponent<TargetDisplayController>();
                targetDisplayController.target = gameObject;

                if (GetComponent<AIShipWeaponController>() != null) {
                    targetDisplayController.setUnfriendly();
                }

                init = true;

                GameEvents.INSTANCE.onHealthChanged += onHealthChanged;
                GameEvents.INSTANCE.onTargetDisplayProgressChanged += onTargetDisplayProgressChanged;
            }
        }

        private void OnDestroy() {
            if (init) {
                Destroy(targetDisplay);
                GameEvents.INSTANCE.onHealthChanged -= onHealthChanged;
                GameEvents.INSTANCE.onTargetDisplayProgressChanged -= onTargetDisplayProgressChanged;
            }
        }

        private void onHealthChanged(HealthChangedEvent evt) {
            if (evt.gameObject == gameObject) {
                targetDisplayController.updateHitpoints(evt.hitPointPercentage);
                targetDisplayController.updateShields(evt.shieldPercentage);
            }
        }


        private void onTargetDisplayProgressChanged(TargetDisplayProgressChangedEvent evt) {
            if (init && evt.gameObject == gameObject) {
                targetDisplayController.updateProgress(evt.progress);
            }
        }
    }
}