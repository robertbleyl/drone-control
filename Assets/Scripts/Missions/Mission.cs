﻿using System.Collections.Generic;
using DroneControl.Ship;
using UnityEngine;

public class Mission : AbstractScene {
    [SerializeField]
    private List<MissionWave> waves = new();

    [SerializeField]
    private int nextWaveSeconds = 30;

    [SerializeField]
    private string nextMissionName;

    private int waveIndex;
    private MissionWave activeWave;
    private bool nextWaveInbound;

    protected override void Start() {
        base.Start();

        stageTimer = DEFAULT_NOTIFICATION_TIME;

        nextMissionSceneName = nextMissionName;
    }

    protected override void Update() {
        base.Update();

        if (endStarted) {
            return;
        }

        if (stageTimer > 0f) {
            stageTimer -= Time.deltaTime;

            if (stageTimer <= 0f) {
                stageTimer = 0f;

                activeWave = waves[waveIndex];
                activeWave.gameObject.SetActive(true);
                activeWave.activateWave();
                waveIndex++;
                nextWaveInbound = false;
            }
        }

        if (!nextWaveInbound && activeWave != null && activeWave.lastSpawnerActive() && waveIndex <= waves.Count - 1) {
            stageTimer = nextWaveSeconds;
            hudController.showNotification("Next wave starts in " + nextWaveSeconds + " seconds!",
                DEFAULT_NOTIFICATION_TIME);
            hudController.setNextWaveSeconds(nextWaveSeconds);
            nextWaveInbound = true;
        }

        if (activeWave != null && activeWave.allSpawnersGone() && waveIndex == waves.Count &&
            Bots.INSTANCE.getBotControllers().Count == 0) {
            hudController.showNotification("That was the last of them!", SUCCESS_END_NOTIFICATION_TIME);
            startSuccessEnd();
        }
    }
}