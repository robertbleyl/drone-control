﻿using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Events;
using UI.BuildMenu;
using UI.HUD;
using UnityEngine;

public class MissionWave : MonoBehaviour {
    [SerializeField]
    private string title;

    private HUDController hudController;
    private BuildMenuController buildMenuController;

    private List<IBotSpawner> botSpawners;

    private int removedSpawners;

    private void Awake() {
        hudController = GameObject.FindGameObjectWithTag(Tags.hud).GetComponent<HUDController>();
        buildMenuController = GameObject.FindGameObjectWithTag(Tags.buildMenu).GetComponent<BuildMenuController>();

        botSpawners = GetComponentsInChildren<IBotSpawner>().ToList();

        GameEvents.INSTANCE.onBotSpawnerRemoved += onBotSpawnerRemoved;
    }

    private void OnDestroy() {
        GameEvents.INSTANCE.onBotSpawnerRemoved -= onBotSpawnerRemoved;
    }

    private void onBotSpawnerRemoved(IBotSpawner botSpawner) {
        if (botSpawners.Contains(botSpawner)) {
            removedSpawners++;
        }
    }

    public void activateWave() {
        hudController.showNotification(title, 3f);
        buildMenuController.showNotification(title, 3f);

        foreach (IBotSpawner spawner in botSpawners) {
            spawner.activate();
        }
    }

    public bool lastSpawnerActive() {
        return botSpawners.Count - removedSpawners <= 1;
    }

    public bool allSpawnersGone() {
        return botSpawners.Count - removedSpawners == 0;
    }
}