using System.Collections;
using Core.Events;
using DroneControl.Buildings;
using UnityEngine;

public class Mission06 : Mission {

    [SerializeField]
    private GameObject singularityPrefab = null;

    protected override void Start () {
        base.Start ();

        GameObject singularity = Instantiate (singularityPrefab);
        singularity.GetComponent<BuildingController> ().showEffectUI (true);
        singularity.transform.position = transform.position;
        StartCoroutine (initSingularity (singularity));
    }

    private IEnumerator initSingularity (GameObject turret) {
        yield return new WaitForSecondsRealtime (0.1f);
        GameEvents.INSTANCE.addBuilding (turret);
    }
}
