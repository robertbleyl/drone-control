using System.Collections;
using Core.Events;
using DroneControl.Buildings;
using UnityEngine;

public class Mission05 : Mission {

    [SerializeField]
    private GameObject turretLevel0Prefab = null;
    [SerializeField]
    private GameObject turretLevel1Prefab = null;
    [SerializeField]
    private GameObject turretLevel2Prefab = null;

    protected override void Start () {
        base.Start ();

        addTurret (new Vector3 (0f, 0f, 500f), turretLevel0Prefab);
        addTurret (new Vector3 (500f, 0f, 500f), turretLevel1Prefab);
        addTurret (new Vector3 (300f, 0f, 900f), turretLevel2Prefab);
        addTurret (new Vector3 (-400f, 0f, 200f), turretLevel0Prefab);
        addTurret (new Vector3 (0f, 0f, -200f), turretLevel2Prefab);

        addTurret (new Vector3 (-1400f, 0f, -800f), turretLevel0Prefab);
        addTurret (new Vector3 (-1200f, 0f, -1500f), turretLevel1Prefab);
        addTurret (new Vector3 (-900f, 0f, -1000f), turretLevel2Prefab);
        addTurret (new Vector3 (-1600f, 0f, -1700f), turretLevel0Prefab);
        addTurret (new Vector3 (-1500f, 0f, -950f), turretLevel2Prefab);

        addTurret (new Vector3 (1300f, 0f, -700f), turretLevel0Prefab);
        addTurret (new Vector3 (1500f, 0f, -1600f), turretLevel1Prefab);
        addTurret (new Vector3 (700f, 0f, -1100f), turretLevel2Prefab);
        addTurret (new Vector3 (1400f, 0f, -1400f), turretLevel0Prefab);
        addTurret (new Vector3 (900f, 0f, -1200f), turretLevel2Prefab);
    }

    private void addTurret (Vector3 pos, GameObject prefab) {
        GameObject turret = Instantiate (prefab);
        turret.GetComponent<BuildingController> ().showEffectUI (true);
        turret.transform.position = pos;
        StartCoroutine (initTurret (turret));
    }

    private IEnumerator initTurret (GameObject turret) {
        yield return new WaitForSecondsRealtime (0.1f);
        GameEvents.INSTANCE.addBuilding (turret);
    }
}
