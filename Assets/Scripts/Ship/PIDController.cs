using UnityEngine;

namespace DroneControl.Ship {
    public class PIDController {
        private Vector3 lastError;
        private Vector3 derivative;
        private Vector3 integral;

        private readonly float errorWeight;
        private readonly float integralWeight;
        private readonly float derivativeWeight;

        public PIDController(float errorWeight, float integralWeight, float derivativeWeight) {
            this.errorWeight = errorWeight;
            this.integralWeight = integralWeight;
            this.derivativeWeight = derivativeWeight;
        }

        public Vector3 getCorrection(Vector3 error) {
            derivative = (error - lastError) / Time.deltaTime;
            integral += error * Time.deltaTime;
            lastError = error;

            Vector3 correction = (error * errorWeight) + (integral * integralWeight) + (derivative * derivativeWeight);
            return correction;
        }
    }
}