﻿using Core;
using Core.Events;
using DroneControl.Core;
using UnityEngine;

namespace DroneControl.Ship {
    public class DestructionController : MonoBehaviour {
        [SerializeField]
        private PoolableObjectType[] explosionTypes;

        [SerializeField]
        private float[] explosionTimes;

        [SerializeField]
        private GameObject standardModel;

        [SerializeField]
        private Rigidbody[] destructionElements;

        private PoolProvider poolProvider;

        private float timer;

        private int currentIndex;
        private int lastDoneIndex = -1;

        private void Start() {
            poolProvider = GameObject.FindGameObjectWithTag(Tags.PoolProvider).GetComponent<PoolProvider>();

            enabled = false;
        }

        public void startDestruction() {
            if (standardModel == null || destructionElements == null) {
                return;
            }

            standardModel.SetActive(false);

            enabled = true;
            Rigidbody standardBody = GetComponent<Rigidbody>();
            Vector3 currentVelocity = standardBody.velocity;
            Vector3 currentAngularVelocity = standardBody.angularVelocity;

            foreach (Rigidbody body in destructionElements) {
                body.gameObject.SetActive(true);
                body.velocity = currentVelocity + ((body.position - transform.position).normalized * 30f);
                body.angularVelocity = currentAngularVelocity;
            }
        }

        private void Update() {
            timer += Time.deltaTime;

            if (lastDoneIndex != currentIndex && timer >= explosionTimes[currentIndex]) {
                lastDoneIndex = currentIndex;

                foreach (Rigidbody body in destructionElements) {
                    GameObject explosion = poolProvider.next(explosionTypes[currentIndex]);
                    explosion.transform.SetParent(transform.parent);
                    explosion.transform.position = body.transform.position;
                    explosion.GetComponent<ExplosionController>()?.playExplosionSound();
                    explosion.SetActive(true);
                }

                currentIndex++;
            }

            if (currentIndex >= explosionTimes.Length) {
                GameEvents.INSTANCE.destroyGameObject(new DestructionEvent(gameObject));
                Destroy(gameObject);
            }
        }
    }
}