﻿using UnityEngine;

namespace Ship.Properties {
    public class EnergyController : MonoBehaviour {

        [SerializeField]
        private EnergyProperties energyProperties = null;

        private float currentEnergy;
        private float energyRegenStep;

        private float recoveryTimer;

        public bool disableRegeneration;

        public void Start () {
            currentEnergy = energyProperties.energy;
        }

        private void Update () {
            if (recoveryTimer >= 0f) {
                recoveryTimer -= Time.deltaTime;
                return;
            }

            if (!disableRegeneration && currentEnergy < energyProperties.energy) {
                energyRegenStep += Time.deltaTime;

                if (energyRegenStep >= 1f) {
                    energyRegenStep--;
                    currentEnergy += energyProperties.energyRegeneration;

                    if (currentEnergy > energyProperties.energy) {
                        currentEnergy = energyProperties.energy;
                    }
                }
            }
        }

        public float getDashEnergyCosts () {
            return energyProperties.energy / 3f;
        }

        public bool hasEnoughEnergy (float requiredAmount) {
            return currentEnergy >= requiredAmount;
        }

        public void spentEnergy (float amount) {
            currentEnergy -= amount;

            if (currentEnergy <= 0.05f) {
                currentEnergy = 0f;
                recoveryTimer = energyProperties.emptyEnergyRecoveryTime;
            }
        }

        public float getCurrentEnergyPercentage () {
            return currentEnergy / energyProperties.energy;
        }
    }
}