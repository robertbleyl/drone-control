﻿using System.Collections;
using Core.Events;
using DroneControl.Difficulty;
using DroneControl.Ship;
using DroneControl.Ship.Weapon;
using FMODUnity;
using UnityEngine;

namespace Ship.Properties {
    public class HealthController : MonoBehaviour {
        [SerializeField]
        private EventReference collisionAudioEvent;

        [SerializeField]
        private EventReference hitNotificationAudioEvent;

        [SerializeField]
        private float spawnInvincibilityTime = 2f;

        [SerializeField]
        private float shieldScaleFactor = 10f;

        [SerializeField]
        private float shieldEffectSpeedFactor = 2f;

        [SerializeField]
        private bool receivesCollisionDamage = true;

        [SerializeField]
        private float ammoGainFactorFromDashCollision = 0.75f;

        private ShieldImpactController[] shieldImpactControllers;

        private HealthProperties healthProperties;

        private float currentHitPoints;
        private float currentShields;

        private float shieldRegenStep;

        private float invincibilityTimer;
        private bool invincible = true;

        private int shieldImpactIndex;

        private SphereCollider sphereCollider;
        private float defaultColliderRadius;
        private bool changeColliderBasedOnDifficulty;

        private GameObject currentPlayerTarget;

        public void Awake() {
            healthProperties = GetComponent<HealthProperties>();

            currentHitPoints = healthProperties.hitPoints;
            currentShields = healthProperties.shields;

            StartCoroutine(initShieldImpactControllers());
        }

        private void Start() {
            sphereCollider = GetComponentInChildren<SphereCollider>();
            ShipProperties shipProperties = GetComponent<ShipProperties>();
            changeColliderBasedOnDifficulty = shipProperties != null && shipProperties.isEnemy;

            defaultColliderRadius = sphereCollider.radius;

            GameEvents.INSTANCE.onDifficultyChanged += onDifficultyChanged;
            GameEvents.INSTANCE.onTargetChange += onTargetChange;
            onDifficultyChanged();
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onDifficultyChanged -= onDifficultyChanged;
            GameEvents.INSTANCE.onTargetChange -= onTargetChange;
        }

        private void onDifficultyChanged() {
            if (changeColliderBasedOnDifficulty) {
                sphereCollider.radius = defaultColliderRadius * Difficulty.getValue(DifficultyItem.ENEMY_HIT_BOX_SIZE);
            }
        }

        private void onTargetChange(GameObject target) {
            currentPlayerTarget = target;
        }

        private IEnumerator initShieldImpactControllers() {
            yield return new WaitForSecondsRealtime(0.1f);
            shieldImpactControllers = GetComponentsInChildren<ShieldImpactController>();

            foreach (ShieldImpactController controller in shieldImpactControllers) {
                controller.init(transform, shieldScaleFactor, shieldEffectSpeedFactor);
            }
        }

        private void Update() {
            if (invincible) {
                invincibilityTimer += Time.deltaTime;

                if (invincibilityTimer >= spawnInvincibilityTime) {
                    invincible = false;
                }
            }

            regenerateShields();
        }

        private void regenerateShields() {
            if (currentShields < healthProperties.shields) {
                shieldRegenStep += Time.deltaTime;

                if (shieldRegenStep >= 1f) {
                    shieldRegenStep--;
                    currentShields += healthProperties.shieldRegeneration;

                    if (currentShields > healthProperties.shields) {
                        currentShields = healthProperties.shields;
                    }

                    fireHealthChangedEvent();
                }
            }
        }

        private void fireHealthChangedEvent() {
            float hitPointPercentage = currentHitPoints / healthProperties.hitPoints;
            float shieldPercentage = currentShields / healthProperties.shields;
            GameEvents.INSTANCE.healthChanged(new HealthChangedEvent(gameObject, hitPointPercentage, shieldPercentage));
        }

        private void OnCollisionEnter(Collision collision) {
            if (currentHitPoints <= 0f) {
                return;
            }

            WeaponFireController weaponFireController = collision.gameObject.GetComponent<WeaponFireController>();
            float damage;
            GameObject firingShip = null;
            float ammoGainFactor = 0f;

            if (weaponFireController != null) {
                damage = weaponFireController.weaponProperties.damage;
                firingShip = weaponFireController.firingShip;

                if (firingShip != null && firingShip.GetComponent<ShipProperties>().isEnemy) {
                    damage *= Difficulty.getValue(DifficultyItem.ENEMY_WEAPON_DAMAGE);
                }
                else if (firingShip == PlayerData.INSTANCE.currentPlayerShip) {
                    if (PlayerData.INSTANCE.godModeEnabled) {
                        damage = 9999999f;
                    }

                    if (damage < currentShields + currentHitPoints && weaponFireController.ammoGain > 0f) {
                        ammoGainFactor = weaponFireController.ammoGain;
                    }
                }

                GameEvents.INSTANCE.weaponHitShip(new WeaponHitEvent(firingShip));

                if (PlayerData.INSTANCE.currentPlayerShip == gameObject && !hitNotificationAudioEvent.IsNull) {
                    RuntimeManager.PlayOneShot(hitNotificationAudioEvent);
                }
            }
            else if (ammoGainFactorFromDashCollision > 0f &&
                     PlayerData.INSTANCE.currentPlayerShip == collision.gameObject &&
                     PlayerData.INSTANCE.currentPlayerShip.GetComponent<ShipMovementController>().dash !=
                     Vector3.zero) {
                damage = 100f;
                ammoGainFactor = ammoGainFactorFromDashCollision;
            }
            else if (!receivesCollisionDamage) {
                return;
            }
            else {
                damage = collision.relativeVelocity.magnitude / 25f;

                if (damage < currentShields + currentHitPoints) {
                    if (!collisionAudioEvent.IsNull) {
                        RuntimeManager.PlayOneShot(collisionAudioEvent, transform.position);
                    }
                }
            }

            if (ammoGainFactor > 0f) {
                GameEvents.INSTANCE.ammoReplenished(new AmmoReplenishEvent(ammoGainFactor));
            }

            showShieldImpact(collision.GetContact(0).point);

            if (damage < 1f) {
                damage = 1f;
            }

            receiveDamage(damage, firingShip);
        }

        public void showShieldImpact(Vector3 point) {
            if (currentShields > 0f && shieldImpactControllers?.Length > 0) {
                ShieldImpactController shieldImpactController = shieldImpactControllers[shieldImpactIndex];
                shieldImpactController.setImpactPoint(transform.InverseTransformPoint(point));
                shieldImpactIndex++;

                if (shieldImpactIndex >= shieldImpactControllers.Length) {
                    shieldImpactIndex = 0;
                }
            }
        }

        public void receiveDamage(float damage, GameObject firingShip) {
            if (invincible || currentHitPoints <= 0f) {
                return;
            }

            if (currentShields > 0) {
                currentShields -= damage;
                damage = -currentShields;

                if (currentShields < 0f) {
                    currentShields = 0f;
                }

                if (currentShields > 0f) {
                    fireHealthChangedEvent();
                    return;
                }
            }

            currentHitPoints -= damage;

            if (currentHitPoints <= 0f) {
                currentHitPoints = 0f;
            }

            fireHealthChangedEvent();

            if (currentHitPoints == 0f) {
                ShipMovementController shipMovementController = GetComponent<ShipMovementController>();

                if (shipMovementController != null) {
                    shipMovementController.enabled = false;
                }

                if (currentPlayerTarget == gameObject) {
                    GameEvents.INSTANCE.changeTarget(null);

                    if (PlayerData.INSTANCE.currentPlayerShip == firingShip) {
                        PlayerData.INSTANCE.addMinerals(15f);
                        GameEvents.INSTANCE.ammoReplenished(new AmmoReplenishEvent(1f));
                        PlayerData.INSTANCE.increaseKilledTargets();
                    }
                }
                else if (PlayerData.INSTANCE.currentPlayerShip == gameObject) {
                    PlayerData.INSTANCE.increasePlayerDeaths();
                }

                GameEvents.INSTANCE.startShipDestruction(gameObject);
                GetComponent<DestructionController>().startDestruction();

                enabled = false;
            }
        }

        public float getCurrentHitPointsPercentage() {
            return currentHitPoints / healthProperties.hitPoints;
        }

        public float getCurrentShieldPercentage() {
            return currentShields / healthProperties.shields;
        }
    }
}