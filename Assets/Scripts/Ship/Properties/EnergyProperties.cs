﻿using UnityEngine;

namespace Ship.Properties {
    public class EnergyProperties : MonoBehaviour {

        [SerializeField]
        public int energy;
        [SerializeField]
        public float energyRegeneration;
        [SerializeField]
        public float emptyEnergyRecoveryTime;
    }
}