﻿using UnityEngine;

namespace Ship.Properties {
    public class HealthProperties : MonoBehaviour {
        [SerializeField]
        public int hitPoints;

        [SerializeField]
        public int shields;

        [SerializeField]
        public float shieldRegeneration;
    }
}