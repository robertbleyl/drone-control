﻿using FMODUnity;
using UnityEngine;

public class ExplosionController : MonoBehaviour {
    [SerializeField]
    private EventReference audioEvent;

    [SerializeField]
    private float duration = 1.5f;

    private float elapsedDuration;

    public void playExplosionSound() {
        if (!audioEvent.IsNull) {
            RuntimeManager.PlayOneShot(audioEvent, transform.position);
        }
    }

    private void Update() {
        elapsedDuration += Time.deltaTime;

        if (elapsedDuration >= duration) {
            gameObject.SetActive(false);
        }
    }
}