﻿using System.Collections.Generic;
using Core;
using Core.Events;
using UnityEngine;

namespace DroneControl.Ship {
    public class Bots {
        public static readonly Bots INSTANCE = new();

        private readonly List<BotController> botControllers = new();

        public int harbingerCount;
        public int ravenCount;
        public int cyclopsCount;

        private readonly MusicController musicController;

        private Bots() {
            musicController = GameObject.FindGameObjectWithTag(Tags.Music).GetComponent<MusicController>();

            GameEvents.INSTANCE.onBotSpawned += addBot;
            GameEvents.INSTANCE.onGameObjectDestroyed += checkRemoveBot;
        }

        private void addBot(GameObject bot) {
            BotController botController = bot.GetComponent<BotController>();
            botControllers.Add(botController);

            string title = getShipTitle(bot);

            if (title.Equals("Raven")) {
                ravenCount++;
            }
            else if (title.Equals("Cyclops")) {
                cyclopsCount++;
            }
            else {
                harbingerCount++;
            }

            updateMusicIntensity();
        }

        private string getShipTitle(GameObject botController) {
            return botController.GetComponentInChildren<ShipProperties>().title;
        }

        private void updateMusicIntensity() {
            int intensity = botControllers.Count;

            if (intensity > 2) {
                intensity = 2;
            }

            musicController.setIntensity(intensity);
        }

        private void checkRemoveBot(DestructionEvent evt) {
            if (evt.gameObject.transform.parent != null) {
                GameObject bot = evt.gameObject.transform.parent.gameObject;

                BotController botController = bot.GetComponent<BotController>();
                int index = botControllers.IndexOf(botController);

                if (index >= 0) {
                    string title = getShipTitle(bot);

                    if (title.Equals("Raven")) {
                        ravenCount--;
                    }
                    else if (title.Equals("Cyclops")) {
                        cyclopsCount--;
                    }
                    else {
                        harbingerCount--;
                    }

                    botControllers.RemoveAt(index);
                    updateMusicIntensity();
                }
            }
        }

        public List<BotController> getBotControllers() {
            return botControllers;
        }

        public void clear() {
            botControllers.Clear();
        }
    }
}