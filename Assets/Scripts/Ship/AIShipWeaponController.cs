﻿using DroneControl.Ship.Weapon;
using UnityEngine;

namespace DroneControl.Ship {
    public class AIShipWeaponController : AbstractShipWeaponController {
        [SerializeField]
        private WeaponController botWeapon;

        public void fireWeapon(Vector3 direction, GameObject target) {
            botWeapon.fireWeapon(direction, gameObject, target);
        }

        public Vector3 getTargetLeadPosition(Rigidbody target) {
            return botWeapon.getTargetLeadPosition(target);
        }
    }
}