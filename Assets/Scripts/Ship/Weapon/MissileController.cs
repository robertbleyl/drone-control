﻿using UnityEngine;

namespace DroneControl.Ship.Weapon {
    public class MissileController : MonoBehaviour {
        [SerializeField]
        private float maxSpeed = 400f;

        [SerializeField]
        private float timeToMaxSpeed = 1f;

        public GameObject target;

        private float timer;

        private Rigidbody rigidBody;

        private void Start() {
            rigidBody = GetComponent<Rigidbody>();
        }

        private void Update() {
            if (target == null) {
                gameObject.SetActive(false);
                return;
            }

            timer += Time.deltaTime;
            float percentage = timer / timeToMaxSpeed;

            if (percentage > 1f) {
                percentage = 1f;
            }

            float speed = maxSpeed * percentage;
            rigidBody.velocity = (target.transform.position - transform.position).normalized * speed;
        }
    }
}