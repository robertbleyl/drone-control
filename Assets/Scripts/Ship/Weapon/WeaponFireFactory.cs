﻿using FMODUnity;
using UnityEngine;

namespace DroneControl.Ship.Weapon {
    public abstract class WeaponFireFactory : MonoBehaviour {
        protected WeaponProperties weaponProperties;

        private void Awake() {
            weaponProperties = GetComponent<WeaponProperties>();
        }

        public abstract GameObject fireWeapon(Vector3 startPoint, Vector3 direction, GameObject firingShip,
            GameObject target, EventReference audioEvent);
    }
}