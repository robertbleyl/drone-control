﻿using System.Collections;
using FMODUnity;
using UnityEngine;

namespace DroneControl.Ship.Weapon {
    public class RapidFireMissileFactory : ProjectileWeaponFireFactory {
        [SerializeField]
        private int count = 4;

        private int currentCount;

        public bool firing;

        public override GameObject fireWeapon(Vector3 startPoint, Vector3 direction, GameObject firingShip,
            GameObject target, EventReference audioEvent) {
            firing = true;
            currentCount = count;
            StartCoroutine(fireMissile(startPoint, direction, firingShip, target, audioEvent));

            return null;
        }

        private IEnumerator fireMissile(Vector3 startPoint, Vector3 direction, GameObject firingShip, GameObject target,
            EventReference audioEvent) {
            yield return new WaitForSeconds(0.2f);
            base.fireWeapon(startPoint, direction, firingShip, target, audioEvent);

            currentCount--;

            if (currentCount > 0) {
                StartCoroutine(fireMissile(startPoint, direction, firingShip, target, audioEvent));
            }
            else {
                firing = false;
            }
        }
    }
}