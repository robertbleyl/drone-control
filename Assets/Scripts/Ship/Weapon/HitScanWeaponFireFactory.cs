using Core;
using Core.Events;
using DroneControl.Core;
using FMODUnity;
using Ship.Properties;
using UnityEngine;

namespace DroneControl.Ship.Weapon {
    public class HitScanWeaponFireFactory : WeaponFireFactory {
        [SerializeField]
        private float visibilityTime = 0.5f;

        [SerializeField]
        private float minDamage = 4f;

        [SerializeField]
        private float maxDamage = 40f;

        [SerializeField]
        private float minRangeDamage = 100f;

        [SerializeField]
        private float maxRangeDamage = 600f;

        private LineRenderer line;

        private PoolProvider poolProvider;

        private float visibilityTimeLeft;

        private void Start() {
            line = GetComponent<LineRenderer>();
            line.enabled = false;

            poolProvider = GameObject.FindGameObjectWithTag(Tags.PoolProvider).GetComponent<PoolProvider>();
        }

        public override GameObject fireWeapon(Vector3 startPoint, Vector3 direction, GameObject firingShip,
            GameObject target, EventReference audioEvent) {
            line.SetPosition(0, transform.position);

            int layerMask = 1 << 8;
            layerMask = ~layerMask;

            if (Physics.Raycast(startPoint, direction, out RaycastHit hit, 5000f, layerMask)) {
                line.SetPosition(1, hit.point);

                GameObject explosion = poolProvider.next(PoolableObjectType.PROJECTILE_HIT_EXPLOSION);
                explosion.transform.parent = transform.parent.parent;
                explosion.transform.position = hit.point;

                if (hit.transform != null && hit.transform.gameObject != null) {
                    HealthController healthController = hit.transform.gameObject.GetComponent<HealthController>();

                    if (healthController != null) {
                        float damage = minDamage;
                        float dist = Mathf.Clamp(hit.distance, minRangeDamage, maxRangeDamage);
                        damage += (maxDamage - minDamage) * dist / (maxRangeDamage - minRangeDamage);

                        if (PlayerData.INSTANCE.godModeEnabled) {
                            damage = 999999f;
                        }

                        healthController.showShieldImpact(hit.point);
                        healthController.receiveDamage(damage, firingShip);
                        GameEvents.INSTANCE.weaponHitShip(new WeaponHitEvent(firingShip));
                    }
                }
            }
            else {
                line.SetPosition(1, transform.position + (direction * 5000f));
            }

            if (!audioEvent.IsNull) {
                RuntimeManager.PlayOneShot(audioEvent, firingShip.transform.position);
            }

            line.enabled = true;
            visibilityTimeLeft = visibilityTime;

            return null;
        }

        private void Update() {
            if (visibilityTimeLeft > 0f) {
                visibilityTimeLeft -= Time.deltaTime;

                reduceAlpha();

                if (visibilityTimeLeft <= 0f) {
                    visibilityTimeLeft = 0f;
                    line.enabled = false;
                }
            }
        }

        private void reduceAlpha() {
            Color color = line.startColor;
            color.a = visibilityTimeLeft / visibilityTime;
            line.startColor = color;
            line.endColor = color;
        }
    }
}