using Core.Events;
using FMODUnity;
using UnityEngine;

namespace DroneControl.Ship.Weapon {
    public class WeaponController : MonoBehaviour {
        [SerializeField]
        private GameObject[] points = new GameObject[1];

        [SerializeField]
        private bool useCameraAsStart;

        [SerializeField]
        private float spread;

        [SerializeField]
        private float noAmmoSoundCooldown = 1f;

        [SerializeField]
        private EventReference noAmmoSound;

        [SerializeField]
        private WeaponProperties weaponProperties;

        [SerializeField]
        private WeaponFireFactory weaponFireFactory;

        private int ammo;

        private float cooldown;
        private bool cooldownActive;

        public float slowZoneEffect;

        private float noAmmoSoundTimer;
        private bool ammoDisabled;

        private Camera cam;

        private GameObject ownedShip;

        private void Start() {
            cam = Camera.main;
            slowZoneEffect = 1f;

            ammo = weaponProperties.ammoCapacity;

            ownedShip = GetComponentInParent<ShipMovementController>()?.gameObject;
        }

        public void fireWeapon(Vector3 direction, GameObject firingShip, GameObject target) {
            if (cooldownActive) {
                return;
            }

            if (ammo >= points.Length) {
                cooldown = weaponProperties.cooldown;
                cooldownActive = true;

                foreach (GameObject point in points) {
                    if (ammo > 0) {
                        Vector3 startPoint = useCameraAsStart ? cam.transform.position : point.transform.position;

                        if (spread > 0f) {
                            direction += point.transform.localPosition * spread;
                        }

                        weaponFireFactory.fireWeapon(startPoint, direction, firingShip, target,
                            weaponProperties.audioEvent);

                        if (!ammoDisabled) {
                            ammo--;
                            fireAmmoChangedEvent();
                        }
                    }
                }
            }
            else if (noAmmoSoundTimer <= 0f) {
                RuntimeManager.PlayOneShot(noAmmoSound);
                noAmmoSoundTimer = noAmmoSoundCooldown;
            }
        }

        private void fireAmmoChangedEvent() {
            if (ownedShip != null) {
                GameEvents.INSTANCE.ammoChanged(new AmmoChangedEvent(gameObject, ownedShip, ammo,
                    getCurrentAmmoPercentage()));
            }
        }

        public Vector3 getTargetLeadPosition(Rigidbody target) {
            if (weaponFireFactory is ProjectileWeaponFireFactory) {
                Vector3 shooterVelocity = Vector3.zero;
                Vector3 targetVelocity = target.velocity;
                Vector3 targetLocation = target.transform.position;
                float weaponFireSpeed = weaponProperties.speed;

                return TargetLead.FirstOrderIntercept(transform.position, shooterVelocity, weaponFireSpeed,
                    targetLocation,
                    targetVelocity);
            }

            return target.transform.position;
        }

        private void Update() {
            if (cooldownActive) {
                cooldown -= Time.deltaTime * slowZoneEffect;

                if (cooldown <= 0f) {
                    cooldown = 0f;
                    cooldownActive = false;
                }
            }

            if (noAmmoSoundTimer > 0f) {
                noAmmoSoundTimer -= Time.deltaTime;
            }
        }

        public float getCooldownPercentage() {
            return cooldown / weaponProperties.cooldown;
        }

        public float getCurrentAmmoPercentage() {
            return (float)ammo / weaponProperties.ammoCapacity;
        }

        public int getCurrentAmmo() {
            return ammo;
        }

        public int getAmmoCapacity() {
            return weaponProperties.ammoCapacity;
        }

        public void addAmmo(int addAmmo) {
            ammo += addAmmo;

            if (ammo > weaponProperties.ammoCapacity) {
                ammo = weaponProperties.ammoCapacity;
            }

            fireAmmoChangedEvent();
        }

        public void disableAmmo(bool disable) {
            ammoDisabled = disable;
        }
    }
}