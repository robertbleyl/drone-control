﻿using DroneControl.Core;
using FMODUnity;
using UnityEngine;

namespace DroneControl.Ship.Weapon {
    public class WeaponProperties : MonoBehaviour {
        [SerializeField]
        public float cooldown;

        [SerializeField]
        public float lifeTime;

        [SerializeField]
        public float speed;

        [SerializeField]
        public float damage;

        [SerializeField]
        public int ammoCapacity;

        [SerializeField]
        public WeaponType weaponType;

        [SerializeField]
        public EventReference audioEvent;
    }
}