using Core;
using FMODUnity;
using UnityEngine;

namespace DroneControl.Ship.Weapon {
    public class ProjectileWeaponFireFactory : WeaponFireFactory {
        [SerializeField]
        private PoolableObjectType poolableObjectType;

        private PoolProvider poolProvider;

        private void Start() {
            poolProvider = GameObject.FindGameObjectWithTag(Tags.PoolProvider).GetComponent<PoolProvider>();
        }

        public override GameObject fireWeapon(Vector3 startPoint, Vector3 direction, GameObject firingShip,
            GameObject target, EventReference audioEvent) {
            GameObject projectile = poolProvider.next(poolableObjectType);
            projectile.transform.parent = transform.parent.parent.parent;
            projectile.transform.position = startPoint;
            projectile.transform.rotation = transform.rotation;

            Collider[] colliders = firingShip.GetComponentsInChildren<Collider>();
            Collider collider = projectile.GetComponent<Collider>();

            foreach (Collider col in colliders) {
                Physics.IgnoreCollision(col, collider, true);
            }

            WeaponFireController weaponFireController = projectile.GetComponent<WeaponFireController>();
            weaponFireController.weaponProperties = weaponProperties;
            weaponFireController.lifeTime = weaponProperties.lifeTime;
            weaponFireController.firingShip = firingShip;

            MissileController missileController = projectile.GetComponent<MissileController>();

            if (missileController != null) {
                missileController.target = target;
            }

            Rigidbody weaponFireRigidBody = projectile.GetComponent<Rigidbody>();
            float speedPercentage = firingShip.GetComponent<ShipMovementController>().speedPercentage;

            weaponFireRigidBody.velocity = direction.normalized * (weaponProperties.speed * speedPercentage);

            if (!audioEvent.IsNull) {
                RuntimeManager.PlayOneShot(audioEvent, firingShip.transform.position);
            }

            projectile.SetActive(true);
            return projectile;
        }
    }
}