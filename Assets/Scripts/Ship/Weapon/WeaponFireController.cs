﻿using Core;
using UnityEngine;

namespace DroneControl.Ship.Weapon {
    public class WeaponFireController : MonoBehaviour {
        public float lifeTime;
        public float ammoGain;
        public WeaponProperties weaponProperties;
        public GameObject firingShip;

        private PoolProvider poolProvider;

        private void Start() {
            poolProvider = GameObject.FindGameObjectWithTag(Tags.PoolProvider).GetComponent<PoolProvider>();
        }

        private void Update() {
            lifeTime -= Time.deltaTime;

            if (lifeTime <= 0) {
                gameObject.SetActive(false);
            }
        }

        private void OnCollisionEnter(Collision collision) {
            GameObject explosion = poolProvider.next(PoolableObjectType.PROJECTILE_HIT_EXPLOSION);
            explosion.transform.parent = transform.parent;
            explosion.transform.position = collision.contacts[0].point;
            explosion.SetActive(true);

            GameObject collisionGameObject = collision.gameObject;

            if (collisionGameObject != null) {
                ShipProperties shipProperties = collisionGameObject.GetComponent<ShipProperties>();

                if (shipProperties == null || !shipProperties.isEnemy) {
                    explosion.GetComponent<ExplosionController>().playExplosionSound();
                }
            }

            gameObject.SetActive(false);
        }
    }
}