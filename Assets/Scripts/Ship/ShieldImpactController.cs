﻿using UnityEngine;

namespace DroneControl.Ship {
    public class ShieldImpactController : MonoBehaviour {
        private Renderer shieldRenderer;
        private Material material;

        private Transform transitionTransform;
        private float effectSpeedFactor;

        private Vector3 impactPointInLocalPos;
        private float timer;

        private static readonly int IMPACT_POSITION = Shader.PropertyToID("ImpactPosition");

        private void Start() {
            shieldRenderer = GetComponent<Renderer>();
            material = shieldRenderer.material;
        }

        public void init(Transform transitionTransform, float scaleFactor, float effectSpeedFactor) {
            this.effectSpeedFactor = effectSpeedFactor;
            this.transitionTransform = transitionTransform;
            material.SetVector("ScaleFactor", new Vector3(0f, scaleFactor, 0f));
            enabled = false;
            shieldRenderer.enabled = false;
        }

        public void setImpactPoint(Vector3 impactPointInLocalPos) {
            this.impactPointInLocalPos = impactPointInLocalPos;
            timer = 0f;
            enabled = true;
            shieldRenderer.enabled = true;
        }

        private void Update() {
            if (timer >= 1.5f || transitionTransform == null) {
                enabled = false;
                shieldRenderer.enabled = false;
                return;
            }

            timer += Time.deltaTime;
            Vector3 impactPointInWorldPos = transitionTransform.TransformPoint(impactPointInLocalPos);

            material.SetVector(IMPACT_POSITION, impactPointInWorldPos);

            impactPointInLocalPos +=
                (impactPointInLocalPos - transitionTransform.InverseTransformPoint(transitionTransform.position))
                .normalized * (Time.deltaTime * effectSpeedFactor);
        }
    }
}