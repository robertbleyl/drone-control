﻿using System;
using Core.Events;
using DroneControl.Difficulty;
using FMODUnity;
using Ship;
using Ship.Properties;
using UnityEngine;

namespace DroneControl.Ship {
    public class ShipMovementController : AbstractMovementController {
        [SerializeField]
        public float forwardThrustPercentage;

        [SerializeField]
        public bool strafeLeft;

        [SerializeField]
        public bool strafeRight;

        [SerializeField]
        public bool rollLeft;

        [SerializeField]
        public bool rollRight;

        [SerializeField]
        public float yawPercentage;

        [SerializeField]
        public float pitchPercentage;

        [SerializeField]
        public bool dashForward;

        [SerializeField]
        public bool dashBackwards;

        [SerializeField]
        public bool dashLeft;

        [SerializeField]
        public bool dashRight;

        [SerializeField]
        public bool decoupled;

        public Vector3 acceleration;
        private Vector3 lastVelocity;

        private float currentMaxSpeed;
        private float currentAgility;

        private float drag;
        private ShipProperties shipProperties;
        private DashEnergyController dashEnergyController;
        private StudioEventEmitter engineEventEmitter;

        private Vector3 forward;
        private Vector3 strafe;

        public Vector3 dash = Vector3.zero;
        public float dashTimer;

        public float singularityFieldEffect;

        private readonly PIDController pidController = new(10f, 0f, 0.05f);

        protected override void Start() {
            base.Start();
            drag = rigidBody.drag;

            shipProperties = GetComponent<ShipProperties>();
            currentMaxSpeed = shipProperties.maxForwardSpeed;
            currentAgility = shipProperties.agility;

            dashEnergyController = GetComponent<DashEnergyController>();
            engineEventEmitter = GetComponent<StudioEventEmitter>();

            singularityFieldEffect = 1f;

            GameEvents.INSTANCE.onDifficultyChanged += onDifficultyChanged;

            onDifficultyChanged();
        }

        private void onDifficultyChanged() {
            if (shipProperties.isEnemy) {
                currentMaxSpeed = shipProperties.maxForwardSpeed * Difficulty.Difficulty.getValue(DifficultyItem.ENEMY_MOVEMENT_SPEED);
                currentAgility = shipProperties.agility * Difficulty.Difficulty.getValue(DifficultyItem.ENEMY_MANEUVERABILITY);
            }
        }

        private void OnDestroy() {
            GameEvents.INSTANCE.onDifficultyChanged -= onDifficultyChanged;
        }

        private void Update() {
            updateDash();

            if (decoupled) {
                rigidBody.drag = 0;
            }
            else {
                rigidBody.drag = drag;
                updateLinearForce();
            }

            updateAngularForce();
            updateEngineSound();
        }

        private void updateLinearForce() {
            Vector3 forwardDir = transform.forward;
            float currentForwardSpeed = Vector3.Dot(rigidBody.velocity, forwardDir);
            forward = updateLinearForce(forwardThrustPercentage, currentMaxSpeed * speedPercentage, forwardDir,
                currentForwardSpeed);

            if ((strafeLeft || strafeRight) && dashTimer == 0f) {
                float strafePercentage = strafeLeft ? -1 : 1;
                Vector3 right = transform.right;
                float currentStrafeSpeed = Vector3.Dot(rigidBody.velocity, right);
                strafe = updateLinearForce(strafePercentage, shipProperties.maxStrafeSpeed * speedPercentage, right,
                    currentStrafeSpeed);
            }
            else {
                strafe = Vector3.zero;
            }
        }

        private Vector3 updateLinearForce(float percentage, float max, Vector3 direction, float currentSpeed) {
            float targetSpeed = percentage * max;
            float targetSpeedDiff = Math.Abs(currentSpeed - targetSpeed);

            if (targetSpeedDiff > 0.3f) {
                Vector3 targetSpeedVector = direction * targetSpeed;
                Vector3 currentSpeedVector = direction * currentSpeed;

                if (targetSpeedDiff > 0f) {
                    Vector3 error = targetSpeedVector - currentSpeedVector;
                    return pidController.getCorrection(error) * singularityFieldEffect;
                }
                else {
                    Vector3 error = currentSpeedVector - targetSpeedVector;
                    return -pidController.getCorrection(error) * singularityFieldEffect;
                }
            }

            return Vector3.zero;
        }

        private void updateAngularForce() {
            pitch = Vector3.zero;
            yaw = Vector3.zero;
            roll = Vector3.zero;

            float agilityFactor = currentAgility * speedPercentage * Time.fixedDeltaTime;

            if (pitchPercentage != 0f) {
                pitch = rigidBody.transform.right * (agilityFactor * pitchPercentage);
            }

            if (yawPercentage != 0f) {
                yaw = rigidBody.transform.up * (agilityFactor * yawPercentage);
            }

            if (rollRight) {
                roll = -rigidBody.transform.forward * agilityFactor;
            }
            else if (rollLeft) {
                roll = rigidBody.transform.forward * agilityFactor;
            }
        }

        private void updateDash() {
            if (dashEnergyController == null) {
                return;
            }

            if (dashTimer == 0f && dash == Vector3.zero) {
                float dashCosts = dashEnergyController.getDashEnergyCosts();

                if (dashEnergyController.hasEnoughEnergy(dashCosts)) {
                    if (dashForward) {
                        dash = transform.forward * MAX_FORCE / 3f;
                        dashEnergyController.spentEnergy(dashCosts);
                    }
                    else if (dashBackwards) {
                        dash = -transform.forward * MAX_FORCE / 3f;
                        dashEnergyController.spentEnergy(dashCosts);
                    }
                    else if (dashLeft) {
                        dash = -transform.right.normalized * MAX_FORCE / 4f;
                        dashEnergyController.spentEnergy(dashCosts);
                    }
                    else if (dashRight) {
                        dash = transform.right.normalized * MAX_FORCE / 4f;
                        dashEnergyController.spentEnergy(dashCosts);
                    }
                }

                dashForward = false;
                dashBackwards = false;
                dashLeft = false;
                dashRight = false;
            }
            else {
                dashTimer += Time.deltaTime;

                if (dashTimer > 0.4f) {
                    dash = Vector3.zero;
                }

                if (dashTimer > 0.6f) {
                    dashTimer = 0f;
                }
            }
        }

        private void updateEngineSound() {
            engineEventEmitter.SetParameter("forwardThrust", forwardThrustPercentage);
        }

        private void FixedUpdate() {
            if (shipProperties != null) {
                Vector3 velocity = rigidBody.velocity;
                acceleration = (velocity - lastVelocity) / Time.fixedDeltaTime;
                lastVelocity = velocity;

                if (!decoupled) {
                    if (dashTimer == 0f) {
                        addForce(strafe);
                        addForce(forward);
                    }

                    addForce(dash);
                }

                addTorque(pitch);
                addTorque(yaw);
                addTorque(roll);
            }
        }
    }
}