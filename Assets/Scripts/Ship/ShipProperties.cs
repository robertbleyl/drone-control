﻿using UnityEngine;

namespace DroneControl.Ship {
    public class ShipProperties : MonoBehaviour {
        [SerializeField]
        public string title;

        [SerializeField]
        public int agility;

        [SerializeField]
        public int maxForwardSpeed;

        [SerializeField]
        public int maxStrafeSpeed;

        [SerializeField]
        public Vector3 cameraOffset;

        [SerializeField]
        public bool isEnemy;
    }
}