﻿using UnityEngine;

namespace Ship {
    public class AbstractMovementController : MonoBehaviour {

        protected const float MAX_FORCE = 1000f;

        protected Rigidbody rigidBody;

        protected Vector3 pitch;
        protected Vector3 yaw;
        protected Vector3 roll;

        public float speedPercentage = 1f;

        protected virtual void Start () {
            rigidBody = GetComponent<Rigidbody> ();
        }

        protected void addForce (Vector3 force) {
            if (!isNan (force) && force.magnitude > 0.05f) {
                force = limitForce (force);

                if (!isNan (force)) {
                    rigidBody.AddForce (force * speedPercentage, ForceMode.Force);
                }
            }
        }

        protected void addTorque (Vector3 torque) {
            if (!isNan (torque) && torque.magnitude > 0.05f) {
                torque = limitForce (torque);

                if (!isNan (torque)) {
                    rigidBody.AddTorque (torque * speedPercentage, ForceMode.Force);
                }
            }
        }

        protected Vector3 limitForce (Vector3 force) {
            if (force.magnitude > MAX_FORCE) {
                return force.normalized * MAX_FORCE;
            }

            return force;
        }

        protected bool isNan (Vector3 v) {
            return float.IsNaN (v.x) || float.IsNaN (v.y) || float.IsNaN (v.z);
        }
    }
}