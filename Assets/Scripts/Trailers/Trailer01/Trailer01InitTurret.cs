using System.Collections;
using Core.Events;
using DroneControl.Buildings;
using DroneControl.Core;
using UnityEngine;

public class Trailer01InitTurret : MonoBehaviour {

    [SerializeField]
    private GameObject turretLevel2Prefab = null;

    private void Start () {
        addTurret (new Vector3 (0f, 0f, 0f), turretLevel2Prefab);
    }

    private void addTurret (Vector3 pos, GameObject prefab) {
        GameObject turret = Instantiate (prefab);
        turret.GetComponent<BuildingController> ().showEffectUI (true);
        turret.transform.position = pos;
        StartCoroutine (initTurret (turret));
    }

    private IEnumerator initTurret (GameObject turret) {
        yield return new WaitForSecondsRealtime (0.1f);
        GameEvents.INSTANCE.addBuilding (turret);
    }
}
