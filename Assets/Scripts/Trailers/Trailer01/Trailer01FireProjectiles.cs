using DroneControl.Player;
using DroneControl.Ship.Weapon;
using UnityEngine;

public class Trailer01FireProjectiles : MonoBehaviour {

    [SerializeField]
    private PlayerShipWeaponController shipWeaponController = null;
    [SerializeField]
    private Rigidbody target = null;
    [SerializeField]
    private Vector3 targetLeadOffset = new Vector3 (-100, 0f, 0f);

    private void Update () {
        WeaponController controller = shipWeaponController.getActiveWeaponController ();
        Vector3 targetLeadPos = controller.getTargetLeadPosition (target);

        Vector3 direction = targetLeadPos - gameObject.transform.position + targetLeadOffset;
        controller.fireWeapon (direction, gameObject, null);
    }
}
