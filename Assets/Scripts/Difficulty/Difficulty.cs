﻿using System;
using System.Collections.Generic;
using Core.Events;
using DroneControl.Core;
using UnityEngine;

namespace DroneControl.Difficulty {
    public static class Difficulty {
        private const string OPTION_KEY_PREFIX = "Difficulty_";
        private const string OPTION_KEY_LEVEL = "Level_";
        private const string OPTION_KEY_VALUE = "Value_";

        private static readonly Dictionary<DifficultyItem, DifficultyValue> VALUES = new();

        static Difficulty() {
            foreach (DifficultyItem item in Enum.GetValues(typeof(DifficultyItem))) {
                string difficultyLevelString =
                    PlayerPrefs.GetString(OPTION_KEY_PREFIX + OPTION_KEY_LEVEL + item, "NORMAL");
                float value = PlayerPrefs.GetFloat(OPTION_KEY_PREFIX + OPTION_KEY_VALUE + item, 1f);

                if (!Enum.TryParse(difficultyLevelString, out DifficultyLevel level)) {
                    level = DifficultyLevel.NORMAL;
                }

                VALUES[item] = new DifficultyValue(level, value);
            }
        }

        public static float getValue(DifficultyItem item) {
            return VALUES[item].value;
        }

        public static DifficultyLevel getLevel(DifficultyItem item) {
            return VALUES[item].level;
        }

        public static DifficultyLevel? sameLevelForAllItems() {
            DifficultyLevel? globalLevel = null;

            foreach (DifficultyValue val in VALUES.Values) {
                if (globalLevel != null) {
                    if (globalLevel != val.level) {
                        return null;
                    }
                }
                else {
                    globalLevel = val.level;
                }
            }

            return globalLevel;
        }

        public static void changeValue(DifficultyItem item, DifficultyLevel level, float value) {
            VALUES[item] = new DifficultyValue(level, value);
            PlayerPrefs.SetString(OPTION_KEY_PREFIX + OPTION_KEY_LEVEL + item, level.ToString());
            PlayerPrefs.SetFloat(OPTION_KEY_PREFIX + OPTION_KEY_VALUE + item, value);
            GameEvents.INSTANCE.difficultyChanged();
        }

        public static int getScoreMultiplier() {
            int multiplier = 1;

            foreach (DifficultyValue val in VALUES.Values) {
                switch (val.level) {
                    case DifficultyLevel.VERY_EASY:
                        multiplier += 1;
                        break;
                    case DifficultyLevel.EASY:
                        multiplier += 2;
                        break;
                    case DifficultyLevel.NORMAL:
                        multiplier += 3;
                        break;
                    case DifficultyLevel.HARD:
                        multiplier += 4;
                        break;
                    case DifficultyLevel.VERY_HARD:
                        multiplier += 5;
                        break;
                }
            }

            return multiplier;
        }
    }
}