﻿namespace DroneControl.Difficulty {
    public class DifficultyValue {
        public readonly DifficultyLevel level;
        public readonly float value;

        public DifficultyValue(DifficultyLevel level, float value) {
            this.level = level;
            this.value = value;
        }
    }
}