﻿namespace DroneControl.Difficulty {
    public enum DifficultyLevel {
        VERY_EASY,
        EASY,
        NORMAL,
        HARD,
        VERY_HARD
    }
}