﻿namespace DroneControl.Difficulty {
    public enum DifficultyItem {
        DRONE_MINING_RATE,
        ENEMY_HIT_BOX_SIZE,
        ENEMY_WEAPON_DAMAGE,
        ENEMY_MOVEMENT_SPEED,
        ENEMY_MANEUVERABILITY
    }
}