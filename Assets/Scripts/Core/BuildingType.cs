﻿namespace DroneControl.Core {
    public enum BuildingType {
        TURRET,
        INHIBITOR,
        SLOW_ZONE,
        SINGULARITY
    }
}