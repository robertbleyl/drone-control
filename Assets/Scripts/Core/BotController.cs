using UnityEngine;

namespace Core {
    public interface BotController {
        GameObject getCurrentShip();
    }
}