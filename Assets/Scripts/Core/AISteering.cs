﻿using UnityEngine;

namespace DroneControl.Core {
    public class AISteering {
        private static readonly float MAX_DIFF = 0.02f;
        public static readonly float MAX_VIEW_ANGLE = 35f;
        protected static readonly float MAX_VIEW_ANGLE_TAN = Mathf.Tan(Mathf.Deg2Rad * MAX_VIEW_ANGLE);

        public float yawPercentage;
        public float pitchPercentage;

        public void steerToTargetLocation(Vector3 targetLocation, Transform transform) {
            Vector3 forward = transform.forward.normalized;
            Vector3 right = transform.right.normalized;
            Vector3 up = transform.up.normalized;
            Vector3 location = transform.position;

            Plane planeZY = new Plane(location, location + forward, location + up);
            Plane planeZX = new Plane(location, location + forward, location + right);

            yawPercentage = getRotationPercentage(planeZY, planeZX, forward, yawPercentage, targetLocation, transform);
            pitchPercentage = getRotationPercentage(planeZX, planeZY, forward, pitchPercentage, targetLocation, transform);
        }

        private float getRotationPercentage(Plane plane, Plane plane2, Vector3 forward, float currentPercentage,
            Vector3 targetLocation, Transform transform) {
            Vector3 closestPoint2 = plane2.ClosestPointOnPlane(targetLocation);
            Vector3 toTargetDirection = closestPoint2 - transform.position;
            float angle = Vector3.Angle(forward, toTargetDirection.normalized);

            float dist = plane.GetDistanceToPoint(targetLocation);
            float percentage = 0f;

            if (angle > MAX_VIEW_ANGLE) {
                percentage = dist < 0f ? 1f : -1f;
            }
            else {
                Vector3 closestPoint = plane.ClosestPointOnPlane(targetLocation);

                float maxDist = Vector3.Distance(closestPoint, transform.position) * MAX_VIEW_ANGLE_TAN;

                if (maxDist < Mathf.Abs(dist)) {
                    percentage = dist < 0f ? 1f : -1f;
                }
                else {
                    percentage = -dist / maxDist;
                }
            }

            float diff = currentPercentage - percentage;

            if (Mathf.Abs(diff) > MAX_DIFF) {
                percentage = currentPercentage + (diff < 0 ? MAX_DIFF : -MAX_DIFF);
            }

            return Mathf.Clamp(percentage, -1f, 1f);
        }
    }
}