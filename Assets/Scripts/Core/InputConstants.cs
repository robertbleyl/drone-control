namespace Core {
    public class InputConstants {

        public const string INPUT_SCHEME_GAMEPAD = "Gamepad";
        public const string INPUT_SCHEME_KEYBOARD_AND_MOUSE = "KeyboardAndMouse";

        public const string INPUT_MAP_SHIP_INPUTS = "ShipInputs";
        public const string INPUT_MAP_BUILD_MENU_INPUTS = "BuildMenuInputs";
        public const string INPUT_MAP_UI_INPUTS = "UI";
    }
}
