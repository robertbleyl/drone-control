using System.Collections.Generic;
using Core.Events;

namespace Core {
    public class BotSpawners {
        public static readonly BotSpawners INSTANCE = new();

        private readonly List<IBotSpawner> botSpawners = new();

        private BotSpawners() {
            GameEvents.INSTANCE.onBotSpawnerActivated += addBotSpawner;
            GameEvents.INSTANCE.onBotSpawnerRemoved += removeBotSpawner;
        }

        public void addBotSpawner(IBotSpawner botSpawner) {
            botSpawners.Add(botSpawner);
        }

        private void removeBotSpawner(IBotSpawner botSpawner) {
            botSpawners.Remove(botSpawner);
        }

        public List<IBotSpawner> getBotSpawners() {
            return botSpawners;
        }

        public void clear() {
            botSpawners.Clear();
        }
    }
}