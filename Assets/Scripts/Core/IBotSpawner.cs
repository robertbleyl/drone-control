using UnityEngine;

namespace Core {
    public interface IBotSpawner {
        void setShowInRangeCircle(bool show);

        Vector3 getPosition();

        void activate();
        
        void destroy();
    }
}