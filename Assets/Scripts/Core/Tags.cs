namespace Core {
    public static class Tags {
        public const string hud = "hud";
        public const string BotSpawner = "BotSpawner";
        public const string Asteroid = "Asteroid";
        public const string MotherShip = "MotherShip";
        public const string DronePath = "DronePath";
        public const string BotPlayer = "BotPlayer";
        public const string Player = "Player";
        public const string AsteroidMineralField = "AsteroidMineralField";
        public const string BuildGrid = "BuildGrid";
        public const string BuildDroneButton = "BuildDroneButton";
        public const string buildMenu = "buildMenu";
        public const string BuildMenuOverlayCanvas = "BuildMenuOverlayCanvas";
        public const string endGame = "endGame";
        public const string playerDestroyedScreen = "playerDestroyedScreen";
        public const string DroneSpawner = "DroneSpawner";
        public const string Music = "Music";
        public const string PoolProvider = "PoolProvider";
        public const string CursorController = "CursorController";
    }
}