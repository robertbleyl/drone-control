namespace DroneControl.Core {
    public enum GameScreenType {
        hud,
        buildMenu,
        playerDestroyedScreen,
        pauseMenu,
        endGame
    }
}