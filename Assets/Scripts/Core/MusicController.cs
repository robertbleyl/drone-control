﻿using FMOD.Studio;
using FMODUnity;
using UnityEngine;

namespace Core {
    public class MusicController : MonoBehaviour {
        [SerializeField]
        private EventReference musicEvent;

        [SerializeField]
        private bool disabled;

        private EventInstance musicEventInstance;

        private float stopMusicTime;
        private float stopMusicTimer;

        private void Awake() {
            if (GameObject.FindGameObjectsWithTag(Tags.Music).Length > 1) {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);

            if (disabled) {
                return;
            }

            musicEventInstance = FMODUnity.RuntimeManager.CreateInstance(musicEvent);
            musicEventInstance.start();
            setIntensity(0);
        }

        private void Update() {
            if (stopMusicTimer > 0f) {
                stopMusicTimer -= Time.deltaTime;

                if (stopMusicTimer <= 0f) {
                    musicEventInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                    musicEventInstance.release();
                    Destroy(gameObject);
                }
                else {
                    musicEventInstance.setVolume(stopMusicTimer / stopMusicTime);
                }
            }
        }

        public void setIntensity(int intensity) {
            musicEventInstance.setParameterByName("intensity", intensity);
        }

        public void stopMusic(float stopMusicTime) {
            this.stopMusicTime = stopMusicTime;
            stopMusicTimer = stopMusicTime;
        }
    }
}