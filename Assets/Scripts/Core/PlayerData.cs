using System;
using Core.Events;
using UnityEngine;

public class PlayerData {
    public static readonly PlayerData INSTANCE = new();

    public GameObject currentPlayerShip;
    public bool hasLost;
    public bool godModeEnabled;

    private float minerals;

    private int lostDrones;
    private int playerDeaths;
    private int killedTargets;
    private float spentMinerals;
    private int deployedObjects;


    private PlayerData() {
    }

    public void initMinerals(float initAmount) {
        minerals = initAmount;
    }

    public void addMinerals(float amount) {
        if (amount <= 0) {
            throw new ArgumentException("amount must be higher than zero!");
        }

        minerals += amount;
        GameEvents.INSTANCE.playerMineralsChanged(minerals);
    }

    public void spendMinerals(float amount) {
        if (amount <= 0) {
            throw new ArgumentException("amount must be higher than zero!");
        }

        minerals -= amount;
        spentMinerals += amount;
        GameEvents.INSTANCE.playerMineralsChanged(minerals);
    }

    public float getMinerals() {
        return minerals;
    }

    public void increaseLostDrones() {
        lostDrones++;
    }

    public void increasePlayerDeaths() {
        playerDeaths++;
    }

    public void increaseKilledTargets() {
        killedTargets++;
    }

    public void increaseSpentMinerals() {
        spentMinerals++;
    }

    public void increaseDeployedObjects() {
        deployedObjects++;
    }

    public int getLostDrones() {
        return lostDrones;
    }

    public int getPlayerDeaths() {
        return playerDeaths;
    }

    public int getKilledTargets() {
        return killedTargets;
    }

    public float getSpentMinerals() {
        return spentMinerals;
    }

    public int getDeployedObjects() {
        return deployedObjects;
    }
}