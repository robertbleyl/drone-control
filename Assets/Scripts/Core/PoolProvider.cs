using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

namespace Core {
    public class PoolProvider : MonoBehaviour {
        [SerializeField]
        private GameObject gatlingGunProjectilePrefab;

        [SerializeField]
        private GameObject pulseCannonProjectilePrefab;

        [SerializeField]
        private GameObject termiteMissilePrefab;

        [SerializeField]
        private GameObject harbingerProjectilePrefab;

        [SerializeField]
        private GameObject ravenMissilePrefab;

        [SerializeField]
        private GameObject cyclopsBombPrefab;

        [SerializeField]
        private GameObject projectileHitExplosionPrefab;

        [SerializeField]
        private GameObject smallSilentExplosionPrefab;

        [SerializeField]
        private GameObject shipExplosionPrefab;

        private readonly Dictionary<PoolableObjectType, ObjectPool<GameObject>> pools = new();

        private void Start() {
            pools[PoolableObjectType.GATLING_GUN_PROJECTILE] = new ObjectPool<GameObject>(gatlingGunProjectile);
            pools[PoolableObjectType.PULSE_CANNON_PROJECTILE] = new ObjectPool<GameObject>(pulseCannonProjectile);
            pools[PoolableObjectType.TERMITE_MISSILE] = new ObjectPool<GameObject>(termiteMissile);

            pools[PoolableObjectType.HARBINGER_PROJECTILE] = new ObjectPool<GameObject>(harbingerProjectile);
            pools[PoolableObjectType.RAVEN_MISSILES] = new ObjectPool<GameObject>(ravenMissile);
            pools[PoolableObjectType.CYCLOPS_BOMBS] = new ObjectPool<GameObject>(cyclopsBomb);

            pools[PoolableObjectType.PROJECTILE_HIT_EXPLOSION] = new ObjectPool<GameObject>(projectileHitExplosion);
            pools[PoolableObjectType.SMALL_SILENT_EXPLOSION] = new ObjectPool<GameObject>(smallSilentExplosion);
            pools[PoolableObjectType.SHIP_EXPLOSION] = new ObjectPool<GameObject>(shipExplosion);
        }

        private GameObject gatlingGunProjectile() {
            return Instantiate(gatlingGunProjectilePrefab);
        }

        private GameObject pulseCannonProjectile() {
            return Instantiate(pulseCannonProjectilePrefab);
        }

        private GameObject termiteMissile() {
            return Instantiate(termiteMissilePrefab);
        }

        private GameObject harbingerProjectile() {
            return Instantiate(harbingerProjectilePrefab);
        }

        private GameObject ravenMissile() {
            return Instantiate(ravenMissilePrefab);
        }

        private GameObject cyclopsBomb() {
            return Instantiate(cyclopsBombPrefab);
        }

        private GameObject projectileHitExplosion() {
            return Instantiate(projectileHitExplosionPrefab);
        }

        private GameObject smallSilentExplosion() {
            return Instantiate(smallSilentExplosionPrefab);
        }

        private GameObject shipExplosion() {
            return Instantiate(shipExplosionPrefab);
        }

        public GameObject next(PoolableObjectType type) {
            return pools[type].Get();
        }
    }
}