namespace Core {
    public enum PoolableObjectType {

        GATLING_GUN_PROJECTILE,
        PULSE_CANNON_PROJECTILE,
        TERMITE_MISSILE,

        HARBINGER_PROJECTILE,
        RAVEN_MISSILES,
        CYCLOPS_BOMBS,

        PROJECTILE_HIT_EXPLOSION,
        SMALL_SILENT_EXPLOSION,
        SHIP_EXPLOSION
    }
}