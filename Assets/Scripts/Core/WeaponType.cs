﻿namespace DroneControl.Core {
    public enum WeaponType {

        GATLING_GUN,
        RAIL_GUN,
        PULSE_CANNON,
        MISSILES
    }
}