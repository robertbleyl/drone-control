using UnityEngine;

[CreateAssetMenu(fileName = "Mission", menuName = "Create Mission", order = 1)]
public class Mission : ScriptableObject {

    public string id;
    public string displayName;
    public Vector3 position;
}