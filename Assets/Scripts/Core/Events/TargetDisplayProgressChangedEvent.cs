using UnityEngine;

namespace Core.Events {
    public class TargetDisplayProgressChangedEvent {
        public GameObject gameObject { get; }
        public float progress { get; }

        public TargetDisplayProgressChangedEvent(GameObject gameObject, float progress) {
            this.gameObject = gameObject;
            this.progress = progress;
        }
    }
}