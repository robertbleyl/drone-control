using DroneControl.Core;
using UnityEngine;

namespace Core.Events {
    public class WeaponSwitchedEvent {
        public WeaponType weaponType { get; }
        public GameObject ownedShip { get; }
        public float newAmmoPercentage { get; }

        public WeaponSwitchedEvent(WeaponType weaponType, GameObject ownedShip, float newAmmoPercentage) {
            this.weaponType = weaponType;
            this.ownedShip = ownedShip;
            this.newAmmoPercentage = newAmmoPercentage;
        }
    }
}