﻿using DroneControl.Core;

namespace Core.Events {
    public class BuildingUpgradedEvent {
        private readonly BuildingType buildingType;
        private readonly int level;

        public BuildingUpgradedEvent(BuildingType buildingType, int level) {
            this.buildingType = buildingType;
            this.level = level;
        }

        public BuildingType getBuildingType() {
            return buildingType;
        }

        public int getLevel() {
            return level;
        }
    }
}