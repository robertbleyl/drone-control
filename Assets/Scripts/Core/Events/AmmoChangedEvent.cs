using UnityEngine;

namespace Core.Events {
    public class AmmoChangedEvent {
        public GameObject weapon { get; }
        public GameObject ownedShip { get; }
        public int newAmmoValue { get; }
        public float newAmmoPercentage { get; }

        public AmmoChangedEvent(GameObject weapon, GameObject ownedShip, int newAmmoValue, float newAmmoPercentage) {
            this.weapon = weapon;
            this.ownedShip = ownedShip;
            this.newAmmoValue = newAmmoValue;
            this.newAmmoPercentage = newAmmoPercentage;
        }
    }
}