using UnityEngine;

namespace Core.Events {
    public class HealthChangedEvent {
        public GameObject gameObject { get; }
        public float hitPointPercentage { get; }
        public float shieldPercentage { get; }

        public HealthChangedEvent(GameObject gameObject, float hitPointPercentage, float shieldPercentage) {
            this.gameObject = gameObject;
            this.hitPointPercentage = hitPointPercentage;
            this.shieldPercentage = shieldPercentage;
        }
    }
}