using UnityEngine;

namespace Core.Events {
    public class BuildingButtonHoverEvent {
        public GameObject button { get; }
        public bool hovering { get; }

        public BuildingButtonHoverEvent(GameObject button, bool hovering) {
            this.button = button;
            this.hovering = hovering;
        }
    }
}