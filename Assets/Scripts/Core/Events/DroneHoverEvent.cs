using UnityEngine;

namespace Core.Events {
    public class DroneHoverEvent {
        public GameObject drone { get; }
        public bool hovering { get; }

        public DroneHoverEvent(GameObject drone, bool hovering) {
            this.drone = drone;
            this.hovering = hovering;
        }
    }
}