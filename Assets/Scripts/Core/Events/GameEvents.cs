﻿using System;
using DroneControl.Core;
using UnityEngine;

namespace Core.Events {
    public class GameEvents {
        public static readonly GameEvents INSTANCE = new();

        public event Action<GameObject> onTargetChange;

        public event Action<GameObject> onPlayerShipChange;

        public event Action<GameScreenType> onShowScreen;

        public event Action onTogglePauseMenu;

        public event Action<DestructionEvent> onGameObjectDestroyed;

        public event Action<GameObject> onStartShipDestruction;

        public event Action onPlayerShipExplosionFinished;

        public event Action<GameObject> onBuildDrone;

        public event Action<GameObject> onReplaceDrone;

        public event Action<DroneHoverEvent> onHoverDrone;

        public event Action<GameObject> onBotSpawned;

        public event Action<GameObject> onBuildingAdded;

        public event Action<WeaponHitEvent> onWeaponHitShip;

        public event Action<AmmoReplenishEvent> onAmmoReplenished;

        public event Action<IBotSpawner> onBotSpawnerActivated;

        public event Action<IBotSpawner> onBotSpawnerRemoved;

        public event Action<BuildingUpgradedEvent> onBuildingTypeUpgraded;

        public event Action<BuildingButtonHoverEvent> onHoverBuildingButton;

        public event Action<WeaponSwitchedEvent> onWeaponTypeChanged;

        public event Action onDifficultyChanged;

        public event Action onKeyBindingsChanged;

        public event Action<AmmoChangedEvent> onAmmoChanged;

        public event Action<float> onPlayerMineralsChanged;

        public event Action<HealthChangedEvent> onHealthChanged;

        public event Action<TargetDisplayProgressChangedEvent> onTargetDisplayProgressChanged;

        public void changeTarget(GameObject target) {
            onTargetChange?.Invoke(target);
        }

        public void changePlayerShip(GameObject ship) {
            onPlayerShipChange?.Invoke(ship);
        }

        public void showScreen(GameScreenType type) {
            onShowScreen?.Invoke(type);
        }

        public void togglePauseMenu() {
            onTogglePauseMenu?.Invoke();
        }

        public void destroyGameObject(DestructionEvent evt) {
            onGameObjectDestroyed?.Invoke(evt);
        }

        public void startShipDestruction(GameObject ship) {
            onStartShipDestruction?.Invoke(ship);
        }

        public void playerShipExplosionFinished() {
            onPlayerShipExplosionFinished?.Invoke();
        }

        public void buildDrone(GameObject drone) {
            onBuildDrone?.Invoke(drone);
        }

        public void replaceDrone(GameObject drone) {
            onReplaceDrone?.Invoke(drone);
        }

        public void hoverDrone(DroneHoverEvent drone) {
            onHoverDrone?.Invoke(drone);
        }

        public void botSpawned(GameObject bot) {
            onBotSpawned?.Invoke(bot);
        }

        public void addBuilding(GameObject building) {
            onBuildingAdded?.Invoke(building);
        }

        public void weaponHitShip(WeaponHitEvent evt) {
            onWeaponHitShip?.Invoke(evt);
        }

        public void ammoReplenished(AmmoReplenishEvent evt) {
            onAmmoReplenished?.Invoke(evt);
        }

        public void botSpawnerActivated(IBotSpawner botSpawner) {
            onBotSpawnerActivated?.Invoke(botSpawner);
        }

        public void botSpawnerRemoved(IBotSpawner botSpawner) {
            onBotSpawnerRemoved?.Invoke(botSpawner);
        }

        public void buildingTypeUpgraded(BuildingUpgradedEvent evt) {
            onBuildingTypeUpgraded?.Invoke(evt);
        }

        public void hoverBuildingButton(BuildingButtonHoverEvent evt) {
            onHoverBuildingButton?.Invoke(evt);
        }

        public void weaponTypeChanged(WeaponSwitchedEvent evt) {
            onWeaponTypeChanged?.Invoke(evt);
        }

        public void ammoChanged(AmmoChangedEvent ammoChangedEvent) {
            onAmmoChanged?.Invoke(ammoChangedEvent);
        }

        public void difficultyChanged() {
            onDifficultyChanged?.Invoke();
        }

        public void keyBindingsChanged() {
            onKeyBindingsChanged?.Invoke();
        }

        public void playerMineralsChanged(float minerals) {
            onPlayerMineralsChanged?.Invoke(minerals);
        }

        public void healthChanged(HealthChangedEvent healthChangedEvent) {
            onHealthChanged?.Invoke(healthChangedEvent);
        }

        public void targetDisplayProgressChanged(TargetDisplayProgressChangedEvent evt) {
            onTargetDisplayProgressChanged?.Invoke(evt);
        }
    }
}