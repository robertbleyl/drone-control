namespace Core.Events {
    public class AmmoReplenishEvent {
        public float gainFactor { get; }

        public AmmoReplenishEvent(float gainFactor) {
            this.gainFactor = gainFactor;
        }
    }
}