using UnityEngine;

namespace Core.Events {
    public class DestructionEvent {
        public GameObject gameObject { get; }

        public DestructionEvent(GameObject gameObject) {
            this.gameObject = gameObject;
        }
    }
}