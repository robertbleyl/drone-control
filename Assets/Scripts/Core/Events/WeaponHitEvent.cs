﻿using UnityEngine;

namespace Core.Events {
    public class WeaponHitEvent {
        public GameObject firingShip { get; }

        public WeaponHitEvent(GameObject firingShip) {
            this.firingShip = firingShip;
        }
    }
}