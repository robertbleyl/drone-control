﻿using Core;
using Core.Events;
using DroneControl.Core;
using DroneControl.Ship;
using DroneControl.Ship.Weapon;
using Ship;
using UnityEngine;
using UnityEngine.UI;

namespace DroneControl.Buildings {
    public class TurretController : AbstractMovementController {
        [SerializeField]
        private float radius = 700f;

        [SerializeField]
        private GameObject rangeCirclePrefab;

        private WeaponController weaponController;
        private BuildingController buildingController;

        private GameObject rangeCircle;
        private Image rangeCircleImage;
        private bool initialized;

        private readonly AISteering steering = new();
        private Camera cam;

        protected override void Start() {
            base.Start();
            cam = Camera.main;

            weaponController = GetComponentInChildren<WeaponController>();
            buildingController = GetComponent<BuildingController>();

            rangeCircle = Instantiate(rangeCirclePrefab,
                GameObject.FindGameObjectWithTag(Tags.BuildMenuOverlayCanvas).transform, true);
            rangeCircleImage = rangeCircle.GetComponent<Image>();

            GameEvents.INSTANCE.onBuildingAdded += onBuildingAdded;
        }

        private void onBuildingAdded(GameObject building) {
            if (building == gameObject) {
                initialized = true;
            }
        }

        private void OnDestroy() {
            Destroy(rangeCircle);
            GameEvents.INSTANCE.onBuildingAdded -= onBuildingAdded;
        }

        private void Update() {
            if (initialized) {
                float minDist = radius;
                GameObject nearestBotShip = null;

                foreach (BotController botController in Bots.INSTANCE.getBotControllers()) {
                    GameObject ship = botController.getCurrentShip();
                    float dist = Vector3.Distance(ship.transform.position, transform.position);

                    if (dist < minDist) {
                        minDist = dist;
                        nearestBotShip = ship;
                    }
                }

                if (nearestBotShip != null) {
                    steerTowardsTarget(nearestBotShip);
                    shootBot(nearestBotShip);
                }
            }

            rangeCircle.SetActive(buildingController.showingEffect());

            if (rangeCircle.activeSelf) {
                Vector3 screenPos = cam.WorldToScreenPoint(transform.position);

                if (screenPos.z > 0f) {
                    screenPos.z = 0f;
                    rangeCircle.transform.position = screenPos;

                    Vector3 rangeScreenPos = cam.WorldToScreenPoint(transform.position + new Vector3(radius, 0f, 0f));
                    rangeScreenPos.z = 0f;

                    float dist = Vector3.Distance(rangeScreenPos, screenPos) * 2f;
                    rangeCircleImage.rectTransform.sizeDelta = new Vector2(dist, dist);
                }
            }
        }

        private void steerTowardsTarget(GameObject nearestBotShip) {
            steering.steerToTargetLocation(nearestBotShip.transform.position, transform);

            pitch = Vector3.zero;
            yaw = Vector3.zero;

            float agilityFactor = 10000f * Time.fixedDeltaTime;

            if (steering.pitchPercentage != 0f) {
                pitch = rigidBody.transform.right * (agilityFactor * steering.pitchPercentage);
            }

            if (steering.yawPercentage != 0f) {
                yaw = rigidBody.transform.up * (agilityFactor * steering.yawPercentage);
            }
        }

        private void shootBot(GameObject botShip) {
            if (Vector3.Angle(transform.forward, botShip.transform.position - transform.position) < 20f) {
                weaponController.fireWeapon(botShip.transform.position - weaponController.transform.position, gameObject,
                    botShip);
            }
        }

        private void FixedUpdate() {
            addTorque(pitch);
            addTorque(yaw);
        }
    }
}