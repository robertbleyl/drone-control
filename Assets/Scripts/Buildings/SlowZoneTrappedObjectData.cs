﻿using DroneControl.Ship;
using DroneControl.Ship.Weapon;

namespace DroneControl.Buildings {
    public class SlowZoneTrappedObjectData {
        private readonly WeaponController[] weaponControllers;
        private readonly ShipMovementController shipMovementController;

        public SlowZoneTrappedObjectData(WeaponController[] weaponControllers,
            ShipMovementController shipMovementController) {
            this.weaponControllers = weaponControllers;
            this.shipMovementController = shipMovementController;
        }

        public WeaponController[] getWeaponControllers() {
            return weaponControllers;
        }

        public ShipMovementController getShipMovementController() {
            return shipMovementController;
        }
    }
}