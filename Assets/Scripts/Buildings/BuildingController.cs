﻿using Core;
using Core.Events;
using DroneControl.Core;
using UnityEngine;

namespace DroneControl.Buildings {
    public class BuildingController : MonoBehaviour {
        [SerializeField]
        private GameObject invalidMarkerPrefab;

        [SerializeField]
        public BuildingType buildingType;

        public bool cannotBePlaced;
        private GameObject invalidMarker;

        private bool showEffect;

        public bool initialized;
        public Vector3 originalPos;

        private Camera cam;

        private void Awake() {
            cam = Camera.main;
            GameEvents.INSTANCE.onShowScreen += checkHighlight;
            GameEvents.INSTANCE.onBuildingAdded += onBuildingAdded;

            invalidMarker = Instantiate(invalidMarkerPrefab,
                GameObject.FindGameObjectWithTag(Tags.BuildMenuOverlayCanvas).transform, true);
            invalidMarker.SetActive(false);
        }

        private void OnDestroy() {
            Destroy(invalidMarker);

            GameEvents.INSTANCE.onShowScreen -= checkHighlight;
            GameEvents.INSTANCE.onBuildingAdded -= onBuildingAdded;
        }

        private void checkHighlight(GameScreenType screenType) {
            showEffectUI(screenType == GameScreenType.buildMenu);
        }

        private void onBuildingAdded(GameObject building) {
            if (building == gameObject) {
                initialized = true;
                originalPos = transform.position;
            }
        }

        public void showEffectUI(bool active) {
            showEffect = active;
        }

        public void showInvalidPlacementCircle(bool active) {
            invalidMarker.SetActive(active);
        }

        private void Update() {
            if (initialized) {
                transform.position = originalPos;
            }

            if (invalidMarker.activeSelf) {
                Vector3 screenPos = cam.WorldToScreenPoint(transform.position);

                if (screenPos.z >= 0f) {
                    screenPos.z = 0f;
                    invalidMarker.transform.localScale = Vector3.one;
                    invalidMarker.transform.position = screenPos;
                }
            }
        }

        public bool showingEffect() {
            return showEffect;
        }
    }
}