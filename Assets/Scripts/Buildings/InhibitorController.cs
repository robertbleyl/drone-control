﻿using System.Collections.Generic;
using Core;
using Core.Events;
using UnityEngine;
using UnityEngine.UI;

namespace DroneControl.Buildings {
    public class InhibitorController : MonoBehaviour {
        [SerializeField]
        private float radius = 600f;

        [SerializeField]
        private float time = 20f;

        [SerializeField]
        private GameObject rangeCirclePrefab;

        [SerializeField]
        private LineRenderer effect;

        private BuildingController buildingController;
        private GameObject rangeCircle;
        private Image rangeCircleImage;

        private bool initialized;
        private IBotSpawner targetBotSpawner;

        private float timer;
        private Camera cam;

        private List<IBotSpawner> botSpawners;

        private void Start() {
            cam = Camera.main;
            buildingController = GetComponent<BuildingController>();

            rangeCircle = Instantiate(rangeCirclePrefab,
                GameObject.FindGameObjectWithTag(Tags.BuildMenuOverlayCanvas).transform, true);
            rangeCircleImage = rangeCircle.GetComponent<Image>();

            botSpawners = BotSpawners.INSTANCE.getBotSpawners();

            GameEvents.INSTANCE.onBuildingAdded += onBuildingAdded;
        }

        private void onBuildingAdded(GameObject building) {
            if (building == gameObject) {
                initialized = true;
                Transform trans = transform;
                Vector3 spawnPosition = targetBotSpawner.getPosition();

                trans.LookAt(spawnPosition);
                effect.SetPosition(0, trans.position);
                effect.SetPosition(1, spawnPosition);
                targetBotSpawner.setShowInRangeCircle(false);
            }
        }

        private void OnDestroy() {
            Destroy(rangeCircle);
            GameEvents.INSTANCE.onBuildingAdded -= onBuildingAdded;
        }

        private void Update() {
            if (initialized) {
                if (targetBotSpawner == null) {
                    GameEvents.INSTANCE.destroyGameObject(new DestructionEvent(gameObject));
                    Destroy(gameObject);
                    return;
                }

                timer += Time.deltaTime;

                GameEvents.INSTANCE.targetDisplayProgressChanged(
                    new TargetDisplayProgressChangedEvent(gameObject, timer / time));

                if (timer >= time) {
                    targetBotSpawner.destroy();
                    GameEvents.INSTANCE.destroyGameObject(new DestructionEvent(gameObject));
                    Destroy(gameObject);
                }
            }
            else {
                float minDist = float.MaxValue;
                IBotSpawner oldTargetBotSpawner = targetBotSpawner;
                targetBotSpawner = null;

                foreach (IBotSpawner botSpawner in botSpawners) {
                    float dist = Vector3.Distance(botSpawner.getPosition(), transform.position);

                    if (dist < radius && dist < minDist) {
                        minDist = dist;
                        targetBotSpawner = botSpawner;
                    }
                }

                buildingController.cannotBePlaced = targetBotSpawner == null;

                if (targetBotSpawner != null) {
                    targetBotSpawner.setShowInRangeCircle(true);
                }

                if (oldTargetBotSpawner != null && oldTargetBotSpawner != targetBotSpawner) {
                    oldTargetBotSpawner.setShowInRangeCircle(false);
                }
            }

            rangeCircle.SetActive(buildingController.showingEffect());

            if (rangeCircle.activeSelf) {
                Vector3 screenPos = cam.WorldToScreenPoint(transform.position);

                if (screenPos.z > 0f) {
                    screenPos.z = 0f;
                    rangeCircle.transform.position = screenPos;

                    Vector3 rangeScreenPos = cam.WorldToScreenPoint(transform.position + new Vector3(radius, 0f, 0f));
                    rangeScreenPos.z = 0f;

                    float dist = Vector3.Distance(rangeScreenPos, screenPos) * 2f;
                    rangeCircleImage.rectTransform.sizeDelta = new Vector2(dist, dist);
                }
            }
        }
    }
}