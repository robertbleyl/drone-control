﻿using System;
using System.Collections.Generic;
using Core.Events;
using DroneControl.Core;
using UnityEngine;

namespace DroneControl.Buildings {
    public class BuildingsProvider {
        public static readonly BuildingsProvider INSTANCE = new();

        private readonly List<GameObject> allBuildings = new();
        private readonly Dictionary<BuildingType, List<GameObject>> map = new();

        private BuildingsProvider() {
            initMap();

            GameEvents.INSTANCE.onBuildingAdded += addBuilding;
            GameEvents.INSTANCE.onGameObjectDestroyed += checkRemoveBuilding;
        }

        private void initMap() {
            foreach (BuildingType type in Enum.GetValues(typeof(BuildingType))) {
                map[type] = new List<GameObject>();
            }
        }

        private void addBuilding(GameObject building) {
            allBuildings.Add(building);

            BuildingType type = building.GetComponent<BuildingController>().buildingType;
            List<GameObject> list = map[type];

            if (list == null) {
                list = new List<GameObject>();
                map[type] = list;
            }

            list.Add(building);
        }

        private void checkRemoveBuilding(DestructionEvent evt) {
            GameObject building = evt.gameObject;

            int index = allBuildings.IndexOf(building);
            if (index >= 0) {
                allBuildings.RemoveAt(index);

                BuildingType type = building.GetComponent<BuildingController>().buildingType;
                map[type].Remove(building);
            }
        }

        public List<GameObject> getAllBuildings() {
            return allBuildings;
        }

        public List<GameObject> getBuildings(BuildingType type) {
            return map[type];
        }

        public void clear() {
            allBuildings.Clear();
            initMap();
        }
    }
}