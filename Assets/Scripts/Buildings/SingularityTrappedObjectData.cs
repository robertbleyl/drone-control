﻿using DroneControl.Ship;
using UnityEngine;

namespace DroneControl.Buildings {
    public class SingularityTrappedObjectData {
        private readonly Rigidbody body;
        private readonly ShipMovementController shipMovementController;

        public SingularityTrappedObjectData(Rigidbody body, ShipMovementController shipMovementController) {
            this.body = body;
            this.shipMovementController = shipMovementController;
        }

        public Rigidbody getBody() {
            return body;
        }

        public ShipMovementController getShipMovementController() {
            return shipMovementController;
        }
    }
}