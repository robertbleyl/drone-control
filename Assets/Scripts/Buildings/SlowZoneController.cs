﻿using System.Collections.Generic;
using Core;
using Core.Events;
using DroneControl.Ship;
using DroneControl.Ship.Weapon;
using UnityEngine;
using UnityEngine.UI;

namespace DroneControl.Buildings {
    public class SlowZoneController : MonoBehaviour {
        [SerializeField]
        private float radius = 800f;

        [SerializeField]
        private float slowEffect = 0.5f;

        [SerializeField]
        private float expireTime = 90f;

        [SerializeField]
        private bool affectsDrones = true;

        [SerializeField]
        private SphereCollider sphereCollider;

        [SerializeField]
        private ParticleSystem particles;

        [SerializeField]
        private GameObject rangeCirclePrefab;

        private readonly Dictionary<GameObject, SlowZoneTrappedObjectData> affectedObjects = new();

        private BuildingController buildingController;
        private GameObject rangeCircle;
        private Image rangeCircleImage;

        private float expireTimer;
        private Camera cam;

        private void Start() {
            cam = Camera.main;
            buildingController = GetComponent<BuildingController>();

            updateScale();

            rangeCircle = Instantiate(rangeCirclePrefab,
                GameObject.FindGameObjectWithTag(Tags.BuildMenuOverlayCanvas).transform, true);
            rangeCircleImage = rangeCircle.GetComponent<Image>();

            GameEvents.INSTANCE.onGameObjectDestroyed += onGameObjectDestroyed;
            GameEvents.INSTANCE.onReplaceDrone += onReplaceDrone;
        }

        private void OnDestroy() {
            Destroy(rangeCircle);

            foreach (SlowZoneTrappedObjectData data in affectedObjects.Values) {
                removeSlowEffect(data);
            }

            GameEvents.INSTANCE.onGameObjectDestroyed -= onGameObjectDestroyed;
            GameEvents.INSTANCE.onReplaceDrone -= onReplaceDrone;
        }

        private void onGameObjectDestroyed(DestructionEvent evt) {
            affectedObjects.Remove(evt.gameObject);
        }

        private void onReplaceDrone(GameObject drone) {
            affectedObjects.Remove(drone);
        }

        private void updateScale() {
            sphereCollider.radius = radius;
            ParticleSystem.ShapeModule shape = particles.shape;
            shape.radius = radius;
        }

        private void Update() {
            updateRangeCircle();

            if (!buildingController.initialized) {
                return;
            }

            expireTimer += Time.deltaTime;

            GameEvents.INSTANCE.targetDisplayProgressChanged(
                new TargetDisplayProgressChangedEvent(gameObject, 1f - (expireTimer / expireTime)));

            if (expireTimer >= expireTime) {
                GameEvents.INSTANCE.destroyGameObject(new DestructionEvent(gameObject));
                Destroy(gameObject);
            }
        }

        private void updateRangeCircle() {
            rangeCircle.SetActive(buildingController.showingEffect());

            if (rangeCircle.activeSelf) {
                Vector3 screenPos = cam.WorldToScreenPoint(transform.position);

                if (screenPos.z > 0f) {
                    screenPos.z = 0f;
                    rangeCircle.transform.position = screenPos;

                    Vector3 rangeScreenPos = cam.WorldToScreenPoint(transform.position + new Vector3(radius, 0f, 0f));
                    rangeScreenPos.z = 0f;

                    float dist = Vector3.Distance(rangeScreenPos, screenPos) * 2f;
                    rangeCircleImage.rectTransform.sizeDelta = new Vector2(dist, dist);
                }
            }
        }

        private void removeSlowEffect(SlowZoneTrappedObjectData data) {
            if (data == null) {
                return;
            }

            data.getShipMovementController().speedPercentage = 1f;

            foreach (WeaponController weaponController in data.getWeaponControllers()) {
                weaponController.slowZoneEffect = 1f;
            }
        }

        private void OnTriggerEnter(Collider other) {
            if (!buildingController.initialized) {
                return;
            }

            // TODO
            // if (!affectsDrones && other.GetComponentInParent<DroneController>() != null) {
            //     return;
            // }

            ShipMovementController shipMovementController = other.GetComponentInParent<ShipMovementController>();

            if (shipMovementController != null &&
                PlayerData.INSTANCE.currentPlayerShip != shipMovementController.gameObject) {
                shipMovementController.speedPercentage = slowEffect;
                WeaponController[] weaponControllers = shipMovementController.GetComponentsInChildren<WeaponController>();

                foreach (WeaponController weaponController in weaponControllers) {
                    weaponController.slowZoneEffect = slowEffect;
                }

                if (!affectedObjects.ContainsKey(shipMovementController.gameObject)) {
                    affectedObjects.Add(shipMovementController.gameObject,
                        new SlowZoneTrappedObjectData(weaponControllers, shipMovementController));
                }
            }
        }

        private void OnTriggerExit(Collider other) {
            if (!buildingController.initialized) {
                return;
            }

            ShipMovementController shipMovementController = other.GetComponentInParent<ShipMovementController>();

            if (shipMovementController != null && affectedObjects.ContainsKey(shipMovementController.gameObject)) {
                removeSlowEffect(affectedObjects[shipMovementController.gameObject]);
            }
        }
    }
}