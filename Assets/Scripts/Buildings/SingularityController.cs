﻿using System.Collections;
using System.Collections.Generic;
using Core;
using Core.Events;
using DroneControl.Ship;
using UnityEngine;
using UnityEngine.UI;

namespace DroneControl.Buildings {
    public class SingularityController : MonoBehaviour {
        [SerializeField]
        private float radius = 800f;

        [SerializeField]
        private float minPullStrength = 50f;

        [SerializeField]
        private float maxPullStrength = 500f;

        [SerializeField]
        private float destructionDist = 20f;

        [SerializeField]
        private float expireTime = 20f;

        [SerializeField]
        private bool affectsDrones = true;

        [SerializeField]
        private bool affectsPlayer = true;

        [SerializeField]
        private SphereCollider sphereCollider;

        [SerializeField]
        private ParticleSystem particles;

        [SerializeField]
        private GameObject rangeCirclePrefab;

        private BuildingController buildingController;
        private GameObject rangeCircle;
        private Image rangeCircleImage;

        private float expireTimer;

        private Camera cam;

        private readonly Dictionary<GameObject, SingularityTrappedObjectData> affectedBodies = new();

        private void Start() {
            cam = Camera.main;
            buildingController = GetComponent<BuildingController>();

            updateScale();

            rangeCircle = Instantiate(rangeCirclePrefab,
                GameObject.FindGameObjectWithTag(Tags.BuildMenuOverlayCanvas).transform, true);
            rangeCircleImage = rangeCircle.GetComponent<Image>();

            GameEvents.INSTANCE.onGameObjectDestroyed += onGameObjectDestroyed;
            GameEvents.INSTANCE.onReplaceDrone += onReplaceDrone;
        }

        private void OnDestroy() {
            Destroy(rangeCircle);

            foreach (SingularityTrappedObjectData data in affectedBodies.Values) {
                data.getShipMovementController().singularityFieldEffect = 1f;
            }

            GameEvents.INSTANCE.onGameObjectDestroyed -= onGameObjectDestroyed;
            GameEvents.INSTANCE.onReplaceDrone -= onReplaceDrone;
        }


        private void onGameObjectDestroyed(DestructionEvent evt) {
            affectedBodies.Remove(evt.gameObject);
        }

        private void onReplaceDrone(GameObject drone) {
            affectedBodies.Remove(drone);
        }

        private void updateScale() {
            sphereCollider.radius = radius;
            ParticleSystem.ShapeModule shape = particles.shape;
            shape.radius = radius;
        }

        private void Update() {
            updateRangeCircle();

            if (!buildingController.initialized) {
                return;
            }

            expireTimer += Time.deltaTime;

            GameEvents.INSTANCE.targetDisplayProgressChanged(
                new TargetDisplayProgressChangedEvent(gameObject, 1f - (expireTimer / expireTime)));

            if (expireTimer >= expireTime) {
                GameEvents.INSTANCE.destroyGameObject(new DestructionEvent(gameObject));
                Destroy(gameObject);
            }
            else {
                foreach (GameObject entry in affectedBodies.Keys) {
                    float dist = Vector3.Distance(entry.transform.position, transform.position);

                    if (dist <= destructionDist) {
                        StartCoroutine(destroyBody(entry));
                    }
                }
            }
        }

        private void updateRangeCircle() {
            rangeCircle.SetActive(buildingController.showingEffect());

            if (rangeCircle.activeSelf) {
                Vector3 screenPos = cam.WorldToScreenPoint(transform.position);

                if (screenPos.z > 0f) {
                    screenPos.z = 0f;
                    rangeCircle.transform.position = screenPos;

                    Vector3 rangeScreenPos = cam.WorldToScreenPoint(transform.position + new Vector3(radius, 0f, 0f));
                    rangeScreenPos.z = 0f;

                    float dist = Vector3.Distance(rangeScreenPos, screenPos) * 2f;
                    rangeCircleImage.rectTransform.sizeDelta = new Vector2(dist, dist);
                }
            }
        }

        private IEnumerator destroyBody(GameObject body) {
            yield return new WaitForEndOfFrame();
            affectedBodies.Remove(body);
            GameEvents.INSTANCE.destroyGameObject(new DestructionEvent(body));
            Destroy(body);
        }

        private void FixedUpdate() {
            foreach (SingularityTrappedObjectData data in affectedBodies.Values) {
                Rigidbody body = data.getBody();
                Vector3 direction = transform.position - body.position;
                float effect = direction.magnitude / radius;

                data.getShipMovementController().singularityFieldEffect = effect;

                float pullStrength = maxPullStrength * (1f - effect);

                if (pullStrength < minPullStrength) {
                    pullStrength = minPullStrength;
                }

                Vector3 force = direction.normalized * pullStrength;
                body.AddForce(force);
            }
        }

        private void OnTriggerEnter(Collider other) {
            if (!buildingController.initialized) {
                return;
            }

            // TODO
            // if (!affectsDrones && other.GetComponentInParent<DroneController>() != null) {
            //     return;
            // }

            ShipMovementController shipMovementController = other.GetComponentInParent<ShipMovementController>();

            if (shipMovementController != null) {
                if (!affectsPlayer && PlayerData.INSTANCE.currentPlayerShip == shipMovementController.gameObject) {
                    return;
                }

                if (affectedBodies.ContainsKey(shipMovementController.gameObject)) {
                    return;
                }

                Rigidbody body = shipMovementController.GetComponent<Rigidbody>();

                if (body != null) {
                    SingularityTrappedObjectData data = new(body, shipMovementController);
                    affectedBodies.Add(shipMovementController.gameObject, data);
                }
            }
        }

        private void OnTriggerExit(Collider other) {
            if (!buildingController.initialized) {
                return;
            }

            ShipMovementController shipMovementController = other.GetComponentInParent<ShipMovementController>();

            if (shipMovementController != null) {
                if (!affectedBodies.ContainsKey(shipMovementController.gameObject)) {
                    return;
                }

                Rigidbody body = shipMovementController.GetComponent<Rigidbody>();

                if (body != null) {
                    shipMovementController.singularityFieldEffect = 1f;
                    affectedBodies.Remove(shipMovementController.gameObject);
                }
            }
        }
    }
}